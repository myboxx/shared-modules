/**
 * Generated bundle index. Do not edit.
 */
export * from './public-api';
export { CalendarSheetComponent as ɵb } from './lib/components/calendar-sheet/calendar-sheet.component';
export { NativeVersionInfoComponent as ɵc } from './lib/components/native-version-info/native-version-info.component';
export { AppSettingsService as ɵa } from './lib/providers/global-params';
