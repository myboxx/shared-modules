import { __decorate, __awaiter, __generator, __read, __spread, __param } from 'tslib';
import { CommonModule } from '@angular/common';
import { Input, Component, ViewChild, InjectionToken, Optional, Inject, ɵɵdefineInjectable, ɵɵinject, Injectable, EventEmitter, Output, NgModule } from '@angular/core';
import { FormGroup, FormControl, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { ModalController, AlertController, PopoverController, LoadingController, ToastController, Platform, IonicModule } from '@ionic/angular';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { ContactStore, COUNTRY_CALLING_CODES, ContactModel } from '@boxx/contacts-core';
import { Subject, BehaviorSubject } from 'rxjs';
import { takeUntil, withLatestFrom } from 'rxjs/operators';
import { EventsStore } from '@boxx/events-core';
import * as moment_ from 'moment';
import { AppVersion as AppVersion$1 } from '@ionic-native/app-version/ngx/index';

var CalendarSheetComponent = /** @class */ (function () {
    function CalendarSheetComponent() {
    }
    CalendarSheetComponent.prototype.ngOnInit = function () {
        this.dayName = this.dayName.toUpperCase().replace('.', '');
        this.monthName = this.monthName.toUpperCase().replace('.', '');
    };
    __decorate([
        Input()
    ], CalendarSheetComponent.prototype, "dayName", void 0);
    __decorate([
        Input()
    ], CalendarSheetComponent.prototype, "monthDay", void 0);
    __decorate([
        Input()
    ], CalendarSheetComponent.prototype, "monthName", void 0);
    __decorate([
        Input()
    ], CalendarSheetComponent.prototype, "headerIonColor", void 0);
    CalendarSheetComponent = __decorate([
        Component({
            selector: 'boxx-calendar-sheet',
            template: "<ion-card>\n    <ion-card-header color=\"{{headerIonColor || 'primary'}}\">\n        <ion-card-subtitle>{{dayName}}</ion-card-subtitle>\n    </ion-card-header>\n\n    <ion-card-content>\n        <p>\n            <strong>{{monthDay}}</strong>\n        </p>\n        <p>{{monthName}}</p>\n    </ion-card-content>\n</ion-card>",
            styles: [":host{margin:0}:host ion-card{line-height:1.5;padding-right:0;border-radius:8px;margin-left:0;margin-right:17px}:host ion-card.ios{margin:8px 17px 8px 0}:host ion-card-header{padding:0;text-align:center}:host ion-card-content{min-width:40px;text-align:center;font-size:x-small;line-height:1;padding:2px 8px 4px}:host ion-card-content strong{font-size:16px}:host ion-card-content p{padding:0;font-size:12px;font-weight:600}:host .card-content-md p{line-height:1.2;margin-bottom:0}:host ion-card-subtitle{margin-bottom:0;padding:2px 0;font-size:small}"]
        })
    ], CalendarSheetComponent);
    return CalendarSheetComponent;
}());

var ContactPickerModalComponent = /** @class */ (function () {
    function ContactPickerModalComponent(modalCtrl, store, alertCtrl, translate) {
        var _this = this;
        this.modalCtrl = modalCtrl;
        this.store = store;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.searchValue = '';
        this.contactList = [];
        this.isFiltering = false;
        this.destroyed$ = new Subject();
        this.translate.get(['CONTACTS', 'GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (t) { return _this.translations = t; });
    }
    ContactPickerModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isLoading$ = this.store.Loading$;
        this.store.Contacts$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (cl) {
            _this.searchResults = _this.contactList = cl.map(function (c) {
                return ({ isChecked: _this.currentAttendees.findIndex(function (e) { return e === c.id; }) !== -1, contact: c });
            });
            setTimeout(function () {
                _this.searchBar.setFocus();
            }, 800);
        });
        this.store.HasBeenFetched$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (ok) {
            if (!ok) {
                _this.store.fetchContacts();
            }
        });
    };
    ContactPickerModalComponent.prototype.ngOnDestroy = function () {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    };
    ContactPickerModalComponent.prototype.checkMaster = function () {
        var _this = this;
        setTimeout(function () {
            _this.searchResults.forEach(function (obj) {
                obj.isChecked = _this.masterCheck;
            });
        });
    };
    ContactPickerModalComponent.prototype.checkEvent = function () {
        var totalItems = this.searchResults.length;
        var checked = 0;
        this.searchResults.map(function (obj) {
            if (obj.isChecked) {
                checked++;
            }
        });
        if (checked > 0 && checked < totalItems) {
            // If even one item is checked but not all
            this.isIndeterminate = true;
            this.masterCheck = false;
        }
        else if (checked === totalItems) {
            // If all are checked
            this.masterCheck = true;
            this.isIndeterminate = false;
        }
        else {
            // If none is checked
            this.isIndeterminate = false;
            this.masterCheck = false;
        }
    };
    ContactPickerModalComponent.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.hasChanges()) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.alertCtrl.create({
                                header: this.translations.GENERAL.ACTION.confirm,
                                message: this.translations.CONTACTS.confirmCloseWithSelectedContactsMsg,
                                buttons: [{
                                        text: this.translations.GENERAL.ACTION.ok,
                                        handler: function () {
                                            _this.modalCtrl.dismiss( /*{someData: ...} */);
                                        }
                                    }, {
                                        text: this.translations.GENERAL.ACTION.cancel,
                                        role: 'cancel',
                                        cssClass: 'primary',
                                    },]
                            })];
                    case 1:
                        (_a.sent()).present();
                        return [3 /*break*/, 3];
                    case 2:
                        this.modalCtrl.dismiss( /*{someData: ...} */);
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ContactPickerModalComponent.prototype.select = function () {
        return this.modalCtrl.dismiss(this._getSelectedContacts());
    };
    ContactPickerModalComponent.prototype._getSelectedContacts = function () {
        return this.searchResults.filter(function (c) { return c.isChecked; }).map(function (c) { return c.contact; });
    };
    ContactPickerModalComponent.prototype.hasChanges = function () {
        var _this = this;
        return this._getSelectedContacts().length !== this.currentAttendees.length ||
            this._getSelectedContacts()
                .filter(function (c) { return !_this.currentAttendees.includes(c.id); }).length > 0;
    };
    ContactPickerModalComponent.prototype.search = function (searchTerm) {
        this.searchResults = this.contactList;
        if (!searchTerm) {
            this.isFiltering = false;
            return;
        }
        this.isFiltering = true;
        this.searchResults = this.searchResults.filter(function (item) {
            return (item.contact.name + item.contact.lastName).toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
                item.contact.phone.indexOf(searchTerm) > -1 ||
                item.contact.email.indexOf(searchTerm) > -1;
        });
    };
    ContactPickerModalComponent.prototype.showAll = function () {
        var _this = this;
        this.searchValue = ' ';
        setTimeout(function () {
            _this.searchValue = '';
        }, 10);
    };
    ContactPickerModalComponent.ctorParameters = function () { return [
        { type: ModalController },
        { type: ContactStore },
        { type: AlertController },
        { type: TranslateService }
    ]; };
    __decorate([
        ViewChild('contactPickerSB', { static: false })
    ], ContactPickerModalComponent.prototype, "searchBar", void 0);
    __decorate([
        Input()
    ], ContactPickerModalComponent.prototype, "currentAttendees", void 0);
    ContactPickerModalComponent = __decorate([
        Component({
            selector: 'boxx-contact-picker-modal',
            template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                GENERAL.contacts\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\" [hidden]=\"isFiltering\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <ion-buttons slot=\"end\" [hidden]=\"isFiltering || !hasChanges()\">\n            <ion-button (click)=\"select()\" translate>\n                <ion-icon name=\"checkmark-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <ion-buttons slot=\"end\" [hidden]=\"!isFiltering\">\n            <ion-button (click)=\"showAll()\" translate>\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #contactPickerSB showCancelButton=\"never\" cancelButtonIcon=\"close-circle-outline\"\n        (ionChange)=\"search($event.detail.value)\" [value]=\"searchValue\"\n        placeholder=\"{{'CONTACTS.searchPlaceholder'| translate}}\"></ion-searchbar>\n    </ion-item-divider>\n\n    <div *ngIf=\"isLoading$ | async\" class=\"ion-padding\">\n        <ion-item class=\"ion-no-padding\">\n            <ion-label>\n                <ion-skeleton-text animated style=\"width: 40%\"></ion-skeleton-text>\n            </ion-label>\n            <ion-thumbnail slot=\"start\" style=\"width: 20px; height: 20px; margin-right: 30px; margin-left: 4px;\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n            </ion-thumbnail>\n        </ion-item>\n        <ion-list>\n            <ion-item class=\"ion-no-padding\" *ngFor=\"let item of [].constructor(5)\">\n                <ion-label>\n                    <ion-skeleton-text animated style=\"width: 80%\"></ion-skeleton-text>\n                    <ion-skeleton-text animated style=\"width: 60%\"></ion-skeleton-text>\n                </ion-label>\n                <ion-thumbnail slot=\"start\" style=\"width: 20px; height: 20px; margin-right: 30px; margin-left: 4px;\">\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                </ion-thumbnail>\n            </ion-item>\n        </ion-list>\n    </div>\n\n    <ion-item [hidden]=\"(isLoading$ | async) || isFiltering\" class=\"padding-right\">\n        <ion-label>\n            <strong>{{\"GENERAL.total\" | translate}}: {{contactList.length}}\n                {{ (contactList.length == 1 ? \"GENERAL.contact\" : \"GENERAL.contacts\") | translate}}\n            </strong>\n        </ion-label>\n        <ion-checkbox mode=\"ios\" slot=\"start\" [(ngModel)]=\"masterCheck\" [indeterminate]=\"isIndeterminate\"\n            (click)=\"checkMaster()\"></ion-checkbox>\n    </ion-item>\n\n    <ion-list class=\"ion-padding\">\n        <ion-item class=\"ion-no-padding\" *ngFor=\"let item of searchResults\">\n            <ion-label>\n                <h3>{{item.contact.name}} {{item.contact.lastName}}</h3>\n                <p>{{item.contact.email}}</p>\n                <p>{{item.contact.phone}}</p>\n            </ion-label>\n            <ion-checkbox mode=\"ios\" slot=\"start\" [(ngModel)]=\"item.isChecked\" (ionChange)=\"checkEvent()\">\n            </ion-checkbox>\n        </ion-item>\n    </ion-list>\n</ion-content>",
            styles: ["ion-toolbar.md ion-title{padding:0}"]
        })
    ], ContactPickerModalComponent);
    return ContactPickerModalComponent;
}());

var AppSettingsService = /** @class */ (function () {
    function AppSettingsService(settings) {
        this.setAppConstants(settings.appConstants);
    }
    AppSettingsService.prototype.setAppConstants = function (value) { this.appConstants = value; };
    AppSettingsService.prototype.getAppConstants = function () { return this.appConstants; };
    return AppSettingsService;
}());
var LOCAL_NOTIFICATIONS_SERVICE = new InjectionToken('localNotificationsService');

var CountryCodeSelectorPopoverComponent = /** @class */ (function () {
    function CountryCodeSelectorPopoverComponent(popoverController) {
        this.popoverController = popoverController;
        this.codes = [];
    }
    CountryCodeSelectorPopoverComponent.prototype.ngOnInit = function () {
    };
    CountryCodeSelectorPopoverComponent.prototype.select = function (code) {
        this.popoverController.dismiss(code);
    };
    CountryCodeSelectorPopoverComponent.ctorParameters = function () { return [
        { type: PopoverController }
    ]; };
    __decorate([
        Input()
    ], CountryCodeSelectorPopoverComponent.prototype, "codes", void 0);
    CountryCodeSelectorPopoverComponent = __decorate([
        Component({
            selector: 'boxx-country-code-select-popover',
            template: "\n    <ion-content>\n        <ion-list-header translate>\n            GENERAL.ACTION.selectCountryCode\n        </ion-list-header>\n        <ion-item *ngFor=\"let code of codes\" (click)=\"select(code)\">\n            <ion-label>\n                +{{code}}\n            </ion-label>\n        </ion-item>\n    </ion-content>\n    "
        })
    ], CountryCodeSelectorPopoverComponent);
    return CountryCodeSelectorPopoverComponent;
}());

var CountrySelectorModalComponent = /** @class */ (function () {
    function CountrySelectorModalComponent(popoverController) {
        this.popoverController = popoverController;
        this.countries = [];
        this.items = [];
    }
    CountrySelectorModalComponent.prototype.ngOnInit = function () {
        this.items = this.countries;
    };
    CountrySelectorModalComponent.prototype.select = function (country) {
        this.popoverController.dismiss(country);
    };
    CountrySelectorModalComponent.prototype.search = function (searchTerm) {
        this.items = this.countries.filter(function (country) {
            return (country.translations.es || country.name || '')
                .toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') // replace accented chars
                .indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    CountrySelectorModalComponent.prototype.close = function () {
        this.popoverController.dismiss();
    };
    CountrySelectorModalComponent.ctorParameters = function () { return [
        { type: ModalController }
    ]; };
    __decorate([
        Input()
    ], CountrySelectorModalComponent.prototype, "countries", void 0);
    CountrySelectorModalComponent = __decorate([
        Component({
            selector: 'boxx-country-selector-modal',
            template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                GENERAL.ACTION.selectCountry\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #CountryPicker showCancelButton=\"never\" cancelButtonIcon=\"close-circle-outline\"\n            (ionChange)=\"search($event.detail.value)\" placeholder=\"{{'GENERAL.ACTION.searchCountry' | translate}}\"></ion-searchbar>\n    </ion-item-divider>\n\n    <ion-virtual-scroll [items]=\"items\" approxItemHeight=\"64px\">\n        <ion-item *virtualItem=\"let country\" (click)=\"select(country)\">\n            <ion-label>\n                <ion-img [src]=\"country.flag\" [alt]=\"country.name\" style=\"width: 24px; display: inline-block;\">\n                </ion-img>\n                {{country.translations.es || country.name}}\n            </ion-label>\n        </ion-item>\n    </ion-virtual-scroll>\n</ion-content>",
            styles: ["ion-toolbar.md ion-title{padding:0}"]
        })
    ], CountrySelectorModalComponent);
    return CountrySelectorModalComponent;
}());

var ContactUpsertModalComponent = /** @class */ (function () {
    function ContactUpsertModalComponent(modalCtrl, store, translate, popoverController, mdalCtrl, loadingCtrl, moduleService) {
        var _this = this;
        this.modalCtrl = modalCtrl;
        this.store = store;
        this.translate = translate;
        this.popoverController = popoverController;
        this.mdalCtrl = mdalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.moduleService = moduleService;
        this.destroyed$ = new Subject();
        this.types = [
            { key: 'CLIENT', value: 'Client' },
            { key: 'PROSPECT', value: 'Prospect' },
            { key: 'NOT_SPECIFIED', value: 'Not qualified' }
        ];
        this.validationMessages = {
            name: this.moduleService.getAppConstants().VALIDATION_MESSAGES.name,
            last_name: this.moduleService.getAppConstants().VALIDATION_MESSAGES.last_name,
            phone: this.moduleService.getAppConstants().VALIDATION_MESSAGES.phone,
            email: this.moduleService.getAppConstants().VALIDATION_MESSAGES.email,
            address: this.moduleService.getAppConstants().VALIDATION_MESSAGES.address
        };
        this.translate.get(['GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (t) {
            _this.types[0].value = t.GENERAL.client;
            _this.types[1].value = t.GENERAL.prospect;
            _this.types[2].value = t.GENERAL.not_specified;
        });
    }
    ContactUpsertModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.contactForm = new FormGroup({
            name: new FormControl(this.contact.name, Validators.required),
            last_name: new FormControl(this.contact.lastName, Validators.required),
            phone: new FormControl(this.contact.phone, [Validators.minLength(10), Validators.maxLength(10), Validators.required, Validators.pattern('[0-9]*')]),
            email: new FormControl(this.contact.email, Validators.pattern(this.moduleService.getAppConstants().EMAILPATTERN)),
            street_address: new FormControl(this.contact.streetAddress),
            type: new FormControl(this.contact.type),
            city: new FormControl(this.contact.city),
            id: new FormControl(this.contact.id),
            country_code: new FormControl(COUNTRY_CALLING_CODES[1]),
            phone_code: new FormControl(this.contact.phoneCode || COUNTRY_CALLING_CODES[1].callingCodes[0]),
        });
        this.store.CountryCodes$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (countries) { return __awaiter(_this, void 0, void 0, function () {
            var countryCode;
            var _this = this;
            return __generator(this, function (_a) {
                if (!countries.length) {
                    return [2 /*return*/, this.store.fetchCountryCodes()];
                }
                countryCode = countries.filter(function (c) { return c.alpha3Code === _this.contact.countryCode; })[0] || COUNTRY_CALLING_CODES[1];
                this.contactForm.patchValue({ country_code: countryCode });
                return [2 /*return*/];
            });
        }); });
    };
    ContactUpsertModalComponent.prototype.ngOnDestroy = function () {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    };
    ContactUpsertModalComponent.prototype.close = function () {
        this.modalCtrl.dismiss( /*{someData: ...} */);
    };
    ContactUpsertModalComponent.prototype.update = function () {
        // this._appendCountryCode();
        var form = this.contactForm.value;
        delete form.phone_code;
        form.country_code = form.country_code.alpha3Code;
        this.store.updateContact(form);
        this.modalCtrl.dismiss( /*{someData: ...} */);
    };
    ContactUpsertModalComponent.prototype.create = function () {
        // this._appendCountryCode();
        var form = this.contactForm.value;
        delete form.phone_code;
        form.country_code = form.country_code.alpha3Code;
        this.store.createContact(form);
        this.modalCtrl.dismiss( /*{someData: ...} */);
    };
    ContactUpsertModalComponent.prototype.presentPopover = function ( /*ev: any*/) {
        return __awaiter(this, void 0, void 0, function () {
            var loader;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create()];
                    case 1:
                        loader = _a.sent();
                        loader.present();
                        this.store.CountryCodes$
                            .pipe(takeUntil(this.destroyed$))
                            .subscribe(function (countries) { return __awaiter(_this, void 0, void 0, function () {
                            var popover, response, selCountry, data;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!countries.length) {
                                            return [2 /*return*/, this.store.fetchCountryCodes()];
                                        }
                                        return [4 /*yield*/, this.mdalCtrl.create({
                                                component: CountrySelectorModalComponent,
                                                componentProps: { countries: countries },
                                            })];
                                    case 1:
                                        popover = _a.sent();
                                        return [4 /*yield*/, popover.present()];
                                    case 2:
                                        _a.sent();
                                        loader.dismiss();
                                        return [4 /*yield*/, popover.onDidDismiss()];
                                    case 3:
                                        response = _a.sent();
                                        selCountry = response.data;
                                        if (!selCountry) return [3 /*break*/, 8];
                                        if (!(selCountry.callingCodes.length > 1)) return [3 /*break*/, 7];
                                        return [4 /*yield*/, this.popoverController.create({
                                                component: CountryCodeSelectorPopoverComponent,
                                                componentProps: { codes: selCountry.callingCodes },
                                                // event: ev,
                                                translucent: true,
                                                backdropDismiss: false
                                            })];
                                    case 4:
                                        popover = _a.sent();
                                        return [4 /*yield*/, popover.present()];
                                    case 5:
                                        _a.sent();
                                        return [4 /*yield*/, popover.onDidDismiss()];
                                    case 6:
                                        data = (_a.sent()).data;
                                        this.contactForm.patchValue({ country_code: selCountry, phone_code: "+" + data });
                                        return [3 /*break*/, 8];
                                    case 7:
                                        this.contactForm.patchValue({ country_code: selCountry, phone_code: "+" + selCountry.callingCodes[0] });
                                        _a.label = 8;
                                    case 8: return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    ContactUpsertModalComponent.prototype._appendCountryCode = function () {
        var country = this.contactForm.get('country_code').value;
        switch (country.alpha3Code) {
            case 'MEX':
                this.contactForm.patchValue({
                    phone: ("+" + country.callingCodes[0] + " 1 " + this.contactForm.get('phone').value).replace(/ /g, '')
                });
                break;
            case 'ARG':
                this.contactForm.patchValue({
                    phone: ("+" + country.callingCodes[0] + " 9 " + this.contactForm.get('phone').value).replace(/ /g, '')
                });
        }
        this.contactForm.get('country_code').disable();
    };
    ContactUpsertModalComponent.ctorParameters = function () { return [
        { type: ModalController },
        { type: ContactStore },
        { type: TranslateService },
        { type: PopoverController },
        { type: ModalController },
        { type: LoadingController },
        { type: AppSettingsService }
    ]; };
    __decorate([
        Input()
    ], ContactUpsertModalComponent.prototype, "contact", void 0);
    __decorate([
        Input()
    ], ContactUpsertModalComponent.prototype, "mode", void 0);
    ContactUpsertModalComponent = __decorate([
        Component({
            selector: 'boxx-contact-upsert-modal',
            template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                GENERAL.contacts\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n    <ion-list class=\"ion-no-margin ion-no-padding\" [formGroup]=\"contactForm\">\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.name\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-input formControlName=\"name\" type=\"text\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.name\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('name').hasError(validation.type) && contactForm.get('name').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.lastName\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-input formControlName=\"last_name\" type=\"text\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.last_name\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('last_name').hasError(validation.type) && contactForm.get('last_name').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.type\n            </ion-label>\n            <ion-select placeholder=\"{{'GENERAL.ACTION.select' | translate}}\" formControlName=\"type\"\n                [okText]=\"'GENERAL.ACTION.ok' | translate\" [cancelText]=\"'GENERAL.ACTION.cancel' | translate\"\n                interface=\"popover\">\n                <ion-select-option *ngFor=\"let cType of types\" [value]=\"cType.key\">{{cType.value}}</ion-select-option>\n            </ion-select>\n        </ion-item>\n\n        <ion-item (click)=\"presentPopover()\" class=\"boxx-input-item\">\n            <ion-label>\n                <p>\n                    <ion-text color=\"primary\" translate>FORM.LABEL.country</ion-text>\n                </p>\n                <img alt=\"country flag\" src=\"{{contactForm.get('country_code').value.flag}}\" style=\"width: 32px;\">\n                {{contactForm.get('country_code').value.translations.es || contactForm.get('country_code').value.name}}\n            </ion-label>\n        </ion-item>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.phone\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-row style=\"width: 100%;\">\n                <ion-col size=\"2\" style=\"display: grid; align-content: center;\">\n                    <small>{{contactForm.get('phone_code').value}}</small>\n                </ion-col>\n                <ion-col size=\"10\">\n                    <ion-input formControlName=\"phone\" type=\"tel\" id=\"phone-input\" minLength=\"10\" maxLength=\"10\"\n                        placeholder=\"123 4567 890\">\n                    </ion-input>\n                </ion-col>\n            </ion-row>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.phone\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('phone').hasError(validation.type) && contactForm.get('phone').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.email\n            </ion-label>\n            <ion-input formControlName=\"email\" type=\"email\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.email\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('email').hasError(validation.type) && contactForm.get('email').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.address\n            </ion-label>\n            <ion-input formControlName=\"street_address\" type=\"text\"></ion-input>\n        </ion-item>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.city\n            </ion-label>\n            <ion-input formControlName=\"city\" type=\"text\"></ion-input>\n        </ion-item>\n    </ion-list>\n\n    <div class=\"ion-margin-vertical\">\n        <ion-button color=\"primary\" expand=\"block\" (click)=\"update()\" *ngIf=\"mode == 'UPDATE'\"\n            [disabled]=\"contactForm.invalid\" translate>\n            GENERAL.ACTION.update\n        </ion-button>\n        <ion-button color=\"primary\" expand=\"block\" (click)=\"create()\" *ngIf=\"mode == 'CREATE'\"\n            [disabled]=\"contactForm.invalid\" translate>\n            GENERAL.ACTION.create\n        </ion-button>\n    </div>\n\n</ion-content>",
            styles: [":host ion-toolbar.md ion-title{padding:0}:host #phone-input{background-color:#efefef;padding-left:8px!important}"]
        })
    ], ContactUpsertModalComponent);
    return ContactUpsertModalComponent;
}());

var PlacePickerModalComponent = /** @class */ (function () {
    function PlacePickerModalComponent(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.destroyed$ = new Subject();
        this.searchResults = [];
        this.mapsService = new google.maps.places.AutocompleteService();
    }
    PlacePickerModalComponent.prototype.ngOnDestroy = function () {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    };
    PlacePickerModalComponent.prototype.ionViewDidEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.searchBar.setFocus();
        }, 200);
    };
    PlacePickerModalComponent.prototype.onInput = function (query) {
        var _this = this;
        this.searchResults = [];
        if (!query) {
            return;
        }
        var config = {
            types: [],
            input: query
        };
        this.mapsService.getPlacePredictions(config, function (predictions, status) {
            if (predictions && predictions.length > 0) {
                predictions.forEach(function (prediction) {
                    _this.searchResults.push(prediction);
                });
            }
            else {
                var locationDesc = {
                    description: query,
                    place_id: 'default_place_id'
                };
                _this.searchResults.push(locationDesc);
            }
        });
    };
    PlacePickerModalComponent.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.modalCtrl.dismiss( /*{someData: ...} */);
                return [2 /*return*/];
            });
        });
    };
    PlacePickerModalComponent.prototype.chooseItem = function (item) {
        this.modalCtrl.dismiss({ description: item.description, id: item.place_id });
    };
    PlacePickerModalComponent.prototype.select = function () { };
    PlacePickerModalComponent.ctorParameters = function () { return [
        { type: ModalController }
    ]; };
    __decorate([
        ViewChild('placePickerSB', { static: false })
    ], PlacePickerModalComponent.prototype, "searchBar", void 0);
    PlacePickerModalComponent = __decorate([
        Component({
            selector: 'boxx-place-picker-modal',
            template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                FORM.LABEL.place\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #placePickerSB [debounce]=\"300\" showCancelButton=\"never\"\n            (ionInput)=\"onInput($event.target.value)\" cancelButtonIcon=\"close-circle-outline\"\n            placeholder='{{\"EVENTS.enterAddress\" | translate}}'></ion-searchbar>\n    </ion-item-divider>\n\n    <ion-list class=\"padding-right\">\n        <ion-item *ngFor=\"let item of searchResults\" (click)=\"chooseItem(item)\">\n            <p>{{ item.description }}</p>\n        </ion-item>\n    </ion-list>\n</ion-content>",
            styles: ["ion-toolbar.md ion-title{padding:0}"]
        })
    ], PlacePickerModalComponent);
    return PlacePickerModalComponent;
}());

// To prevent compiling errors
var moment = moment_;
var EventUpsertModalComponent = /** @class */ (function () {
    function EventUpsertModalComponent(localNotifSrvc, store, loadingCtrl, modalCtrl, alertCtrl, translate, toastController, moduleService) {
        var _this = this;
        this.localNotifSrvc = localNotifSrvc;
        this.store = store;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.toastController = toastController;
        this.moduleService = moduleService;
        this.destroyed$ = new Subject();
        // startTime = moment().toISOString(true);
        // endTime = moment().add(1, 'hours').toISOString(true);
        this.monthNames = moment.months().toString();
        this.evAttendees = [];
        this.validationMessages = {
            title: this.moduleService.getAppConstants().VALIDATION_MESSAGES.title,
            place: this.moduleService.getAppConstants().VALIDATION_MESSAGES.place,
            datetime_from: this.moduleService.getAppConstants().VALIDATION_MESSAGES.datetime_from,
            datetime_to: this.moduleService.getAppConstants().VALIDATION_MESSAGES.datetime_to,
        };
        this.translate.get(['GENERAL', 'FORM', 'EVENTS'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (t) { return _this.translations = t; });
    }
    EventUpsertModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.eventForm = new FormGroup({
            title: new FormControl('', Validators.required),
            place: new FormControl(),
            datetime_from: new FormControl(moment(this.period.from).toISOString(true), Validators.required),
            datetime_to: new FormControl(moment(this.period.to).add(1, 'hours').toISOString(true)),
            attendees: new FormControl(this.attendees || []),
            description: new FormControl(''),
            latitude: new FormControl(''),
            longitude: new FormControl('')
        });
        this.evAttendees = this.eventForm.get('attendees').value;
        if (this.action === 'UPDATE') {
            this.store.selectedEvent$()
                .pipe(takeUntil(this.destroyed$))
                .subscribe(function (id) {
                _this.eventId = id;
                _this.store.EventById$(id)
                    .pipe(takeUntil(_this.destroyed$))
                    .subscribe(function (m) {
                    _this.evAttendees = m.attendees.map(function (c) { return new ContactModel({
                        id: c.contactId, name: c.name, lastName: c.lastName, email: c.email, phone: c.phone
                    }); });
                    _this.eventForm.patchValue({
                        title: m.title,
                        place: m.place,
                        datetime_from: moment(m.datetimeFrom).toISOString(true),
                        datetime_to: moment(m.datetimeTo).toISOString(true),
                        attendees: _this.evAttendees,
                        description: m.description
                    });
                });
            });
        }
        else {
            // only to reset the state.success property
            this.store.EventById$(null);
        }
        this._handleLoading();
        this._handleSuccess();
        this._handleError();
    };
    EventUpsertModalComponent.prototype._handleLoading = function () {
        var _this = this;
        this.loading$ = this.store.Loading$;
        this.loading$
            .pipe(takeUntil(this.destroyed$), withLatestFrom(this.store.Error$))
            .subscribe(function (_a) {
            var _b = __read(_a, 2), loading = _b[0], error = _b[1];
            return __awaiter(_this, void 0, void 0, function () {
                var _c;
                var _this = this;
                return __generator(this, function (_d) {
                    switch (_d.label) {
                        case 0:
                            if (!loading) return [3 /*break*/, 2];
                            _c = this;
                            return [4 /*yield*/, this.loadingCtrl.create()];
                        case 1:
                            _c.loader = _d.sent();
                            this.loader.present();
                            return [3 /*break*/, 3];
                        case 2:
                            if (this.loader) {
                                this.loadingCtrl.dismiss().then(function () {
                                    _this.loader = null;
                                });
                            }
                            _d.label = 3;
                        case 3: return [2 /*return*/];
                    }
                });
            });
        });
    };
    EventUpsertModalComponent.prototype._handleError = function () {
        var _this = this;
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (e) { return __awaiter(_this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!e) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.toastController.create({
                                message: this.translations.EVENTS.ERRORS[e.after.toLowerCase()],
                                duration: 5000,
                                color: 'danger'
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        }); });
    };
    EventUpsertModalComponent.prototype._handleSuccess = function () {
        var _this = this;
        this.store.Success$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (success) { return __awaiter(_this, void 0, void 0, function () {
            var toast, momentStartTime, reminder;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!success) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.toastController.create({
                                message: this.translations.EVENTS.SUCCESS[success.after.toLowerCase()],
                                duration: 3000,
                                color: 'success'
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present().then(function () { return _this.modalCtrl.dismiss({ eventId: _this.eventId }); });
                        if (success.after === 'CREATE' || success.after === 'UPDATE') {
                            momentStartTime = moment(this.eventForm.get('datetime_from').value);
                            reminder = {
                                id: success.after === 'CREATE' ? success.data.id : this.eventId,
                                title: 'Evento próximo',
                                text: this.eventForm.get('title').value,
                                date: momentStartTime.subtract(10, 'minutes').set({ second: 0, millisecond: 0 }).toDate()
                            };
                            if (this.localNotifSrvc) {
                                if (success.after === 'CREATE') {
                                    this.localNotifSrvc.setReminder(reminder);
                                }
                                else {
                                    this.localNotifSrvc.updateReminder(reminder);
                                }
                            }
                        }
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        }); });
    };
    EventUpsertModalComponent.prototype.ngOnDestroy = function () {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    };
    EventUpsertModalComponent.prototype.upsertEvent = function () {
        this.eventForm.get('attendees')
            .patchValue(this.eventForm.get('attendees').value.map(function (c) { return ({ contact_id: c.id }); }));
        this.action === 'CREATE' ?
            this.store.createEvent(this.eventForm.value) : this.store.updateEvent(this.eventId, this.eventForm.value);
    };
    EventUpsertModalComponent.prototype.openContactPicker = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: ContactPickerModalComponent,
                            componentProps: {
                                currentAttendees: this.evAttendees.map(function (c) { return c.id; })
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, modal.onWillDismiss()];
                    case 3:
                        data = (_a.sent()).data;
                        if (data) {
                            this.eventForm.patchValue({ attendees: __spread(data) });
                            this.evAttendees = __spread(data);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    EventUpsertModalComponent.prototype.removeAttendee = function (contact) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.translate.get('EVENTS.confirmRemoveAttendee', { name: contact.name + " " + contact.lastName })
                    .pipe(takeUntil(this.destroyed$))
                    .subscribe(function (msg) { return __awaiter(_this, void 0, void 0, function () {
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.alertCtrl.create({
                                    header: this.translations.GENERAL.ACTION.confirm,
                                    message: msg,
                                    buttons: [{
                                            text: this.translations.GENERAL.ACTION.ok,
                                            handler: function () {
                                                _this.evAttendees = _this.evAttendees.filter(function (c) { return c.id !== contact.id; });
                                                _this.eventForm.patchValue({
                                                    attendees: _this.evAttendees
                                                });
                                            }
                                        }, {
                                            text: this.translations.GENERAL.ACTION.cancel,
                                            role: 'cancel',
                                            cssClass: 'primary',
                                        }]
                                })];
                            case 1:
                                (_a.sent()).present();
                                return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    EventUpsertModalComponent.prototype.showAttendeeDetails = function (contact) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: contact.name + " " + contact.lastName,
                            message: "<b>Email</b>: " + contact.email + "<br><b>" + this.translations.FORM.LABEL.phone + "</b>: " + contact.phone,
                            buttons: [{
                                    text: this.translations.GENERAL.ACTION.ok,
                                    role: 'cancel',
                                    cssClass: 'primary',
                                }]
                        })];
                    case 1:
                        (_a.sent()).present();
                        return [2 /*return*/];
                }
            });
        });
    };
    EventUpsertModalComponent.prototype.openSearchPlace = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: PlacePickerModalComponent,
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, modal.onWillDismiss()];
                    case 3:
                        data = (_a.sent()).data;
                        if (data) {
                            this.eventForm.patchValue({ place: data.description });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    EventUpsertModalComponent.prototype.close = function () {
        this.modalCtrl.dismiss( /*{someData: ...} */);
    };
    EventUpsertModalComponent.prototype.dateChange = function (value, isStart) {
        if (isStart === void 0) { isStart = true; }
        var currentStartDate = moment(this.eventForm.value.datetime_from);
        var currentEndDate = moment(this.eventForm.value.datetime_to);
        if (isStart) {
            var startValue = moment(value);
            if (startValue > currentEndDate) {
                this.eventForm.patchValue({ datetime_to: currentStartDate.add(1, 'hours').toISOString(true) });
            }
        }
        else {
            var endValue = moment(value);
            if (endValue < currentStartDate) {
                this.eventForm.patchValue({ datetime_from: currentEndDate.subtract(1, 'hours').toISOString(true) });
            }
        }
    };
    EventUpsertModalComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Optional }, { type: Inject, args: [LOCAL_NOTIFICATIONS_SERVICE,] }] },
        { type: EventsStore },
        { type: LoadingController },
        { type: ModalController },
        { type: AlertController },
        { type: TranslateService },
        { type: ToastController },
        { type: AppSettingsService }
    ]; };
    __decorate([
        Input()
    ], EventUpsertModalComponent.prototype, "period", void 0);
    __decorate([
        Input()
    ], EventUpsertModalComponent.prototype, "action", void 0);
    __decorate([
        Input()
    ], EventUpsertModalComponent.prototype, "attendees", void 0);
    EventUpsertModalComponent = __decorate([
        Component({
            selector: 'boxx-event-upsert',
            template: "<ion-header mode=\"md\" class=\"ion-no-border\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\">\n                {{'GENERAL.ACTION.'+ (action == 'CREATE' ? 'create' : 'update') | translate}} {{'GENERAL.event' |\n            translate}}\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <ion-buttons slot=\"end\">\n            <ion-button [disabled]=\"true\">\n                <ion-icon></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding-horizontal\">\n\n    <ion-list [formGroup]=\"eventForm\">\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.eventName\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-input formControlName=\"title\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.title\">\n            <div class=\"error-feedback\"\n                *ngIf=\"eventForm.get('title').hasError(validation.type) && eventForm.get('title').invalid && (eventForm.get('title').dirty || eventForm.get('title').touched)\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item custom-item ion-no-padding\">\n            <ion-label>\n                <p translate (click)=\"openSearchPlace()\">\n                    <ion-text color=\"primary\" translate>FORM.LABEL.place</ion-text>\n                    <ion-icon name=\"search-outline\" color=\"primary\" style=\"margin-left: 8px;\"></ion-icon>\n                </p>\n                <p [hidden]=\"eventForm.get('place').value\" (click)=\"openSearchPlace()\">\n                    <ion-text color=\"medium\" translate>EVENTS.tapToSearchPlace</ion-text>\n                </p>\n                <p [hidden]=\"!eventForm.get('place').value\" (click)=\"openSearchPlace()\" class=\"ion-text-wrap\">\n                    <ion-text color=\"medium\">{{eventForm.get('place').value}}</ion-text>\n                </p>\n            </ion-label>\n        </ion-item>\n        <!--<div *ngFor=\"let validation of validationMessages.place\">\n      <div class=\"error-feedback\"\n        *ngIf=\"eventForm.get('place').hasError(validation.type) && eventForm.get('place').invalid\">\n        <p>{{validation.message | translate: validation.params}}</p>\n      </div>\n    </div>-->\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                GENERAL.from\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-datetime #myStartPicker cancelText=\"{{'GENERAL.ACTION.cancel' | translate}}\" doneText=\"Ok\"\n                formControlName=\"datetime_from\" displayFormat=\"DD MMMM YYYY - HH:mm\"\n                value=\"{{eventForm.get('datetime_from').value}}\" (ionChange)=\"dateChange(myStartPicker.value)\"\n                placeholder=\"{{'GENERAL.ACTION.selectDate' | translate}}\" monthNames=\"{{monthNames}}\"></ion-datetime>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.datetime_from\">\n            <div class=\"error-feedback\"\n                *ngIf=\"eventForm.get('datetime_from').hasError(validation.type) && eventForm.get('datetime_from').invalid && (eventForm.get('datetime_from').dirty || eventForm.get('datetime_from').touched)\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                GENERAL.to\n                <!--<ion-text color=\"danger\">*</ion-text>-->\n            </ion-label>\n            <ion-datetime #myEndPicker cancelText=\"{{'GENERAL.ACTION.cancel' | translate}}\" doneText=\"Ok\"\n                formControlName=\"datetime_to\" (ionChange)=\"dateChange(myEndPicker.value, false)\"\n                displayFormat=\"DD MMMM YYYY - HH:mm\" value=\"{{eventForm.get('datetime_to').value}}\"\n                placeholder=\"{{'GENERAL.ACTION.selectDate' | translate}}\" monthNames=\"{{monthNames}}\"></ion-datetime>\n        </ion-item>\n        <!--<div *ngFor=\"let validation of validationMessages.datetime_to\">\n      <div class=\"error-feedback\"\n        *ngIf=\"eventForm.get('datetime_to').hasError(validation.type) && eventForm.get('datetime_to').invalid\">\n        <p>{{validation.message | translate: validation.params}}</p>\n      </div>\n    </div>-->\n\n        <ion-item class=\"boxx-input-item custom-item ion-no-padding\">\n            <ion-label class=\"attendee-chip\" color=\"primary\">\n                <p (click)=\"openContactPicker()\" class=\"vertical-center\">\n                    <ion-text color=\"primary\" translate>FORM.LABEL.attendees</ion-text>\n                    <ion-icon name=\"add-circle\" color=\"primary\"></ion-icon>\n                </p>\n                <p [hidden]=\"evAttendees.length > 0\" (click)=\"openContactPicker()\">\n                    <ion-text color=\"medium\" translate>EVENTS.tapToSelectContact</ion-text>\n                </p>\n                <ion-chip *ngFor=\"let contact of evAttendees\">\n                    <ion-avatar>\n                        <div>{{contact.name[0]}}{{contact.lastName[0]}}</div>\n                    </ion-avatar>\n                    <ion-label (click)=\"showAttendeeDetails(contact)\">{{contact.name}} {{contact.lastName}}</ion-label>\n                    <ion-icon name=\"close-circle-outline\" (click)=\"removeAttendee(contact)\"></ion-icon>\n                </ion-chip>\n            </ion-label>\n        </ion-item>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.description\n                <!--<ion-text color=\"danger\">*</ion-text>-->\n            </ion-label>\n            <ion-textarea formControlName=\"description\"></ion-textarea>\n        </ion-item>\n    </ion-list>\n\n    <div class=\"ion-margin-vertical\">\n        <ion-button color=\"primary\" expand=\"block\" [disabled]=\"eventForm.invalid\" (click)=\"upsertEvent()\">\n            {{(action == 'CREATE' ? 'GENERAL.ACTION.create': 'GENERAL.ACTION.update') | translate}} {{'GENERAL.event' |\n            translate}}\n        </ion-button>\n    </div>\n\n</ion-content>",
            styles: ["@charset \"UTF-8\";:host ion-toolbar.md ion-title{padding:0}:host .custom-item ion-label{padding-left:16px}:host .custom-item ion-label .attendee-chip p:first-child{display:flex;justify-content:space-between}:host .custom-item ion-label .attendee-chip p:first-child ion-icon{width:20px;height:20px}:host .custom-item ion-chip ion-label{padding-left:0}ion-label.attendee-chip p:first-child{display:flex;justify-content:space-between}ion-label.attendee-chip p:first-child ion-icon{width:20px!important;height:20px!important}"]
        }),
        __param(0, Optional()), __param(0, Inject(LOCAL_NOTIFICATIONS_SERVICE))
    ], EventUpsertModalComponent);
    return EventUpsertModalComponent;
}());

var AppVersionService = /** @class */ (function () {
    function AppVersionService(appVersion, platform) {
        var _this = this;
        this.appVersion = appVersion;
        this.platform = platform;
        this.whenReadySubject = new BehaviorSubject({
            appName: undefined,
            packageName: undefined,
            versionCode: undefined,
            versionNumber: 'PWA'
        });
        this.whenReady$ = this.whenReadySubject.asObservable();
        this.platform.ready().then(function () { return __awaiter(_this, void 0, void 0, function () {
            var _a, _b, _c, _d;
            return __generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        if (!(this.platform.is('cordova') || this.platform.is('capacitor'))) return [3 /*break*/, 5];
                        _a = this;
                        return [4 /*yield*/, this.appVersion.getAppName()];
                    case 1:
                        _a.appNameVal = _e.sent();
                        _b = this;
                        return [4 /*yield*/, this.appVersion.getPackageName()];
                    case 2:
                        _b.packageNameVal = _e.sent();
                        _c = this;
                        return [4 /*yield*/, this.appVersion.getVersionCode()];
                    case 3:
                        _c.versionCodeVal = _e.sent();
                        _d = this;
                        return [4 /*yield*/, this.appVersion.getVersionNumber()];
                    case 4:
                        _d.versionNumberVal = _e.sent();
                        _e.label = 5;
                    case 5:
                        this.whenReadySubject.next({
                            appName: this.appNameVal,
                            packageName: this.packageNameVal,
                            versionCode: this.versionCodeVal,
                            versionNumber: this.versionNumberVal
                        });
                        return [2 /*return*/];
                }
            });
        }); });
    }
    AppVersionService.prototype.appName = function () {
        return this.appNameVal;
    };
    AppVersionService.prototype.packageName = function () {
        return this.packageNameVal;
    };
    AppVersionService.prototype.versionCode = function () {
        return this.versionCodeVal;
    };
    AppVersionService.prototype.versionNumber = function () {
        return this.versionNumberVal;
    };
    AppVersionService.ctorParameters = function () { return [
        { type: AppVersion },
        { type: Platform }
    ]; };
    AppVersionService.ɵprov = ɵɵdefineInjectable({ factory: function AppVersionService_Factory() { return new AppVersionService(ɵɵinject(AppVersion$1), ɵɵinject(Platform)); }, token: AppVersionService, providedIn: "root" });
    AppVersionService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], AppVersionService);
    return AppVersionService;
}());

var NativeVersionInfoComponent = /** @class */ (function () {
    function NativeVersionInfoComponent(appVersionService) {
        this.appVersionService = appVersionService;
    }
    NativeVersionInfoComponent.prototype.ngOnInit = function () { };
    NativeVersionInfoComponent.ctorParameters = function () { return [
        { type: AppVersionService }
    ]; };
    NativeVersionInfoComponent = __decorate([
        Component({
            selector: 'boxx-native-version-info',
            template: "<div class=\"app-version-number\">\n    <div *ngIf=\"(appVersionService.whenReady$ | async) as appVersionService\">\n        {{appVersionService.versionNumber && appVersionService.versionNumber !== 'PWA' ? 'v'+appVersionService.versionNumber : 'PWA'}}\n    </div>\n</div>",
            styles: [""]
        })
    ], NativeVersionInfoComponent);
    return NativeVersionInfoComponent;
}());

var FIREBASEX_SERVICE = new InjectionToken('FIREBASEX_SERVICE');

var RemoteConfigComponent = /** @class */ (function () {
    function RemoteConfigComponent(firebasex, translate, appVersion, market, alertCtrl, platform) {
        var _this = this;
        this.firebasex = firebasex;
        this.translate = translate;
        this.appVersion = appVersion;
        this.market = market;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.whenFinish = new EventEmitter();
        this.destroyed$ = new Subject();
        this.configMessage = '...';
        this.isIos = false;
        this.translate.get(['GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (t) { return _this.translations = t; });
    }
    RemoteConfigComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.configMessage = _this.translations.GENERAL.loadingRemoteConfig;
            _this.isIos = _this.platform.is('ios');
            _this.checkForUpdates();
            _this.configMessage = '';
        });
    };
    RemoteConfigComponent.prototype.checkForUpdates = function () {
        return __awaiter(this, void 0, void 0, function () {
            var currentVersionNumber, packageName, mostRecentVersion, alert_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.appVersion.getVersionNumber()];
                    case 1:
                        currentVersionNumber = _a.sent();
                        return [4 /*yield*/, this.appVersion.getPackageName()];
                    case 2:
                        packageName = _a.sent();
                        return [4 /*yield*/, this.firebasex.fetch(600)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.firebasex.activateFetched()];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, this.firebasex.getValue('most_recent_version' + (this.isIos ? '_ios' : ''))];
                    case 5:
                        mostRecentVersion = (_a.sent()) || '0.0.1';
                        console.log('--- most_recent_version', mostRecentVersion);
                        if (!(mostRecentVersion > currentVersionNumber)) return [3 /*break*/, 7];
                        return [4 /*yield*/, this.alertCtrl.create({
                                header: this.translations.GENERAL.appUpdateHeader,
                                message: this.translations.GENERAL.appUpdateMsg + " " + mostRecentVersion,
                                buttons: [{
                                        text: this.translations.GENERAL.ACTION.update,
                                        handler: function () {
                                            _this.market.open(packageName);
                                            return false;
                                        }
                                    }],
                                backdropDismiss: false
                            })];
                    case 6:
                        alert_1 = _a.sent();
                        alert_1.present();
                        return [3 /*break*/, 8];
                    case 7:
                        this.whenFinish.emit(true);
                        _a.label = 8;
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    RemoteConfigComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [FIREBASEX_SERVICE,] }] },
        { type: TranslateService },
        { type: AppVersion },
        { type: Market },
        { type: AlertController },
        { type: Platform }
    ]; };
    __decorate([
        Output()
    ], RemoteConfigComponent.prototype, "whenFinish", void 0);
    RemoteConfigComponent = __decorate([
        Component({
            selector: 'boxx-remote-config',
            template: "<div>\n  {{configMessage}}\n</div>\n",
            styles: [""]
        }),
        __param(0, Inject(FIREBASEX_SERVICE))
    ], RemoteConfigComponent);
    return RemoteConfigComponent;
}());

var AppSettingsObject = new InjectionToken('AppSettingsObject');
function createAppSettingsService(settings) {
    return new AppSettingsService(settings);
}
var SharedModulesModule = /** @class */ (function () {
    function SharedModulesModule() {
    }
    SharedModulesModule_1 = SharedModulesModule;
    SharedModulesModule.forRoot = function (config) {
        return {
            ngModule: SharedModulesModule_1,
            providers: __spread([
                { provide: AppSettingsObject, useValue: config },
                {
                    provide: AppSettingsService,
                    useFactory: (createAppSettingsService),
                    deps: [AppSettingsObject]
                }
            ], config.providers, [
                AppVersionService,
                AppVersion,
                Market
            ])
        };
    };
    var SharedModulesModule_1;
    SharedModulesModule = SharedModulesModule_1 = __decorate([
        NgModule({
            declarations: [
                CalendarSheetComponent,
                ContactPickerModalComponent,
                ContactUpsertModalComponent,
                CountryCodeSelectorPopoverComponent,
                CountrySelectorModalComponent,
                EventUpsertModalComponent,
                PlacePickerModalComponent,
                NativeVersionInfoComponent,
                RemoteConfigComponent,
            ],
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                IonicModule,
                TranslateModule.forChild()
            ],
            exports: [
                CalendarSheetComponent,
                ContactPickerModalComponent,
                ContactUpsertModalComponent,
                CountryCodeSelectorPopoverComponent,
                CountrySelectorModalComponent,
                EventUpsertModalComponent,
                PlacePickerModalComponent,
                NativeVersionInfoComponent,
                RemoteConfigComponent,
            ]
        })
    ], SharedModulesModule);
    return SharedModulesModule;
}());

/*
 * Public API Surface of shared-modules
 */

/**
 * Generated bundle index. Do not edit.
 */

export { AppSettingsObject, AppVersionService, ContactPickerModalComponent, ContactUpsertModalComponent, CountryCodeSelectorPopoverComponent, CountrySelectorModalComponent, EventUpsertModalComponent, FIREBASEX_SERVICE, LOCAL_NOTIFICATIONS_SERVICE, PlacePickerModalComponent, RemoteConfigComponent, SharedModulesModule, createAppSettingsService, AppSettingsService as ɵa, CalendarSheetComponent as ɵb, NativeVersionInfoComponent as ɵc };
//# sourceMappingURL=boxx-shared-modules.js.map
