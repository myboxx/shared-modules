var SharedModulesModule_1;
import { __decorate } from "tslib";
import { CommonModule } from '@angular/common';
import { InjectionToken, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { CalendarSheetComponent } from './components/calendar-sheet/calendar-sheet.component';
import { ContactPickerModalComponent } from './components/contact-picker-modal/contact-picker.modal';
import { ContactUpsertModalComponent } from './components/contact-upsert-modal/contact-upsert.modal';
import { CountryCodeSelectorPopoverComponent } from './components/contact-upsert-modal/country-codes-selector.popover';
import { CountrySelectorModalComponent } from './components/contact-upsert-modal/country-selector.modal';
import { EventUpsertModalComponent } from './components/event-upsert-modal/event-upsert.modal';
import { NativeVersionInfoComponent } from './components/native-version-info/native-version-info.component';
import { PlacePickerModalComponent } from './components/place-picker-modal/place-picker.modal';
import { RemoteConfigComponent } from './components/remote-config/remote-config.component';
import { AppSettingsService } from './providers/global-params';
import { AppVersionService } from './services/NativeAppVersion.service';
export const AppSettingsObject = new InjectionToken('AppSettingsObject');
export function createAppSettingsService(settings) {
    return new AppSettingsService(settings);
}
let SharedModulesModule = SharedModulesModule_1 = class SharedModulesModule {
    static forRoot(config) {
        return {
            ngModule: SharedModulesModule_1,
            providers: [
                { provide: AppSettingsObject, useValue: config },
                {
                    provide: AppSettingsService,
                    useFactory: (createAppSettingsService),
                    deps: [AppSettingsObject]
                },
                ...config.providers,
                AppVersionService,
                AppVersion,
                Market
            ]
        };
    }
};
SharedModulesModule = SharedModulesModule_1 = __decorate([
    NgModule({
        declarations: [
            CalendarSheetComponent,
            ContactPickerModalComponent,
            ContactUpsertModalComponent,
            CountryCodeSelectorPopoverComponent,
            CountrySelectorModalComponent,
            EventUpsertModalComponent,
            PlacePickerModalComponent,
            NativeVersionInfoComponent,
            RemoteConfigComponent,
        ],
        imports: [
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            IonicModule,
            TranslateModule.forChild()
        ],
        exports: [
            CalendarSheetComponent,
            ContactPickerModalComponent,
            ContactUpsertModalComponent,
            CountryCodeSelectorPopoverComponent,
            CountrySelectorModalComponent,
            EventUpsertModalComponent,
            PlacePickerModalComponent,
            NativeVersionInfoComponent,
            RemoteConfigComponent,
        ]
    })
], SharedModulesModule);
export { SharedModulesModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLW1vZHVsZXMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkLW1vZHVsZXMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxjQUFjLEVBQXVCLFFBQVEsRUFBWSxNQUFNLGVBQWUsQ0FBQztBQUN4RixPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFbEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzNELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQzlGLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHdEQUF3RCxDQUFDO0FBQ3JHLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHdEQUF3RCxDQUFDO0FBQ3JHLE9BQU8sRUFBRSxtQ0FBbUMsRUFBRSxNQUFNLGtFQUFrRSxDQUFDO0FBQ3ZILE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDBEQUEwRCxDQUFDO0FBQ3pHLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBQy9GLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGdFQUFnRSxDQUFDO0FBQzVHLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBQy9GLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBQzNGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQy9ELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBRXhFLE1BQU0sQ0FBQyxNQUFNLGlCQUFpQixHQUFHLElBQUksY0FBYyxDQUFDLG1CQUFtQixDQUFDLENBQUM7QUFFekUsTUFBTSxVQUFVLHdCQUF3QixDQUFDLFFBQXVDO0lBQzVFLE9BQU8sSUFBSSxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUM1QyxDQUFDO0FBb0RELElBQWEsbUJBQW1CLDJCQUFoQyxNQUFhLG1CQUFtQjtJQUM1QixNQUFNLENBQUMsT0FBTyxDQUFDLE1BQXFDO1FBRWhELE9BQU87WUFDSCxRQUFRLEVBQUUscUJBQW1CO1lBQzdCLFNBQVMsRUFBRTtnQkFDUCxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFO2dCQUNoRDtvQkFDSSxPQUFPLEVBQUUsa0JBQWtCO29CQUMzQixVQUFVLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQztvQkFDdEMsSUFBSSxFQUFFLENBQUMsaUJBQWlCLENBQUM7aUJBQzVCO2dCQUNELEdBQUcsTUFBTSxDQUFDLFNBQVM7Z0JBQ25CLGlCQUFpQjtnQkFDakIsVUFBVTtnQkFDVixNQUFNO2FBQ1Q7U0FDSixDQUFDO0lBQ04sQ0FBQztDQUNILENBQUE7QUFuQlcsbUJBQW1CO0lBL0IvQixRQUFRLENBQUM7UUFDUixZQUFZLEVBQUU7WUFDWixzQkFBc0I7WUFDdEIsMkJBQTJCO1lBQzNCLDJCQUEyQjtZQUMzQixtQ0FBbUM7WUFDbkMsNkJBQTZCO1lBQzdCLHlCQUF5QjtZQUN6Qix5QkFBeUI7WUFDekIsMEJBQTBCO1lBQzFCLHFCQUFxQjtTQUN0QjtRQUNELE9BQU8sRUFBRTtZQUNMLFlBQVk7WUFDWixXQUFXO1lBQ1gsbUJBQW1CO1lBQ25CLFdBQVc7WUFDWCxlQUFlLENBQUMsUUFBUSxFQUFFO1NBQzdCO1FBQ0QsT0FBTyxFQUFFO1lBQ1Asc0JBQXNCO1lBQ3RCLDJCQUEyQjtZQUMzQiwyQkFBMkI7WUFDM0IsbUNBQW1DO1lBQ25DLDZCQUE2QjtZQUM3Qix5QkFBeUI7WUFDekIseUJBQXlCO1lBQ3pCLDBCQUEwQjtZQUMxQixxQkFBcUI7U0FDdEI7S0FDRixDQUFDO0dBQ1csbUJBQW1CLENBbUI5QjtTQW5CVyxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgSW5qZWN0aW9uVG9rZW4sIE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlLCBQcm92aWRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBJRXZlbnRSZW1pbmRlciB9IGZyb20gJ0Bib3h4L2V2ZW50cy1jb3JlJztcbmltcG9ydCB7IEFwcFZlcnNpb24gfSBmcm9tICdAaW9uaWMtbmF0aXZlL2FwcC12ZXJzaW9uL25neCc7XG5pbXBvcnQgeyBNYXJrZXQgfSBmcm9tICdAaW9uaWMtbmF0aXZlL21hcmtldC9uZ3gnO1xuaW1wb3J0IHsgSW9uaWNNb2R1bGUgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7IENhbGVuZGFyU2hlZXRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2FsZW5kYXItc2hlZXQvY2FsZW5kYXItc2hlZXQuY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRhY3RQaWNrZXJNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb250YWN0LXBpY2tlci1tb2RhbC9jb250YWN0LXBpY2tlci5tb2RhbCc7XG5pbXBvcnQgeyBDb250YWN0VXBzZXJ0TW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29udGFjdC11cHNlcnQtbW9kYWwvY29udGFjdC11cHNlcnQubW9kYWwnO1xuaW1wb3J0IHsgQ291bnRyeUNvZGVTZWxlY3RvclBvcG92ZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29udGFjdC11cHNlcnQtbW9kYWwvY291bnRyeS1jb2Rlcy1zZWxlY3Rvci5wb3BvdmVyJztcbmltcG9ydCB7IENvdW50cnlTZWxlY3Rvck1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NvbnRhY3QtdXBzZXJ0LW1vZGFsL2NvdW50cnktc2VsZWN0b3IubW9kYWwnO1xuaW1wb3J0IHsgRXZlbnRVcHNlcnRNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9ldmVudC11cHNlcnQtbW9kYWwvZXZlbnQtdXBzZXJ0Lm1vZGFsJztcbmltcG9ydCB7IE5hdGl2ZVZlcnNpb25JbmZvQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL25hdGl2ZS12ZXJzaW9uLWluZm8vbmF0aXZlLXZlcnNpb24taW5mby5jb21wb25lbnQnO1xuaW1wb3J0IHsgUGxhY2VQaWNrZXJNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9wbGFjZS1waWNrZXItbW9kYWwvcGxhY2UtcGlja2VyLm1vZGFsJztcbmltcG9ydCB7IFJlbW90ZUNvbmZpZ0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9yZW1vdGUtY29uZmlnL3JlbW90ZS1jb25maWcuY29tcG9uZW50JztcbmltcG9ydCB7IEFwcFNldHRpbmdzU2VydmljZSB9IGZyb20gJy4vcHJvdmlkZXJzL2dsb2JhbC1wYXJhbXMnO1xuaW1wb3J0IHsgQXBwVmVyc2lvblNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL05hdGl2ZUFwcFZlcnNpb24uc2VydmljZSc7XG5cbmV4cG9ydCBjb25zdCBBcHBTZXR0aW5nc09iamVjdCA9IG5ldyBJbmplY3Rpb25Ub2tlbignQXBwU2V0dGluZ3NPYmplY3QnKTtcblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZUFwcFNldHRpbmdzU2VydmljZShzZXR0aW5nczogU2hhcmVkTW9kdWxlc09wdGlvbnNJbnRlcmZhY2UpIHtcbiAgICByZXR1cm4gbmV3IEFwcFNldHRpbmdzU2VydmljZShzZXR0aW5ncyk7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgU2hhcmVkTW9kdWxlc09wdGlvbnNJbnRlcmZhY2Uge1xuICAgIGFwcENvbnN0YW50czogYW55O1xuICAgIHByb3ZpZGVyczogUHJvdmlkZXJbXTtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJTG9jYWxOb3RpZmljYXRpb25zU2VydmljZSB7XG4gICAgc2V0UmVtaW5kZXIocmVtaW5kZXI6IElFdmVudFJlbWluZGVyKTtcblxuICAgIHJlbW92ZVJlbWluZGVyKGlkOiBudW1iZXIpO1xuXG4gICAgdXBkYXRlUmVtaW5kZXIocmVtaW5kZXI6IElFdmVudFJlbWluZGVyKTtcblxuICAgIGNoZWNrSWRTY2hlZHVsZWQoaWQ6IG51bWJlcik7XG5cbiAgICBpbml0KCk7XG5cbiAgICBzdG9wKCk7XG59XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIENhbGVuZGFyU2hlZXRDb21wb25lbnQsXG4gICAgQ29udGFjdFBpY2tlck1vZGFsQ29tcG9uZW50LFxuICAgIENvbnRhY3RVcHNlcnRNb2RhbENvbXBvbmVudCxcbiAgICBDb3VudHJ5Q29kZVNlbGVjdG9yUG9wb3ZlckNvbXBvbmVudCxcbiAgICBDb3VudHJ5U2VsZWN0b3JNb2RhbENvbXBvbmVudCxcbiAgICBFdmVudFVwc2VydE1vZGFsQ29tcG9uZW50LFxuICAgIFBsYWNlUGlja2VyTW9kYWxDb21wb25lbnQsXG4gICAgTmF0aXZlVmVyc2lvbkluZm9Db21wb25lbnQsXG4gICAgUmVtb3RlQ29uZmlnQ29tcG9uZW50LFxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgICBDb21tb25Nb2R1bGUsXG4gICAgICBGb3Jtc01vZHVsZSxcbiAgICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXG4gICAgICBJb25pY01vZHVsZSxcbiAgICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JDaGlsZCgpXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBDYWxlbmRhclNoZWV0Q29tcG9uZW50LFxuICAgIENvbnRhY3RQaWNrZXJNb2RhbENvbXBvbmVudCxcbiAgICBDb250YWN0VXBzZXJ0TW9kYWxDb21wb25lbnQsXG4gICAgQ291bnRyeUNvZGVTZWxlY3RvclBvcG92ZXJDb21wb25lbnQsXG4gICAgQ291bnRyeVNlbGVjdG9yTW9kYWxDb21wb25lbnQsXG4gICAgRXZlbnRVcHNlcnRNb2RhbENvbXBvbmVudCxcbiAgICBQbGFjZVBpY2tlck1vZGFsQ29tcG9uZW50LFxuICAgIE5hdGl2ZVZlcnNpb25JbmZvQ29tcG9uZW50LFxuICAgIFJlbW90ZUNvbmZpZ0NvbXBvbmVudCxcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBTaGFyZWRNb2R1bGVzTW9kdWxlIHtcbiAgICBzdGF0aWMgZm9yUm9vdChjb25maWc6IFNoYXJlZE1vZHVsZXNPcHRpb25zSW50ZXJmYWNlKTogTW9kdWxlV2l0aFByb3ZpZGVyczxTaGFyZWRNb2R1bGVzTW9kdWxlPiB7XG5cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIG5nTW9kdWxlOiBTaGFyZWRNb2R1bGVzTW9kdWxlLFxuICAgICAgICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgICAgICAgICAgeyBwcm92aWRlOiBBcHBTZXR0aW5nc09iamVjdCwgdXNlVmFsdWU6IGNvbmZpZyB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgcHJvdmlkZTogQXBwU2V0dGluZ3NTZXJ2aWNlLFxuICAgICAgICAgICAgICAgICAgICB1c2VGYWN0b3J5OiAoY3JlYXRlQXBwU2V0dGluZ3NTZXJ2aWNlKSxcbiAgICAgICAgICAgICAgICAgICAgZGVwczogW0FwcFNldHRpbmdzT2JqZWN0XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgLi4uY29uZmlnLnByb3ZpZGVycyxcbiAgICAgICAgICAgICAgICBBcHBWZXJzaW9uU2VydmljZSxcbiAgICAgICAgICAgICAgICBBcHBWZXJzaW9uLFxuICAgICAgICAgICAgICAgIE1hcmtldFxuICAgICAgICAgICAgXVxuICAgICAgICB9O1xuICAgIH1cbiB9XG4iXX0=