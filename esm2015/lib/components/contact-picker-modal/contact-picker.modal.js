import { __awaiter, __decorate } from "tslib";
import { Component, Input, ViewChild } from '@angular/core';
import { ContactModel, ContactStore } from '@boxx/contacts-core';
import { AlertController, IonSearchbar, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
let ContactPickerModalComponent = class ContactPickerModalComponent {
    constructor(modalCtrl, store, alertCtrl, translate) {
        this.modalCtrl = modalCtrl;
        this.store = store;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.searchValue = '';
        this.contactList = [];
        this.isFiltering = false;
        this.destroyed$ = new Subject();
        this.translate.get(['CONTACTS', 'GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t) => this.translations = t);
    }
    ngOnInit() {
        this.isLoading$ = this.store.Loading$;
        this.store.Contacts$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(cl => {
            this.searchResults = this.contactList = cl.map(c => ({ isChecked: this.currentAttendees.findIndex((e) => e === c.id) !== -1, contact: c }));
            setTimeout(() => {
                this.searchBar.setFocus();
            }, 800);
        });
        this.store.HasBeenFetched$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(ok => {
            if (!ok) {
                this.store.fetchContacts();
            }
        });
    }
    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
    checkMaster() {
        setTimeout(() => {
            this.searchResults.forEach(obj => {
                obj.isChecked = this.masterCheck;
            });
        });
    }
    checkEvent() {
        const totalItems = this.searchResults.length;
        let checked = 0;
        this.searchResults.map(obj => {
            if (obj.isChecked) {
                checked++;
            }
        });
        if (checked > 0 && checked < totalItems) {
            // If even one item is checked but not all
            this.isIndeterminate = true;
            this.masterCheck = false;
        }
        else if (checked === totalItems) {
            // If all are checked
            this.masterCheck = true;
            this.isIndeterminate = false;
        }
        else {
            // If none is checked
            this.isIndeterminate = false;
            this.masterCheck = false;
        }
    }
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.hasChanges()) {
                (yield this.alertCtrl.create({
                    header: this.translations.GENERAL.ACTION.confirm,
                    message: this.translations.CONTACTS.confirmCloseWithSelectedContactsMsg,
                    buttons: [{
                            text: this.translations.GENERAL.ACTION.ok,
                            handler: () => {
                                this.modalCtrl.dismiss( /*{someData: ...} */);
                            }
                        }, {
                            text: this.translations.GENERAL.ACTION.cancel,
                            role: 'cancel',
                            cssClass: 'primary',
                        },]
                })).present();
            }
            else {
                this.modalCtrl.dismiss( /*{someData: ...} */);
            }
        });
    }
    select() {
        return this.modalCtrl.dismiss(this._getSelectedContacts());
    }
    _getSelectedContacts() {
        return this.searchResults.filter(c => c.isChecked).map(c => c.contact);
    }
    hasChanges() {
        return this._getSelectedContacts().length !== this.currentAttendees.length ||
            this._getSelectedContacts()
                .filter(c => !this.currentAttendees.includes(c.id)).length > 0;
    }
    search(searchTerm) {
        this.searchResults = this.contactList;
        if (!searchTerm) {
            this.isFiltering = false;
            return;
        }
        this.isFiltering = true;
        this.searchResults = this.searchResults.filter(item => {
            return (item.contact.name + item.contact.lastName).toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
                item.contact.phone.indexOf(searchTerm) > -1 ||
                item.contact.email.indexOf(searchTerm) > -1;
        });
    }
    showAll() {
        this.searchValue = ' ';
        setTimeout(() => {
            this.searchValue = '';
        }, 10);
    }
};
ContactPickerModalComponent.ctorParameters = () => [
    { type: ModalController },
    { type: ContactStore },
    { type: AlertController },
    { type: TranslateService }
];
__decorate([
    ViewChild('contactPickerSB', { static: false })
], ContactPickerModalComponent.prototype, "searchBar", void 0);
__decorate([
    Input()
], ContactPickerModalComponent.prototype, "currentAttendees", void 0);
ContactPickerModalComponent = __decorate([
    Component({
        selector: 'boxx-contact-picker-modal',
        template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                GENERAL.contacts\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\" [hidden]=\"isFiltering\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <ion-buttons slot=\"end\" [hidden]=\"isFiltering || !hasChanges()\">\n            <ion-button (click)=\"select()\" translate>\n                <ion-icon name=\"checkmark-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <ion-buttons slot=\"end\" [hidden]=\"!isFiltering\">\n            <ion-button (click)=\"showAll()\" translate>\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #contactPickerSB showCancelButton=\"never\" cancelButtonIcon=\"close-circle-outline\"\n        (ionChange)=\"search($event.detail.value)\" [value]=\"searchValue\"\n        placeholder=\"{{'CONTACTS.searchPlaceholder'| translate}}\"></ion-searchbar>\n    </ion-item-divider>\n\n    <div *ngIf=\"isLoading$ | async\" class=\"ion-padding\">\n        <ion-item class=\"ion-no-padding\">\n            <ion-label>\n                <ion-skeleton-text animated style=\"width: 40%\"></ion-skeleton-text>\n            </ion-label>\n            <ion-thumbnail slot=\"start\" style=\"width: 20px; height: 20px; margin-right: 30px; margin-left: 4px;\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n            </ion-thumbnail>\n        </ion-item>\n        <ion-list>\n            <ion-item class=\"ion-no-padding\" *ngFor=\"let item of [].constructor(5)\">\n                <ion-label>\n                    <ion-skeleton-text animated style=\"width: 80%\"></ion-skeleton-text>\n                    <ion-skeleton-text animated style=\"width: 60%\"></ion-skeleton-text>\n                </ion-label>\n                <ion-thumbnail slot=\"start\" style=\"width: 20px; height: 20px; margin-right: 30px; margin-left: 4px;\">\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                </ion-thumbnail>\n            </ion-item>\n        </ion-list>\n    </div>\n\n    <ion-item [hidden]=\"(isLoading$ | async) || isFiltering\" class=\"padding-right\">\n        <ion-label>\n            <strong>{{\"GENERAL.total\" | translate}}: {{contactList.length}}\n                {{ (contactList.length == 1 ? \"GENERAL.contact\" : \"GENERAL.contacts\") | translate}}\n            </strong>\n        </ion-label>\n        <ion-checkbox mode=\"ios\" slot=\"start\" [(ngModel)]=\"masterCheck\" [indeterminate]=\"isIndeterminate\"\n            (click)=\"checkMaster()\"></ion-checkbox>\n    </ion-item>\n\n    <ion-list class=\"ion-padding\">\n        <ion-item class=\"ion-no-padding\" *ngFor=\"let item of searchResults\">\n            <ion-label>\n                <h3>{{item.contact.name}} {{item.contact.lastName}}</h3>\n                <p>{{item.contact.email}}</p>\n                <p>{{item.contact.phone}}</p>\n            </ion-label>\n            <ion-checkbox mode=\"ios\" slot=\"start\" [(ngModel)]=\"item.isChecked\" (ionChange)=\"checkEvent()\">\n            </ion-checkbox>\n        </ion-item>\n    </ion-list>\n</ion-content>",
        styles: ["ion-toolbar.md ion-title{padding:0}"]
    })
], ContactPickerModalComponent);
export { ContactPickerModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdC1waWNrZXIubW9kYWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtbW9kdWxlcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvbnRhY3QtcGlja2VyLW1vZGFsL2NvbnRhY3QtcGlja2VyLm1vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBcUIsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDakUsT0FBTyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDaEYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkQsT0FBTyxFQUFjLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMzQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFRM0MsSUFBYSwyQkFBMkIsR0FBeEMsTUFBYSwyQkFBMkI7SUFLcEMsWUFDWSxTQUEwQixFQUMxQixLQUFtQixFQUNuQixTQUEwQixFQUMxQixTQUEyQjtRQUgzQixjQUFTLEdBQVQsU0FBUyxDQUFpQjtRQUMxQixVQUFLLEdBQUwsS0FBSyxDQUFjO1FBQ25CLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBQzFCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBT3ZDLGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLGdCQUFXLEdBQXlELEVBQUUsQ0FBQztRQUd2RSxnQkFBVyxHQUFHLEtBQUssQ0FBQztRQUlwQixlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztRQWJoQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsQ0FBQzthQUN0QyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTLENBQUMsQ0FBQyxDQUFNLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQWVELFFBQVE7UUFFSixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1FBRXRDLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUzthQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUNaLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQy9DLENBQUMsRUFBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FDekYsQ0FBQztZQUNGLFVBQVUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ1osSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUM5QixDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQztRQUVQLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZTthQUNyQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDWixJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUNMLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLENBQUM7YUFDOUI7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQsV0FBVztRQUNQLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDWixJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDN0IsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ3JDLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsVUFBVTtRQUNOLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO1FBQzdDLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQztRQUNoQixJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN6QixJQUFJLEdBQUcsQ0FBQyxTQUFTLEVBQUU7Z0JBQUUsT0FBTyxFQUFFLENBQUM7YUFBRTtRQUNyQyxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksT0FBTyxHQUFHLENBQUMsSUFBSSxPQUFPLEdBQUcsVUFBVSxFQUFFO1lBQ3JDLDBDQUEwQztZQUMxQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztZQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztTQUM1QjthQUFNLElBQUksT0FBTyxLQUFLLFVBQVUsRUFBRTtZQUMvQixxQkFBcUI7WUFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7U0FDaEM7YUFBTTtZQUNILHFCQUFxQjtZQUNyQixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztTQUM1QjtJQUNMLENBQUM7SUFFSyxLQUFLOztZQUNQLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFO2dCQUNuQixDQUFDLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7b0JBQ3pCLE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTztvQkFDaEQsT0FBTyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLG1DQUFtQztvQkFDdkUsT0FBTyxFQUFFLENBQUM7NEJBQ04sSUFBSSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFOzRCQUN6QyxPQUFPLEVBQUUsR0FBRyxFQUFFO2dDQUNWLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFDLG9CQUFvQixDQUFDLENBQUM7NEJBQ2pELENBQUM7eUJBQ0osRUFBRTs0QkFDQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU07NEJBQzdDLElBQUksRUFBRSxRQUFROzRCQUNkLFFBQVEsRUFBRSxTQUFTO3lCQUN0QixFQUFHO2lCQUNQLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ2pCO2lCQUNJO2dCQUNELElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFDLG9CQUFvQixDQUFDLENBQUM7YUFDaEQ7UUFDTCxDQUFDO0tBQUE7SUFFRCxNQUFNO1FBQ0YsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFTyxvQkFBb0I7UUFDeEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDM0UsQ0FBQztJQUVELFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTTtZQUN0RSxJQUFJLENBQUMsb0JBQW9CLEVBQUU7aUJBQ3RCLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQzNFLENBQUM7SUFFRCxNQUFNLENBQUMsVUFBa0I7UUFFckIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBRXRDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDYixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztZQUN6QixPQUFPO1NBQ1Y7UUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUV4QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2xELE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ25HLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzNDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNwRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7UUFDdkIsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNaLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQzFCLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNYLENBQUM7Q0FDSixDQUFBOztZQTdJMEIsZUFBZTtZQUNuQixZQUFZO1lBQ1IsZUFBZTtZQUNmLGdCQUFnQjs7QUFSVTtJQUFoRCxTQUFTLENBQUMsaUJBQWlCLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7OERBQXlCO0FBRWhFO0lBQVIsS0FBSyxFQUFFO3FFQUE0QjtBQUgzQiwyQkFBMkI7SUFMdkMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLDJCQUEyQjtRQUNyQyxncUhBQTBDOztLQUU3QyxDQUFDO0dBQ1csMkJBQTJCLENBbUp2QztTQW5KWSwyQkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkRlc3Ryb3ksIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb250YWN0TW9kZWwsIENvbnRhY3RTdG9yZSB9IGZyb20gJ0Bib3h4L2NvbnRhY3RzLWNvcmUnO1xuaW1wb3J0IHsgQWxlcnRDb250cm9sbGVyLCBJb25TZWFyY2hiYXIsIE1vZGFsQ29udHJvbGxlciB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcblxuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtY29udGFjdC1waWNrZXItbW9kYWwnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9jb250YWN0LXBpY2tlci5tb2RhbC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9jb250YWN0LXBpY2tlci5tb2RhbC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQ29udGFjdFBpY2tlck1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICAgIEBWaWV3Q2hpbGQoJ2NvbnRhY3RQaWNrZXJTQicsIHsgc3RhdGljOiBmYWxzZSB9KSBzZWFyY2hCYXI6IElvblNlYXJjaGJhcjtcblxuICAgIEBJbnB1dCgpIGN1cnJlbnRBdHRlbmRlZXM6IG51bWJlcltdO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgbW9kYWxDdHJsOiBNb2RhbENvbnRyb2xsZXIsXG4gICAgICAgIHByaXZhdGUgc3RvcmU6IENvbnRhY3RTdG9yZSxcbiAgICAgICAgcHJpdmF0ZSBhbGVydEN0cmw6IEFsZXJ0Q29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2VcbiAgICApIHtcbiAgICAgICAgdGhpcy50cmFuc2xhdGUuZ2V0KFsnQ09OVEFDVFMnLCAnR0VORVJBTCddKVxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuZGVzdHJveWVkJCkpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKCh0OiBhbnkpID0+IHRoaXMudHJhbnNsYXRpb25zID0gdCk7XG4gICAgfVxuXG4gICAgc2VhcmNoVmFsdWUgPSAnJztcbiAgICBjb250YWN0TGlzdDogQXJyYXk8eyBpc0NoZWNrZWQ6IGJvb2xlYW4sIGNvbnRhY3Q6IENvbnRhY3RNb2RlbCB9PiA9IFtdO1xuICAgIHNlYXJjaFJlc3VsdHM6IEFycmF5PHsgaXNDaGVja2VkOiBib29sZWFuLCBjb250YWN0OiBDb250YWN0TW9kZWwgfT47XG5cbiAgICBpc0ZpbHRlcmluZyA9IGZhbHNlO1xuICAgIGlzSW5kZXRlcm1pbmF0ZTogYm9vbGVhbjtcbiAgICBtYXN0ZXJDaGVjazogYm9vbGVhbjtcblxuICAgIGRlc3Ryb3llZCQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xuICAgIGlzTG9hZGluZyQ6IE9ic2VydmFibGU8Ym9vbGVhbj47XG5cbiAgICB0cmFuc2xhdGlvbnM6IGFueTtcblxuICAgIG5nT25Jbml0KCkge1xuXG4gICAgICAgIHRoaXMuaXNMb2FkaW5nJCA9IHRoaXMuc3RvcmUuTG9hZGluZyQ7XG5cbiAgICAgICAgdGhpcy5zdG9yZS5Db250YWN0cyRcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLmRlc3Ryb3llZCQpKVxuICAgICAgICAgICAgLnN1YnNjcmliZShjbCA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hSZXN1bHRzID0gdGhpcy5jb250YWN0TGlzdCA9IGNsLm1hcChjID0+XG4gICAgICAgICAgICAgICAgICAgICh7IGlzQ2hlY2tlZDogdGhpcy5jdXJyZW50QXR0ZW5kZWVzLmZpbmRJbmRleCgoZSkgPT4gZSA9PT0gYy5pZCkgIT09IC0xLCBjb250YWN0OiBjIH0pXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hCYXIuc2V0Rm9jdXMoKTtcbiAgICAgICAgICAgICAgICB9LCA4MDApO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy5zdG9yZS5IYXNCZWVuRmV0Y2hlZCRcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLmRlc3Ryb3llZCQpKVxuICAgICAgICAgICAgLnN1YnNjcmliZShvayA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKCFvaykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0b3JlLmZldGNoQ29udGFjdHMoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpIHtcbiAgICAgICAgdGhpcy5kZXN0cm95ZWQkLm5leHQodHJ1ZSk7XG4gICAgICAgIHRoaXMuZGVzdHJveWVkJC5jb21wbGV0ZSgpO1xuICAgIH1cblxuICAgIGNoZWNrTWFzdGVyKCkge1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoUmVzdWx0cy5mb3JFYWNoKG9iaiA9PiB7XG4gICAgICAgICAgICAgICAgb2JqLmlzQ2hlY2tlZCA9IHRoaXMubWFzdGVyQ2hlY2s7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgY2hlY2tFdmVudCgpIHtcbiAgICAgICAgY29uc3QgdG90YWxJdGVtcyA9IHRoaXMuc2VhcmNoUmVzdWx0cy5sZW5ndGg7XG4gICAgICAgIGxldCBjaGVja2VkID0gMDtcbiAgICAgICAgdGhpcy5zZWFyY2hSZXN1bHRzLm1hcChvYmogPT4ge1xuICAgICAgICAgICAgaWYgKG9iai5pc0NoZWNrZWQpIHsgY2hlY2tlZCsrOyB9XG4gICAgICAgIH0pO1xuICAgICAgICBpZiAoY2hlY2tlZCA+IDAgJiYgY2hlY2tlZCA8IHRvdGFsSXRlbXMpIHtcbiAgICAgICAgICAgIC8vIElmIGV2ZW4gb25lIGl0ZW0gaXMgY2hlY2tlZCBidXQgbm90IGFsbFxuICAgICAgICAgICAgdGhpcy5pc0luZGV0ZXJtaW5hdGUgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy5tYXN0ZXJDaGVjayA9IGZhbHNlO1xuICAgICAgICB9IGVsc2UgaWYgKGNoZWNrZWQgPT09IHRvdGFsSXRlbXMpIHtcbiAgICAgICAgICAgIC8vIElmIGFsbCBhcmUgY2hlY2tlZFxuICAgICAgICAgICAgdGhpcy5tYXN0ZXJDaGVjayA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLmlzSW5kZXRlcm1pbmF0ZSA9IGZhbHNlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy8gSWYgbm9uZSBpcyBjaGVja2VkXG4gICAgICAgICAgICB0aGlzLmlzSW5kZXRlcm1pbmF0ZSA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5tYXN0ZXJDaGVjayA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgYXN5bmMgY2xvc2UoKSB7XG4gICAgICAgIGlmICh0aGlzLmhhc0NoYW5nZXMoKSkge1xuICAgICAgICAgICAgKGF3YWl0IHRoaXMuYWxlcnRDdHJsLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgaGVhZGVyOiB0aGlzLnRyYW5zbGF0aW9ucy5HRU5FUkFMLkFDVElPTi5jb25maXJtLFxuICAgICAgICAgICAgICAgIG1lc3NhZ2U6IHRoaXMudHJhbnNsYXRpb25zLkNPTlRBQ1RTLmNvbmZpcm1DbG9zZVdpdGhTZWxlY3RlZENvbnRhY3RzTXNnLFxuICAgICAgICAgICAgICAgIGJ1dHRvbnM6IFt7XG4gICAgICAgICAgICAgICAgICAgIHRleHQ6IHRoaXMudHJhbnNsYXRpb25zLkdFTkVSQUwuQUNUSU9OLm9rLFxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVyOiAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZGFsQ3RybC5kaXNtaXNzKC8qe3NvbWVEYXRhOiAuLi59ICovKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICAgICAgdGV4dDogdGhpcy50cmFuc2xhdGlvbnMuR0VORVJBTC5BQ1RJT04uY2FuY2VsLFxuICAgICAgICAgICAgICAgICAgICByb2xlOiAnY2FuY2VsJyxcbiAgICAgICAgICAgICAgICAgICAgY3NzQ2xhc3M6ICdwcmltYXJ5JyxcbiAgICAgICAgICAgICAgICB9LCBdXG4gICAgICAgICAgICB9KSkucHJlc2VudCgpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5tb2RhbEN0cmwuZGlzbWlzcygvKntzb21lRGF0YTogLi4ufSAqLyk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzZWxlY3QoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLm1vZGFsQ3RybC5kaXNtaXNzKHRoaXMuX2dldFNlbGVjdGVkQ29udGFjdHMoKSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfZ2V0U2VsZWN0ZWRDb250YWN0cygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VhcmNoUmVzdWx0cy5maWx0ZXIoYyA9PiBjLmlzQ2hlY2tlZCkubWFwKGMgPT4gYy5jb250YWN0KTtcbiAgICB9XG5cbiAgICBoYXNDaGFuZ2VzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fZ2V0U2VsZWN0ZWRDb250YWN0cygpLmxlbmd0aCAhPT0gdGhpcy5jdXJyZW50QXR0ZW5kZWVzLmxlbmd0aCB8fFxuICAgICAgICAgICAgdGhpcy5fZ2V0U2VsZWN0ZWRDb250YWN0cygpXG4gICAgICAgICAgICAgICAgLmZpbHRlcihjID0+ICF0aGlzLmN1cnJlbnRBdHRlbmRlZXMuaW5jbHVkZXMoYy5pZCkpLmxlbmd0aCA+IDA7XG4gICAgfVxuXG4gICAgc2VhcmNoKHNlYXJjaFRlcm06IHN0cmluZykge1xuXG4gICAgICAgIHRoaXMuc2VhcmNoUmVzdWx0cyA9IHRoaXMuY29udGFjdExpc3Q7XG5cbiAgICAgICAgaWYgKCFzZWFyY2hUZXJtKSB7XG4gICAgICAgICAgICB0aGlzLmlzRmlsdGVyaW5nID0gZmFsc2U7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmlzRmlsdGVyaW5nID0gdHJ1ZTtcblxuICAgICAgICB0aGlzLnNlYXJjaFJlc3VsdHMgPSB0aGlzLnNlYXJjaFJlc3VsdHMuZmlsdGVyKGl0ZW0gPT4ge1xuICAgICAgICAgICAgcmV0dXJuIChpdGVtLmNvbnRhY3QubmFtZSArIGl0ZW0uY29udGFjdC5sYXN0TmFtZSkudG9Mb3dlckNhc2UoKS5pbmRleE9mKHNlYXJjaFRlcm0udG9Mb3dlckNhc2UoKSkgPiAtMSB8fFxuICAgICAgICAgICAgICAgIGl0ZW0uY29udGFjdC5waG9uZS5pbmRleE9mKHNlYXJjaFRlcm0pID4gLTEgfHxcbiAgICAgICAgICAgICAgICBpdGVtLmNvbnRhY3QuZW1haWwuaW5kZXhPZihzZWFyY2hUZXJtKSA+IC0xO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBzaG93QWxsKCkge1xuICAgICAgICB0aGlzLnNlYXJjaFZhbHVlID0gJyAnO1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoVmFsdWUgPSAnJztcbiAgICAgICAgfSwgMTApO1xuICAgIH1cbn1cbiJdfQ==