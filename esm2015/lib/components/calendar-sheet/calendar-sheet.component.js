import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let CalendarSheetComponent = class CalendarSheetComponent {
    constructor() { }
    ngOnInit() {
        this.dayName = this.dayName.toUpperCase().replace('.', '');
        this.monthName = this.monthName.toUpperCase().replace('.', '');
    }
};
__decorate([
    Input()
], CalendarSheetComponent.prototype, "dayName", void 0);
__decorate([
    Input()
], CalendarSheetComponent.prototype, "monthDay", void 0);
__decorate([
    Input()
], CalendarSheetComponent.prototype, "monthName", void 0);
__decorate([
    Input()
], CalendarSheetComponent.prototype, "headerIonColor", void 0);
CalendarSheetComponent = __decorate([
    Component({
        selector: 'boxx-calendar-sheet',
        template: "<ion-card>\n    <ion-card-header color=\"{{headerIonColor || 'primary'}}\">\n        <ion-card-subtitle>{{dayName}}</ion-card-subtitle>\n    </ion-card-header>\n\n    <ion-card-content>\n        <p>\n            <strong>{{monthDay}}</strong>\n        </p>\n        <p>{{monthName}}</p>\n    </ion-card-content>\n</ion-card>",
        styles: [":host{margin:0}:host ion-card{line-height:1.5;padding-right:0;border-radius:8px;margin-left:0;margin-right:17px}:host ion-card.ios{margin:8px 17px 8px 0}:host ion-card-header{padding:0;text-align:center}:host ion-card-content{min-width:40px;text-align:center;font-size:x-small;line-height:1;padding:2px 8px 4px}:host ion-card-content strong{font-size:16px}:host ion-card-content p{padding:0;font-size:12px;font-weight:600}:host .card-content-md p{line-height:1.2;margin-bottom:0}:host ion-card-subtitle{margin-bottom:0;padding:2px 0;font-size:small}"]
    })
], CalendarSheetComponent);
export { CalendarSheetComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FsZW5kYXItc2hlZXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jYWxlbmRhci1zaGVldC9jYWxlbmRhci1zaGVldC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT3pELElBQWEsc0JBQXNCLEdBQW5DLE1BQWEsc0JBQXNCO0lBTy9CLGdCQUFnQixDQUFDO0lBRWpCLFFBQVE7UUFDSixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNuRSxDQUFDO0NBRUosQ0FBQTtBQWJZO0lBQVIsS0FBSyxFQUFFO3VEQUFpQjtBQUNoQjtJQUFSLEtBQUssRUFBRTt3REFBa0I7QUFDakI7SUFBUixLQUFLLEVBQUU7eURBQW1CO0FBRWxCO0lBQVIsS0FBSyxFQUFFOzhEQUF3QjtBQUx2QixzQkFBc0I7SUFMbEMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLHFCQUFxQjtRQUMvQiwrVUFBOEM7O0tBRWpELENBQUM7R0FDVyxzQkFBc0IsQ0FjbEM7U0FkWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LWNhbGVuZGFyLXNoZWV0JyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2FsZW5kYXItc2hlZXQuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL2NhbGVuZGFyLXNoZWV0LmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIENhbGVuZGFyU2hlZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIEBJbnB1dCgpIGRheU5hbWU6IHN0cmluZztcbiAgICBASW5wdXQoKSBtb250aERheTogc3RyaW5nO1xuICAgIEBJbnB1dCgpIG1vbnRoTmFtZTogc3RyaW5nO1xuXG4gICAgQElucHV0KCkgaGVhZGVySW9uQ29sb3I6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5kYXlOYW1lID0gdGhpcy5kYXlOYW1lLnRvVXBwZXJDYXNlKCkucmVwbGFjZSgnLicsICcnKTtcbiAgICAgICAgdGhpcy5tb250aE5hbWUgPSB0aGlzLm1vbnRoTmFtZS50b1VwcGVyQ2FzZSgpLnJlcGxhY2UoJy4nLCAnJyk7XG4gICAgfVxuXG59XG4iXX0=