import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
let CountrySelectorModalComponent = class CountrySelectorModalComponent {
    constructor(popoverController) {
        this.popoverController = popoverController;
        this.countries = [];
        this.items = [];
    }
    ngOnInit() {
        this.items = this.countries;
    }
    select(country) {
        this.popoverController.dismiss(country);
    }
    search(searchTerm) {
        this.items = this.countries.filter(country => {
            return (country.translations.es || country.name || '')
                .toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') // replace accented chars
                .indexOf(searchTerm.toLowerCase()) > -1;
        });
    }
    close() {
        this.popoverController.dismiss();
    }
};
CountrySelectorModalComponent.ctorParameters = () => [
    { type: ModalController }
];
__decorate([
    Input()
], CountrySelectorModalComponent.prototype, "countries", void 0);
CountrySelectorModalComponent = __decorate([
    Component({
        selector: 'boxx-country-selector-modal',
        template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                GENERAL.ACTION.selectCountry\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #CountryPicker showCancelButton=\"never\" cancelButtonIcon=\"close-circle-outline\"\n            (ionChange)=\"search($event.detail.value)\" placeholder=\"{{'GENERAL.ACTION.searchCountry' | translate}}\"></ion-searchbar>\n    </ion-item-divider>\n\n    <ion-virtual-scroll [items]=\"items\" approxItemHeight=\"64px\">\n        <ion-item *virtualItem=\"let country\" (click)=\"select(country)\">\n            <ion-label>\n                <ion-img [src]=\"country.flag\" [alt]=\"country.name\" style=\"width: 24px; display: inline-block;\">\n                </ion-img>\n                {{country.translations.es || country.name}}\n            </ion-label>\n        </ion-item>\n    </ion-virtual-scroll>\n</ion-content>",
        styles: ["ion-toolbar.md ion-title{padding:0}"]
    })
], CountrySelectorModalComponent);
export { CountrySelectorModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRyeS1zZWxlY3Rvci5tb2RhbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3NoYXJlZC1tb2R1bGVzLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29udGFjdC11cHNlcnQtbW9kYWwvY291bnRyeS1zZWxlY3Rvci5tb2RhbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBU2pELElBQWEsNkJBQTZCLEdBQTFDLE1BQWEsNkJBQTZCO0lBR3RDLFlBQ1csaUJBQWtDO1FBQWxDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBaUI7UUFIcEMsY0FBUyxHQUF5QixFQUFFLENBQUM7UUFNOUMsVUFBSyxHQUFHLEVBQUUsQ0FBQztJQUZQLENBQUM7SUFJTCxRQUFRO1FBQ0osSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ2hDLENBQUM7SUFFRCxNQUFNLENBQUMsT0FBc0I7UUFDekIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsTUFBTSxDQUFDLFVBQWtCO1FBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDekMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsRUFBRSxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDO2lCQUNqRCxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLGtCQUFrQixFQUFFLEVBQUUsQ0FBQyxDQUFDLHlCQUF5QjtpQkFDeEYsT0FBTyxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2hELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELEtBQUs7UUFDRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDckMsQ0FBQztDQUNKLENBQUE7O1lBeEJpQyxlQUFlOztBQUhwQztJQUFSLEtBQUssRUFBRTtnRUFBc0M7QUFEckMsNkJBQTZCO0lBTHpDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSw2QkFBNkI7UUFDdkMsMjlDQUE0Qzs7S0FFL0MsQ0FBQztHQUNXLDZCQUE2QixDQTRCekM7U0E1QlksNkJBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNb2RhbENvbnRyb2xsZXIgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBJQ291bnRyeUNvZGVzIH0gZnJvbSAnQGJveHgvY29udGFjdHMtY29yZSc7XG5cblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LWNvdW50cnktc2VsZWN0b3ItbW9kYWwnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9jb3VudHJ5LXNlbGVjdG9yLm1vZGFsLmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL2NvdW50cnktc2VsZWN0b3IubW9kYWwuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIENvdW50cnlTZWxlY3Rvck1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBASW5wdXQoKSBjb3VudHJpZXM6IEFycmF5PElDb3VudHJ5Q29kZXM+ID0gW107XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHVibGljIHBvcG92ZXJDb250cm9sbGVyOiBNb2RhbENvbnRyb2xsZXJcbiAgICApIHsgfVxuXG4gICAgaXRlbXMgPSBbXTtcblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLml0ZW1zID0gdGhpcy5jb3VudHJpZXM7XG4gICAgfVxuXG4gICAgc2VsZWN0KGNvdW50cnk6IElDb3VudHJ5Q29kZXMpIHtcbiAgICAgICAgdGhpcy5wb3BvdmVyQ29udHJvbGxlci5kaXNtaXNzKGNvdW50cnkpO1xuICAgIH1cblxuICAgIHNlYXJjaChzZWFyY2hUZXJtOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5pdGVtcyA9IHRoaXMuY291bnRyaWVzLmZpbHRlcihjb3VudHJ5ID0+IHtcbiAgICAgICAgICAgIHJldHVybiAoY291bnRyeS50cmFuc2xhdGlvbnMuZXMgfHwgY291bnRyeS5uYW1lIHx8ICcnKVxuICAgICAgICAgICAgICAgIC50b0xvd2VyQ2FzZSgpLm5vcm1hbGl6ZSgnTkZEJykucmVwbGFjZSgvW1xcdTAzMDAtXFx1MDM2Zl0vZywgJycpIC8vIHJlcGxhY2UgYWNjZW50ZWQgY2hhcnNcbiAgICAgICAgICAgICAgICAuaW5kZXhPZihzZWFyY2hUZXJtLnRvTG93ZXJDYXNlKCkpID4gLTE7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGNsb3NlKCkge1xuICAgICAgICB0aGlzLnBvcG92ZXJDb250cm9sbGVyLmRpc21pc3MoKTtcbiAgICB9XG59XG4iXX0=