import { __awaiter, __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ContactModel, ContactStore, COUNTRY_CALLING_CODES, ICountryCodes } from '@boxx/contacts-core';
import { LoadingController, ModalController, PopoverController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppSettingsService } from '../../providers/global-params';
import { CountryCodeSelectorPopoverComponent } from './country-codes-selector.popover';
import { CountrySelectorModalComponent } from './country-selector.modal';
let ContactUpsertModalComponent = class ContactUpsertModalComponent {
    constructor(modalCtrl, store, translate, popoverController, mdalCtrl, loadingCtrl, moduleService) {
        this.modalCtrl = modalCtrl;
        this.store = store;
        this.translate = translate;
        this.popoverController = popoverController;
        this.mdalCtrl = mdalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.moduleService = moduleService;
        this.destroyed$ = new Subject();
        this.types = [
            { key: 'CLIENT', value: 'Client' },
            { key: 'PROSPECT', value: 'Prospect' },
            { key: 'NOT_SPECIFIED', value: 'Not qualified' }
        ];
        this.validationMessages = {
            name: this.moduleService.getAppConstants().VALIDATION_MESSAGES.name,
            last_name: this.moduleService.getAppConstants().VALIDATION_MESSAGES.last_name,
            phone: this.moduleService.getAppConstants().VALIDATION_MESSAGES.phone,
            email: this.moduleService.getAppConstants().VALIDATION_MESSAGES.email,
            address: this.moduleService.getAppConstants().VALIDATION_MESSAGES.address
        };
        this.translate.get(['GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t) => {
            this.types[0].value = t.GENERAL.client;
            this.types[1].value = t.GENERAL.prospect;
            this.types[2].value = t.GENERAL.not_specified;
        });
    }
    ngOnInit() {
        this.contactForm = new FormGroup({
            name: new FormControl(this.contact.name, Validators.required),
            last_name: new FormControl(this.contact.lastName, Validators.required),
            phone: new FormControl(this.contact.phone, [Validators.minLength(10), Validators.maxLength(10), Validators.required, Validators.pattern('[0-9]*')]),
            email: new FormControl(this.contact.email, Validators.pattern(this.moduleService.getAppConstants().EMAILPATTERN)),
            street_address: new FormControl(this.contact.streetAddress),
            type: new FormControl(this.contact.type),
            city: new FormControl(this.contact.city),
            id: new FormControl(this.contact.id),
            country_code: new FormControl(COUNTRY_CALLING_CODES[1]),
            phone_code: new FormControl(this.contact.phoneCode || COUNTRY_CALLING_CODES[1].callingCodes[0]),
        });
        this.store.CountryCodes$
            .pipe(takeUntil(this.destroyed$))
            .subscribe((countries) => __awaiter(this, void 0, void 0, function* () {
            if (!countries.length) {
                return this.store.fetchCountryCodes();
            }
            const countryCode = countries.filter(c => c.alpha3Code === this.contact.countryCode)[0] || COUNTRY_CALLING_CODES[1];
            this.contactForm.patchValue({ country_code: countryCode });
        }));
    }
    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
    close() {
        this.modalCtrl.dismiss( /*{someData: ...} */);
    }
    update() {
        // this._appendCountryCode();
        const form = this.contactForm.value;
        delete form.phone_code;
        form.country_code = form.country_code.alpha3Code;
        this.store.updateContact(form);
        this.modalCtrl.dismiss( /*{someData: ...} */);
    }
    create() {
        // this._appendCountryCode();
        const form = this.contactForm.value;
        delete form.phone_code;
        form.country_code = form.country_code.alpha3Code;
        this.store.createContact(form);
        this.modalCtrl.dismiss( /*{someData: ...} */);
    }
    presentPopover( /*ev: any*/) {
        return __awaiter(this, void 0, void 0, function* () {
            const loader = yield this.loadingCtrl.create();
            loader.present();
            this.store.CountryCodes$
                .pipe(takeUntil(this.destroyed$))
                .subscribe((countries) => __awaiter(this, void 0, void 0, function* () {
                if (!countries.length) {
                    return this.store.fetchCountryCodes();
                }
                let popover = yield this.mdalCtrl.create({
                    component: CountrySelectorModalComponent,
                    componentProps: { countries },
                });
                yield popover.present();
                loader.dismiss();
                const response = yield popover.onDidDismiss();
                const selCountry = response.data;
                if (selCountry) {
                    if (selCountry.callingCodes.length > 1) {
                        popover = yield this.popoverController.create({
                            component: CountryCodeSelectorPopoverComponent,
                            componentProps: { codes: selCountry.callingCodes },
                            // event: ev,
                            translucent: true,
                            backdropDismiss: false
                        });
                        yield popover.present();
                        const { data } = yield popover.onDidDismiss();
                        this.contactForm.patchValue({ country_code: selCountry, phone_code: `+${data}` });
                    }
                    else {
                        this.contactForm.patchValue({ country_code: selCountry, phone_code: `+${selCountry.callingCodes[0]}` });
                    }
                }
            }));
        });
    }
    _appendCountryCode() {
        const country = this.contactForm.get('country_code').value;
        switch (country.alpha3Code) {
            case 'MEX':
                this.contactForm.patchValue({
                    phone: `+${country.callingCodes[0]} 1 ${this.contactForm.get('phone').value}`.replace(/ /g, '')
                });
                break;
            case 'ARG':
                this.contactForm.patchValue({
                    phone: `+${country.callingCodes[0]} 9 ${this.contactForm.get('phone').value}`.replace(/ /g, '')
                });
        }
        this.contactForm.get('country_code').disable();
    }
};
ContactUpsertModalComponent.ctorParameters = () => [
    { type: ModalController },
    { type: ContactStore },
    { type: TranslateService },
    { type: PopoverController },
    { type: ModalController },
    { type: LoadingController },
    { type: AppSettingsService }
];
__decorate([
    Input()
], ContactUpsertModalComponent.prototype, "contact", void 0);
__decorate([
    Input()
], ContactUpsertModalComponent.prototype, "mode", void 0);
ContactUpsertModalComponent = __decorate([
    Component({
        selector: 'boxx-contact-upsert-modal',
        template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                GENERAL.contacts\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n    <ion-list class=\"ion-no-margin ion-no-padding\" [formGroup]=\"contactForm\">\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.name\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-input formControlName=\"name\" type=\"text\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.name\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('name').hasError(validation.type) && contactForm.get('name').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.lastName\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-input formControlName=\"last_name\" type=\"text\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.last_name\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('last_name').hasError(validation.type) && contactForm.get('last_name').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.type\n            </ion-label>\n            <ion-select placeholder=\"{{'GENERAL.ACTION.select' | translate}}\" formControlName=\"type\"\n                [okText]=\"'GENERAL.ACTION.ok' | translate\" [cancelText]=\"'GENERAL.ACTION.cancel' | translate\"\n                interface=\"popover\">\n                <ion-select-option *ngFor=\"let cType of types\" [value]=\"cType.key\">{{cType.value}}</ion-select-option>\n            </ion-select>\n        </ion-item>\n\n        <ion-item (click)=\"presentPopover()\" class=\"boxx-input-item\">\n            <ion-label>\n                <p>\n                    <ion-text color=\"primary\" translate>FORM.LABEL.country</ion-text>\n                </p>\n                <img alt=\"country flag\" src=\"{{contactForm.get('country_code').value.flag}}\" style=\"width: 32px;\">\n                {{contactForm.get('country_code').value.translations.es || contactForm.get('country_code').value.name}}\n            </ion-label>\n        </ion-item>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.phone\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-row style=\"width: 100%;\">\n                <ion-col size=\"2\" style=\"display: grid; align-content: center;\">\n                    <small>{{contactForm.get('phone_code').value}}</small>\n                </ion-col>\n                <ion-col size=\"10\">\n                    <ion-input formControlName=\"phone\" type=\"tel\" id=\"phone-input\" minLength=\"10\" maxLength=\"10\"\n                        placeholder=\"123 4567 890\">\n                    </ion-input>\n                </ion-col>\n            </ion-row>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.phone\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('phone').hasError(validation.type) && contactForm.get('phone').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.email\n            </ion-label>\n            <ion-input formControlName=\"email\" type=\"email\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.email\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('email').hasError(validation.type) && contactForm.get('email').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.address\n            </ion-label>\n            <ion-input formControlName=\"street_address\" type=\"text\"></ion-input>\n        </ion-item>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.city\n            </ion-label>\n            <ion-input formControlName=\"city\" type=\"text\"></ion-input>\n        </ion-item>\n    </ion-list>\n\n    <div class=\"ion-margin-vertical\">\n        <ion-button color=\"primary\" expand=\"block\" (click)=\"update()\" *ngIf=\"mode == 'UPDATE'\"\n            [disabled]=\"contactForm.invalid\" translate>\n            GENERAL.ACTION.update\n        </ion-button>\n        <ion-button color=\"primary\" expand=\"block\" (click)=\"create()\" *ngIf=\"mode == 'CREATE'\"\n            [disabled]=\"contactForm.invalid\" translate>\n            GENERAL.ACTION.create\n        </ion-button>\n    </div>\n\n</ion-content>",
        styles: [":host ion-toolbar.md ion-title{padding:0}:host #phone-input{background-color:#efefef;padding-left:8px!important}"]
    })
], ContactUpsertModalComponent);
export { ContactUpsertModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdC11cHNlcnQubW9kYWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtbW9kdWxlcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvbnRhY3QtdXBzZXJ0LW1vZGFsL2NvbnRhY3QtdXBzZXJ0Lm1vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBcUIsTUFBTSxlQUFlLENBQUM7QUFDcEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEUsT0FBTyxFQUFFLFlBQVksRUFBRSxZQUFZLEVBQUUscUJBQXFCLEVBQUUsYUFBYSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkcsT0FBTyxFQUFFLGlCQUFpQixFQUFFLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ25FLE9BQU8sRUFBRSxtQ0FBbUMsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBT3pFLElBQWEsMkJBQTJCLEdBQXhDLE1BQWEsMkJBQTJCO0lBSXBDLFlBQ1ksU0FBMEIsRUFDMUIsS0FBbUIsRUFDbkIsU0FBMkIsRUFDM0IsaUJBQW9DLEVBQ3JDLFFBQXlCLEVBQ3hCLFdBQThCLEVBQzlCLGFBQWlDO1FBTmpDLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBQzFCLFVBQUssR0FBTCxLQUFLLENBQWM7UUFDbkIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0Isc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNyQyxhQUFRLEdBQVIsUUFBUSxDQUFpQjtRQUN4QixnQkFBVyxHQUFYLFdBQVcsQ0FBbUI7UUFDOUIsa0JBQWEsR0FBYixhQUFhLENBQW9CO1FBVzdDLGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBSXBDLFVBQUssR0FBRztZQUNKLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFO1lBQ2xDLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFO1lBQ3RDLEVBQUUsR0FBRyxFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsZUFBZSxFQUFFO1NBQ25ELENBQUM7UUFFRix1QkFBa0IsR0FBRztZQUNqQixJQUFJLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJO1lBQ25FLFNBQVMsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDLG1CQUFtQixDQUFDLFNBQVM7WUFDN0UsS0FBSyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFLENBQUMsbUJBQW1CLENBQUMsS0FBSztZQUNyRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO1lBQ3JFLE9BQU8sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDLG1CQUFtQixDQUFDLE9BQU87U0FDNUUsQ0FBQztRQXpCRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxDQUFDLENBQU0sRUFBRSxFQUFFO1lBQ2xCLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDO1FBQ2xELENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQW9CRCxRQUFRO1FBRUosSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLFNBQVMsQ0FBQztZQUM3QixJQUFJLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLFFBQVEsQ0FBQztZQUM3RCxTQUFTLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFFBQVEsQ0FBQztZQUN0RSxLQUFLLEVBQUUsSUFBSSxXQUFXLENBQ2xCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUNsQixDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFBRSxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FDMUc7WUFDRCxLQUFLLEVBQUUsSUFBSSxXQUFXLENBQ2xCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FDNUY7WUFDRCxjQUFjLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7WUFDM0QsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ3hDLElBQUksRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUN4QyxFQUFFLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7WUFDcEMsWUFBWSxFQUFFLElBQUksV0FBVyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZELFVBQVUsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsSUFBSSxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbEcsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhO2FBQ25CLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxDQUFPLFNBQVMsRUFBRSxFQUFFO1lBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO2dCQUFFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2FBQUU7WUFFakUsTUFBTSxXQUFXLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwSCxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDO1FBQy9ELENBQUMsQ0FBQSxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVELEtBQUs7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBQyxvQkFBb0IsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxNQUFNO1FBQ0YsNkJBQTZCO1FBRTdCLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1FBRXBDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUV2QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO1FBRWpELElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELE1BQU07UUFDRiw2QkFBNkI7UUFDN0IsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7UUFFcEMsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBRXZCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7UUFFakQsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUMsb0JBQW9CLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUssY0FBYyxFQUFDLFdBQVc7O1lBQzVCLE1BQU0sTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUUvQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7WUFFakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhO2lCQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDaEMsU0FBUyxDQUFDLENBQU8sU0FBUyxFQUFFLEVBQUU7Z0JBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO29CQUFFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2lCQUFFO2dCQUVqRSxJQUFJLE9BQU8sR0FBUSxNQUFNLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO29CQUMxQyxTQUFTLEVBQUUsNkJBQTZCO29CQUN4QyxjQUFjLEVBQUUsRUFBRSxTQUFTLEVBQUU7aUJBRWhDLENBQUMsQ0FBQztnQkFFSCxNQUFNLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFFeEIsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUVqQixNQUFNLFFBQVEsR0FBRyxNQUFNLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDOUMsTUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFFakMsSUFBSSxVQUFVLEVBQUU7b0JBQ1osSUFBSSxVQUFVLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ3BDLE9BQU8sR0FBRyxNQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7NEJBQzFDLFNBQVMsRUFBRSxtQ0FBbUM7NEJBQzlDLGNBQWMsRUFBRSxFQUFFLEtBQUssRUFBRSxVQUFVLENBQUMsWUFBWSxFQUFFOzRCQUNsRCxhQUFhOzRCQUNiLFdBQVcsRUFBRSxJQUFJOzRCQUNqQixlQUFlLEVBQUUsS0FBSzt5QkFDekIsQ0FBQyxDQUFDO3dCQUNILE1BQU0sT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUN4QixNQUFNLEVBQUUsSUFBSSxFQUFFLEdBQUcsTUFBTSxPQUFPLENBQUMsWUFBWSxFQUFFLENBQUM7d0JBRTlDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsSUFBSSxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7cUJBQ3JGO3lCQUNJO3dCQUNELElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsSUFBSSxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO3FCQUMzRztpQkFDSjtZQUNMLENBQUMsQ0FBQSxDQUFDLENBQUM7UUFDWCxDQUFDO0tBQUE7SUFFTyxrQkFBa0I7UUFDdEIsTUFBTSxPQUFPLEdBQWtCLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUUxRSxRQUFRLE9BQU8sQ0FBQyxVQUFVLEVBQUU7WUFDeEIsS0FBSyxLQUFLO2dCQUNOLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDO29CQUN4QixLQUFLLEVBQUUsSUFBSSxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDO2lCQUNsRyxDQUFDLENBQUM7Z0JBQ0gsTUFBTTtZQUNWLEtBQUssS0FBSztnQkFDTixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQztvQkFDeEIsS0FBSyxFQUFFLElBQUksT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQztpQkFDbEcsQ0FBQyxDQUFDO1NBQ1Y7UUFFRCxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuRCxDQUFDO0NBQ0osQ0FBQTs7WUFoSzBCLGVBQWU7WUFDbkIsWUFBWTtZQUNSLGdCQUFnQjtZQUNSLGlCQUFpQjtZQUMzQixlQUFlO1lBQ1gsaUJBQWlCO1lBQ2Ysa0JBQWtCOztBQVZwQztJQUFSLEtBQUssRUFBRTs0REFBdUI7QUFDdEI7SUFBUixLQUFLLEVBQUU7eURBQTJCO0FBRjFCLDJCQUEyQjtJQUx2QyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsMkJBQTJCO1FBQ3JDLGloTUFBMEM7O0tBRTdDLENBQUM7R0FDVywyQkFBMkIsQ0FxS3ZDO1NBcktZLDJCQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgRm9ybUdyb3VwLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgQ29udGFjdE1vZGVsLCBDb250YWN0U3RvcmUsIENPVU5UUllfQ0FMTElOR19DT0RFUywgSUNvdW50cnlDb2RlcyB9IGZyb20gJ0Bib3h4L2NvbnRhY3RzLWNvcmUnO1xuaW1wb3J0IHsgTG9hZGluZ0NvbnRyb2xsZXIsIE1vZGFsQ29udHJvbGxlciwgUG9wb3ZlckNvbnRyb2xsZXIgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBcHBTZXR0aW5nc1NlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvZ2xvYmFsLXBhcmFtcyc7XG5pbXBvcnQgeyBDb3VudHJ5Q29kZVNlbGVjdG9yUG9wb3ZlckNvbXBvbmVudCB9IGZyb20gJy4vY291bnRyeS1jb2Rlcy1zZWxlY3Rvci5wb3BvdmVyJztcbmltcG9ydCB7IENvdW50cnlTZWxlY3Rvck1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9jb3VudHJ5LXNlbGVjdG9yLm1vZGFsJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LWNvbnRhY3QtdXBzZXJ0LW1vZGFsJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vY29udGFjdC11cHNlcnQubW9kYWwuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vY29udGFjdC11cHNlcnQubW9kYWwuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBDb250YWN0VXBzZXJ0TW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gICAgQElucHV0KCkgY29udGFjdDogQ29udGFjdE1vZGVsO1xuICAgIEBJbnB1dCgpIG1vZGU6ICdDUkVBVEUnIHwgJ1VQREFURSc7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSBtb2RhbEN0cmw6IE1vZGFsQ29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSBzdG9yZTogQ29udGFjdFN0b3JlLFxuICAgICAgICBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSBwb3BvdmVyQ29udHJvbGxlcjogUG9wb3ZlckNvbnRyb2xsZXIsXG4gICAgICAgIHB1YmxpYyBtZGFsQ3RybDogTW9kYWxDb250cm9sbGVyLFxuICAgICAgICBwcml2YXRlIGxvYWRpbmdDdHJsOiBMb2FkaW5nQ29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSBtb2R1bGVTZXJ2aWNlOiBBcHBTZXR0aW5nc1NlcnZpY2VcbiAgICApIHtcbiAgICAgICAgdGhpcy50cmFuc2xhdGUuZ2V0KFsnR0VORVJBTCddKVxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuZGVzdHJveWVkJCkpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKCh0OiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnR5cGVzWzBdLnZhbHVlID0gdC5HRU5FUkFMLmNsaWVudDtcbiAgICAgICAgICAgICAgICB0aGlzLnR5cGVzWzFdLnZhbHVlID0gdC5HRU5FUkFMLnByb3NwZWN0O1xuICAgICAgICAgICAgICAgIHRoaXMudHlwZXNbMl0udmFsdWUgPSB0LkdFTkVSQUwubm90X3NwZWNpZmllZDtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIGRlc3Ryb3llZCQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xuXG4gICAgY29udGFjdEZvcm06IEZvcm1Hcm91cDtcblxuICAgIHR5cGVzID0gW1xuICAgICAgICB7IGtleTogJ0NMSUVOVCcsIHZhbHVlOiAnQ2xpZW50JyB9LFxuICAgICAgICB7IGtleTogJ1BST1NQRUNUJywgdmFsdWU6ICdQcm9zcGVjdCcgfSxcbiAgICAgICAgeyBrZXk6ICdOT1RfU1BFQ0lGSUVEJywgdmFsdWU6ICdOb3QgcXVhbGlmaWVkJyB9XG4gICAgXTtcblxuICAgIHZhbGlkYXRpb25NZXNzYWdlcyA9IHtcbiAgICAgICAgbmFtZTogdGhpcy5tb2R1bGVTZXJ2aWNlLmdldEFwcENvbnN0YW50cygpLlZBTElEQVRJT05fTUVTU0FHRVMubmFtZSxcbiAgICAgICAgbGFzdF9uYW1lOiB0aGlzLm1vZHVsZVNlcnZpY2UuZ2V0QXBwQ29uc3RhbnRzKCkuVkFMSURBVElPTl9NRVNTQUdFUy5sYXN0X25hbWUsXG4gICAgICAgIHBob25lOiB0aGlzLm1vZHVsZVNlcnZpY2UuZ2V0QXBwQ29uc3RhbnRzKCkuVkFMSURBVElPTl9NRVNTQUdFUy5waG9uZSxcbiAgICAgICAgZW1haWw6IHRoaXMubW9kdWxlU2VydmljZS5nZXRBcHBDb25zdGFudHMoKS5WQUxJREFUSU9OX01FU1NBR0VTLmVtYWlsLFxuICAgICAgICBhZGRyZXNzOiB0aGlzLm1vZHVsZVNlcnZpY2UuZ2V0QXBwQ29uc3RhbnRzKCkuVkFMSURBVElPTl9NRVNTQUdFUy5hZGRyZXNzXG4gICAgfTtcblxuICAgIG5nT25Jbml0KCkge1xuXG4gICAgICAgIHRoaXMuY29udGFjdEZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcbiAgICAgICAgICAgIG5hbWU6IG5ldyBGb3JtQ29udHJvbCh0aGlzLmNvbnRhY3QubmFtZSwgVmFsaWRhdG9ycy5yZXF1aXJlZCksXG4gICAgICAgICAgICBsYXN0X25hbWU6IG5ldyBGb3JtQ29udHJvbCh0aGlzLmNvbnRhY3QubGFzdE5hbWUsIFZhbGlkYXRvcnMucmVxdWlyZWQpLFxuICAgICAgICAgICAgcGhvbmU6IG5ldyBGb3JtQ29udHJvbChcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRhY3QucGhvbmUsXG4gICAgICAgICAgICAgICAgW1ZhbGlkYXRvcnMubWluTGVuZ3RoKDEwKSwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTApLCBWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLnBhdHRlcm4oJ1swLTldKicpXVxuICAgICAgICAgICAgKSxcbiAgICAgICAgICAgIGVtYWlsOiBuZXcgRm9ybUNvbnRyb2woXG4gICAgICAgICAgICAgICAgdGhpcy5jb250YWN0LmVtYWlsLCBWYWxpZGF0b3JzLnBhdHRlcm4odGhpcy5tb2R1bGVTZXJ2aWNlLmdldEFwcENvbnN0YW50cygpLkVNQUlMUEFUVEVSTilcbiAgICAgICAgICAgICksXG4gICAgICAgICAgICBzdHJlZXRfYWRkcmVzczogbmV3IEZvcm1Db250cm9sKHRoaXMuY29udGFjdC5zdHJlZXRBZGRyZXNzKSxcbiAgICAgICAgICAgIHR5cGU6IG5ldyBGb3JtQ29udHJvbCh0aGlzLmNvbnRhY3QudHlwZSksXG4gICAgICAgICAgICBjaXR5OiBuZXcgRm9ybUNvbnRyb2wodGhpcy5jb250YWN0LmNpdHkpLFxuICAgICAgICAgICAgaWQ6IG5ldyBGb3JtQ29udHJvbCh0aGlzLmNvbnRhY3QuaWQpLFxuICAgICAgICAgICAgY291bnRyeV9jb2RlOiBuZXcgRm9ybUNvbnRyb2woQ09VTlRSWV9DQUxMSU5HX0NPREVTWzFdKSxcbiAgICAgICAgICAgIHBob25lX2NvZGU6IG5ldyBGb3JtQ29udHJvbCh0aGlzLmNvbnRhY3QucGhvbmVDb2RlIHx8IENPVU5UUllfQ0FMTElOR19DT0RFU1sxXS5jYWxsaW5nQ29kZXNbMF0pLFxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLnN0b3JlLkNvdW50cnlDb2RlcyRcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLmRlc3Ryb3llZCQpKVxuICAgICAgICAgICAgLnN1YnNjcmliZShhc3luYyAoY291bnRyaWVzKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKCFjb3VudHJpZXMubGVuZ3RoKSB7IHJldHVybiB0aGlzLnN0b3JlLmZldGNoQ291bnRyeUNvZGVzKCk7IH1cblxuICAgICAgICAgICAgICAgIGNvbnN0IGNvdW50cnlDb2RlID0gY291bnRyaWVzLmZpbHRlcihjID0+IGMuYWxwaGEzQ29kZSA9PT0gdGhpcy5jb250YWN0LmNvdW50cnlDb2RlKVswXSB8fCBDT1VOVFJZX0NBTExJTkdfQ09ERVNbMV07XG4gICAgICAgICAgICAgICAgdGhpcy5jb250YWN0Rm9ybS5wYXRjaFZhbHVlKHsgY291bnRyeV9jb2RlOiBjb3VudHJ5Q29kZSB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIG5nT25EZXN0cm95KCkge1xuICAgICAgICB0aGlzLmRlc3Ryb3llZCQubmV4dCh0cnVlKTtcbiAgICAgICAgdGhpcy5kZXN0cm95ZWQkLmNvbXBsZXRlKCk7XG4gICAgfVxuXG4gICAgY2xvc2UoKSB7XG4gICAgICAgIHRoaXMubW9kYWxDdHJsLmRpc21pc3MoLyp7c29tZURhdGE6IC4uLn0gKi8pO1xuICAgIH1cblxuICAgIHVwZGF0ZSgpIHtcbiAgICAgICAgLy8gdGhpcy5fYXBwZW5kQ291bnRyeUNvZGUoKTtcblxuICAgICAgICBjb25zdCBmb3JtID0gdGhpcy5jb250YWN0Rm9ybS52YWx1ZTtcblxuICAgICAgICBkZWxldGUgZm9ybS5waG9uZV9jb2RlO1xuXG4gICAgICAgIGZvcm0uY291bnRyeV9jb2RlID0gZm9ybS5jb3VudHJ5X2NvZGUuYWxwaGEzQ29kZTtcblxuICAgICAgICB0aGlzLnN0b3JlLnVwZGF0ZUNvbnRhY3QoZm9ybSk7XG4gICAgICAgIHRoaXMubW9kYWxDdHJsLmRpc21pc3MoLyp7c29tZURhdGE6IC4uLn0gKi8pO1xuICAgIH1cblxuICAgIGNyZWF0ZSgpIHtcbiAgICAgICAgLy8gdGhpcy5fYXBwZW5kQ291bnRyeUNvZGUoKTtcbiAgICAgICAgY29uc3QgZm9ybSA9IHRoaXMuY29udGFjdEZvcm0udmFsdWU7XG5cbiAgICAgICAgZGVsZXRlIGZvcm0ucGhvbmVfY29kZTtcblxuICAgICAgICBmb3JtLmNvdW50cnlfY29kZSA9IGZvcm0uY291bnRyeV9jb2RlLmFscGhhM0NvZGU7XG5cbiAgICAgICAgdGhpcy5zdG9yZS5jcmVhdGVDb250YWN0KGZvcm0pO1xuICAgICAgICB0aGlzLm1vZGFsQ3RybC5kaXNtaXNzKC8qe3NvbWVEYXRhOiAuLi59ICovKTtcbiAgICB9XG5cbiAgICBhc3luYyBwcmVzZW50UG9wb3ZlcigvKmV2OiBhbnkqLykge1xuICAgICAgICBjb25zdCBsb2FkZXIgPSBhd2FpdCB0aGlzLmxvYWRpbmdDdHJsLmNyZWF0ZSgpO1xuXG4gICAgICAgIGxvYWRlci5wcmVzZW50KCk7XG5cbiAgICAgICAgdGhpcy5zdG9yZS5Db3VudHJ5Q29kZXMkXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoYXN5bmMgKGNvdW50cmllcykgPT4ge1xuICAgICAgICAgICAgICAgIGlmICghY291bnRyaWVzLmxlbmd0aCkgeyByZXR1cm4gdGhpcy5zdG9yZS5mZXRjaENvdW50cnlDb2RlcygpOyB9XG5cbiAgICAgICAgICAgICAgICBsZXQgcG9wb3ZlcjogYW55ID0gYXdhaXQgdGhpcy5tZGFsQ3RybC5jcmVhdGUoe1xuICAgICAgICAgICAgICAgICAgICBjb21wb25lbnQ6IENvdW50cnlTZWxlY3Rvck1vZGFsQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgICAgICBjb21wb25lbnRQcm9wczogeyBjb3VudHJpZXMgfSxcbiAgICAgICAgICAgICAgICAgICAgLy8gZXZlbnQ6IGV2LFxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgYXdhaXQgcG9wb3Zlci5wcmVzZW50KCk7XG5cbiAgICAgICAgICAgICAgICBsb2FkZXIuZGlzbWlzcygpO1xuXG4gICAgICAgICAgICAgICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBwb3BvdmVyLm9uRGlkRGlzbWlzcygpO1xuICAgICAgICAgICAgICAgIGNvbnN0IHNlbENvdW50cnkgPSByZXNwb25zZS5kYXRhO1xuXG4gICAgICAgICAgICAgICAgaWYgKHNlbENvdW50cnkpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbENvdW50cnkuY2FsbGluZ0NvZGVzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvcG92ZXIgPSBhd2FpdCB0aGlzLnBvcG92ZXJDb250cm9sbGVyLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29tcG9uZW50OiBDb3VudHJ5Q29kZVNlbGVjdG9yUG9wb3ZlckNvbXBvbmVudCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb21wb25lbnRQcm9wczogeyBjb2Rlczogc2VsQ291bnRyeS5jYWxsaW5nQ29kZXMgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBldmVudDogZXYsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNsdWNlbnQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFja2Ryb3BEaXNtaXNzOiBmYWxzZVxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBhd2FpdCBwb3BvdmVyLnByZXNlbnQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHsgZGF0YSB9ID0gYXdhaXQgcG9wb3Zlci5vbkRpZERpc21pc3MoKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb250YWN0Rm9ybS5wYXRjaFZhbHVlKHsgY291bnRyeV9jb2RlOiBzZWxDb3VudHJ5LCBwaG9uZV9jb2RlOiBgKyR7ZGF0YX1gIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb250YWN0Rm9ybS5wYXRjaFZhbHVlKHsgY291bnRyeV9jb2RlOiBzZWxDb3VudHJ5LCBwaG9uZV9jb2RlOiBgKyR7c2VsQ291bnRyeS5jYWxsaW5nQ29kZXNbMF19YCB9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgX2FwcGVuZENvdW50cnlDb2RlKCkge1xuICAgICAgICBjb25zdCBjb3VudHJ5OiBJQ291bnRyeUNvZGVzID0gdGhpcy5jb250YWN0Rm9ybS5nZXQoJ2NvdW50cnlfY29kZScpLnZhbHVlO1xuXG4gICAgICAgIHN3aXRjaCAoY291bnRyeS5hbHBoYTNDb2RlKSB7XG4gICAgICAgICAgICBjYXNlICdNRVgnOlxuICAgICAgICAgICAgICAgIHRoaXMuY29udGFjdEZvcm0ucGF0Y2hWYWx1ZSh7XG4gICAgICAgICAgICAgICAgICAgIHBob25lOiBgKyR7Y291bnRyeS5jYWxsaW5nQ29kZXNbMF19IDEgJHt0aGlzLmNvbnRhY3RGb3JtLmdldCgncGhvbmUnKS52YWx1ZX1gLnJlcGxhY2UoLyAvZywgJycpXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlICdBUkcnOlxuICAgICAgICAgICAgICAgIHRoaXMuY29udGFjdEZvcm0ucGF0Y2hWYWx1ZSh7XG4gICAgICAgICAgICAgICAgICAgIHBob25lOiBgKyR7Y291bnRyeS5jYWxsaW5nQ29kZXNbMF19IDkgJHt0aGlzLmNvbnRhY3RGb3JtLmdldCgncGhvbmUnKS52YWx1ZX1gLnJlcGxhY2UoLyAvZywgJycpXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmNvbnRhY3RGb3JtLmdldCgnY291bnRyeV9jb2RlJykuZGlzYWJsZSgpO1xuICAgIH1cbn1cbiJdfQ==