import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';
let CountryCodeSelectorPopoverComponent = class CountryCodeSelectorPopoverComponent {
    constructor(popoverController) {
        this.popoverController = popoverController;
        this.codes = [];
    }
    ngOnInit() {
    }
    select(code) {
        this.popoverController.dismiss(code);
    }
};
CountryCodeSelectorPopoverComponent.ctorParameters = () => [
    { type: PopoverController }
];
__decorate([
    Input()
], CountryCodeSelectorPopoverComponent.prototype, "codes", void 0);
CountryCodeSelectorPopoverComponent = __decorate([
    Component({
        selector: 'boxx-country-code-select-popover',
        template: `
    <ion-content>
        <ion-list-header translate>
            GENERAL.ACTION.selectCountryCode
        </ion-list-header>
        <ion-item *ngFor="let code of codes" (click)="select(code)">
            <ion-label>
                +{{code}}
            </ion-label>
        </ion-item>
    </ion-content>
    `
    })
], CountryCodeSelectorPopoverComponent);
export { CountryCodeSelectorPopoverComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRyeS1jb2Rlcy1zZWxlY3Rvci5wb3BvdmVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb250YWN0LXVwc2VydC1tb2RhbC9jb3VudHJ5LWNvZGVzLXNlbGVjdG9yLnBvcG92ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBa0JuRCxJQUFhLG1DQUFtQyxHQUFoRCxNQUFhLG1DQUFtQztJQUc1QyxZQUNXLGlCQUFvQztRQUFwQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBSHRDLFVBQUssR0FBa0IsRUFBRSxDQUFDO0lBSS9CLENBQUM7SUFFTCxRQUFRO0lBRVIsQ0FBQztJQUVELE1BQU0sQ0FBQyxJQUFZO1FBQ2YsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QyxDQUFDO0NBQ0osQ0FBQTs7WUFWaUMsaUJBQWlCOztBQUh0QztJQUFSLEtBQUssRUFBRTtrRUFBMkI7QUFEMUIsbUNBQW1DO0lBZi9DLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxrQ0FBa0M7UUFDNUMsUUFBUSxFQUFFOzs7Ozs7Ozs7OztLQVdUO0tBQ0osQ0FBQztHQUNXLG1DQUFtQyxDQWMvQztTQWRZLG1DQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUG9wb3ZlckNvbnRyb2xsZXIgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5cblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LWNvdW50cnktY29kZS1zZWxlY3QtcG9wb3ZlcicsXG4gICAgdGVtcGxhdGU6IGBcbiAgICA8aW9uLWNvbnRlbnQ+XG4gICAgICAgIDxpb24tbGlzdC1oZWFkZXIgdHJhbnNsYXRlPlxuICAgICAgICAgICAgR0VORVJBTC5BQ1RJT04uc2VsZWN0Q291bnRyeUNvZGVcbiAgICAgICAgPC9pb24tbGlzdC1oZWFkZXI+XG4gICAgICAgIDxpb24taXRlbSAqbmdGb3I9XCJsZXQgY29kZSBvZiBjb2Rlc1wiIChjbGljayk9XCJzZWxlY3QoY29kZSlcIj5cbiAgICAgICAgICAgIDxpb24tbGFiZWw+XG4gICAgICAgICAgICAgICAgK3t7Y29kZX19XG4gICAgICAgICAgICA8L2lvbi1sYWJlbD5cbiAgICAgICAgPC9pb24taXRlbT5cbiAgICA8L2lvbi1jb250ZW50PlxuICAgIGBcbn0pXG5leHBvcnQgY2xhc3MgQ291bnRyeUNvZGVTZWxlY3RvclBvcG92ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIEBJbnB1dCgpIGNvZGVzOiBBcnJheTxudW1iZXI+ID0gW107XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHVibGljIHBvcG92ZXJDb250cm9sbGVyOiBQb3BvdmVyQ29udHJvbGxlclxuICAgICkgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHtcblxuICAgIH1cblxuICAgIHNlbGVjdChjb2RlOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5wb3BvdmVyQ29udHJvbGxlci5kaXNtaXNzKGNvZGUpO1xuICAgIH1cbn1cbiJdfQ==