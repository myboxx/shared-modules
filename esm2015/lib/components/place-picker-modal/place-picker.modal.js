import { __awaiter, __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { IonSearchbar, ModalController } from '@ionic/angular';
import { Subject } from 'rxjs';
let PlacePickerModalComponent = class PlacePickerModalComponent {
    constructor(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.destroyed$ = new Subject();
        this.searchResults = [];
        this.mapsService = new google.maps.places.AutocompleteService();
    }
    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
    ionViewDidEnter() {
        setTimeout(() => {
            this.searchBar.setFocus();
        }, 200);
    }
    onInput(query) {
        this.searchResults = [];
        if (!query) {
            return;
        }
        const config = {
            types: [],
            input: query
        };
        this.mapsService.getPlacePredictions(config, (predictions, status) => {
            if (predictions && predictions.length > 0) {
                predictions.forEach((prediction) => {
                    this.searchResults.push(prediction);
                });
            }
            else {
                const locationDesc = {
                    description: query,
                    place_id: 'default_place_id'
                };
                this.searchResults.push(locationDesc);
            }
        });
    }
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            this.modalCtrl.dismiss( /*{someData: ...} */);
        });
    }
    chooseItem(item) {
        this.modalCtrl.dismiss({ description: item.description, id: item.place_id });
    }
    select() { }
};
PlacePickerModalComponent.ctorParameters = () => [
    { type: ModalController }
];
__decorate([
    ViewChild('placePickerSB', { static: false })
], PlacePickerModalComponent.prototype, "searchBar", void 0);
PlacePickerModalComponent = __decorate([
    Component({
        selector: 'boxx-place-picker-modal',
        template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                FORM.LABEL.place\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #placePickerSB [debounce]=\"300\" showCancelButton=\"never\"\n            (ionInput)=\"onInput($event.target.value)\" cancelButtonIcon=\"close-circle-outline\"\n            placeholder='{{\"EVENTS.enterAddress\" | translate}}'></ion-searchbar>\n    </ion-item-divider>\n\n    <ion-list class=\"padding-right\">\n        <ion-item *ngFor=\"let item of searchResults\" (click)=\"chooseItem(item)\">\n            <p>{{ item.description }}</p>\n        </ion-item>\n    </ion-list>\n</ion-content>",
        styles: ["ion-toolbar.md ion-title{padding:0}"]
    })
], PlacePickerModalComponent);
export { PlacePickerModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxhY2UtcGlja2VyLm1vZGFsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9wbGFjZS1waWNrZXItbW9kYWwvcGxhY2UtcGlja2VyLm1vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFhLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQy9ELE9BQU8sRUFBYyxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFTM0MsSUFBYSx5QkFBeUIsR0FBdEMsTUFBYSx5QkFBeUI7SUFHbEMsWUFBb0IsU0FBMEI7UUFBMUIsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFJOUMsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFHcEMsa0JBQWEsR0FBZSxFQUFFLENBQUM7UUFOM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDcEUsQ0FBQztJQVdELFdBQVc7UUFDUCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCxlQUFlO1FBQ1gsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNaLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDOUIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ1osQ0FBQztJQUVELE9BQU8sQ0FBQyxLQUFhO1FBQ2pCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBRXhCLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDUixPQUFPO1NBQ1Y7UUFFRCxNQUFNLE1BQU0sR0FBRztZQUNYLEtBQUssRUFBRSxFQUFFO1lBQ1QsS0FBSyxFQUFFLEtBQUs7U0FDZixDQUFDO1FBRUYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxXQUFXLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDakUsSUFBSSxXQUFXLElBQUksV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3ZDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxVQUFVLEVBQUUsRUFBRTtvQkFDL0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3hDLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsTUFBTSxZQUFZLEdBQUc7b0JBQ2pCLFdBQVcsRUFBRSxLQUFLO29CQUNsQixRQUFRLEVBQUUsa0JBQWtCO2lCQUMvQixDQUFDO2dCQUVGLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ3pDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUssS0FBSzs7WUFDUCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQ2pELENBQUM7S0FBQTtJQUVELFVBQVUsQ0FBQyxJQUFTO1FBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0lBQ2pGLENBQUM7SUFFRCxNQUFNLEtBQUssQ0FBQztDQUNmLENBQUE7O1lBN0RrQyxlQUFlOztBQUZDO0lBQTlDLFNBQVMsQ0FBQyxlQUFlLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7NERBQXlCO0FBRDlELHlCQUF5QjtJQUxyQyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUseUJBQXlCO1FBQ25DLCt1Q0FBd0M7O0tBRTNDLENBQUM7R0FDVyx5QkFBeUIsQ0FnRXJDO1NBaEVZLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IElvblNlYXJjaGJhciwgTW9kYWxDb250cm9sbGVyIH0gZnJvbSAnQGlvbmljL2FuZ3VsYXInO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuXG5kZWNsYXJlIHZhciBnb29nbGU6IGFueTtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LXBsYWNlLXBpY2tlci1tb2RhbCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3BsYWNlLXBpY2tlci5tb2RhbC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9wbGFjZS1waWNrZXIubW9kYWwuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFBsYWNlUGlja2VyTW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkRlc3Ryb3kge1xuICAgIEBWaWV3Q2hpbGQoJ3BsYWNlUGlja2VyU0InLCB7IHN0YXRpYzogZmFsc2UgfSkgc2VhcmNoQmFyOiBJb25TZWFyY2hiYXI7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIG1vZGFsQ3RybDogTW9kYWxDb250cm9sbGVyKSB7XG4gICAgICAgIHRoaXMubWFwc1NlcnZpY2UgPSBuZXcgZ29vZ2xlLm1hcHMucGxhY2VzLkF1dG9jb21wbGV0ZVNlcnZpY2UoKTtcbiAgICB9XG5cbiAgICBkZXN0cm95ZWQkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcbiAgICBpc0xvYWRpbmckOiBPYnNlcnZhYmxlPGJvb2xlYW4+O1xuXG4gICAgc2VhcmNoUmVzdWx0czogQXJyYXk8YW55PiA9IFtdO1xuXG4gICAgdHJhbnNsYXRpb25zOiBhbnk7XG5cbiAgICBtYXBzU2VydmljZTogYW55O1xuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMuZGVzdHJveWVkJC5uZXh0KHRydWUpO1xuICAgICAgICB0aGlzLmRlc3Ryb3llZCQuY29tcGxldGUoKTtcbiAgICB9XG5cbiAgICBpb25WaWV3RGlkRW50ZXIoKSB7XG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5zZWFyY2hCYXIuc2V0Rm9jdXMoKTtcbiAgICAgICAgfSwgMjAwKTtcbiAgICB9XG5cbiAgICBvbklucHV0KHF1ZXJ5OiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5zZWFyY2hSZXN1bHRzID0gW107XG5cbiAgICAgICAgaWYgKCFxdWVyeSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgY29uZmlnID0ge1xuICAgICAgICAgICAgdHlwZXM6IFtdLCAvLyBvdGhlciB0eXBlcyBhdmFpbGFibGUgaW4gdGhlIEFQSTogJ2FkZHJlc3MgJ2VzdGFibGlzaG1lbnQnLCAncmVnaW9ucycsIGFuZCAnY2l0aWVzJ1xuICAgICAgICAgICAgaW5wdXQ6IHF1ZXJ5XG4gICAgICAgIH07XG5cbiAgICAgICAgdGhpcy5tYXBzU2VydmljZS5nZXRQbGFjZVByZWRpY3Rpb25zKGNvbmZpZywgKHByZWRpY3Rpb25zLCBzdGF0dXMpID0+IHtcbiAgICAgICAgICAgIGlmIChwcmVkaWN0aW9ucyAmJiBwcmVkaWN0aW9ucy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgcHJlZGljdGlvbnMuZm9yRWFjaCgocHJlZGljdGlvbikgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaFJlc3VsdHMucHVzaChwcmVkaWN0aW9uKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc3QgbG9jYXRpb25EZXNjID0ge1xuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogcXVlcnksXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlX2lkOiAnZGVmYXVsdF9wbGFjZV9pZCdcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hSZXN1bHRzLnB1c2gobG9jYXRpb25EZXNjKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYXN5bmMgY2xvc2UoKSB7XG4gICAgICAgIHRoaXMubW9kYWxDdHJsLmRpc21pc3MoLyp7c29tZURhdGE6IC4uLn0gKi8pO1xuICAgIH1cblxuICAgIGNob29zZUl0ZW0oaXRlbTogYW55KSB7XG4gICAgICAgIHRoaXMubW9kYWxDdHJsLmRpc21pc3MoeyBkZXNjcmlwdGlvbjogaXRlbS5kZXNjcmlwdGlvbiwgaWQ6IGl0ZW0ucGxhY2VfaWQgfSk7XG4gICAgfVxuXG4gICAgc2VsZWN0KCkgeyB9XG59XG4iXX0=