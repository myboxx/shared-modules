import { __awaiter, __decorate, __param } from "tslib";
import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { AlertController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FIREBASEX_SERVICE } from '../../services/ISharedModules.service';
let RemoteConfigComponent = class RemoteConfigComponent {
    constructor(firebasex, translate, appVersion, market, alertCtrl, platform) {
        this.firebasex = firebasex;
        this.translate = translate;
        this.appVersion = appVersion;
        this.market = market;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.whenFinish = new EventEmitter();
        this.destroyed$ = new Subject();
        this.configMessage = '...';
        this.isIos = false;
        this.translate.get(['GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t) => this.translations = t);
    }
    ngOnInit() {
        this.platform.ready().then(() => {
            this.configMessage = this.translations.GENERAL.loadingRemoteConfig;
            this.isIos = this.platform.is('ios');
            this.checkForUpdates();
            this.configMessage = '';
        });
    }
    checkForUpdates() {
        return __awaiter(this, void 0, void 0, function* () {
            const currentVersionNumber = yield this.appVersion.getVersionNumber();
            const packageName = yield this.appVersion.getPackageName();
            yield this.firebasex.fetch(600);
            yield this.firebasex.activateFetched();
            const mostRecentVersion = (yield this.firebasex.getValue('most_recent_version' + (this.isIos ? '_ios' : ''))) || '0.0.1';
            console.log('--- most_recent_version', mostRecentVersion);
            if (mostRecentVersion > currentVersionNumber) {
                const alert = yield this.alertCtrl.create({
                    header: this.translations.GENERAL.appUpdateHeader,
                    message: `${this.translations.GENERAL.appUpdateMsg} ${mostRecentVersion}`,
                    buttons: [{
                            text: this.translations.GENERAL.ACTION.update,
                            handler: () => {
                                this.market.open(packageName);
                                return false;
                            }
                        }],
                    backdropDismiss: false
                });
                alert.present();
            }
            else {
                this.whenFinish.emit(true);
            }
        });
    }
};
RemoteConfigComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [FIREBASEX_SERVICE,] }] },
    { type: TranslateService },
    { type: AppVersion },
    { type: Market },
    { type: AlertController },
    { type: Platform }
];
__decorate([
    Output()
], RemoteConfigComponent.prototype, "whenFinish", void 0);
RemoteConfigComponent = __decorate([
    Component({
        selector: 'boxx-remote-config',
        template: "<div>\n  {{configMessage}}\n</div>\n",
        styles: [""]
    }),
    __param(0, Inject(FIREBASEX_SERVICE))
], RemoteConfigComponent);
export { RemoteConfigComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVtb3RlLWNvbmZpZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtbW9kdWxlcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3JlbW90ZS1jb25maWcvcmVtb3RlLWNvbmZpZy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBVSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzNELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxpQkFBaUIsRUFBcUIsTUFBTSx1Q0FBdUMsQ0FBQztBQVE3RixJQUFhLHFCQUFxQixHQUFsQyxNQUFhLHFCQUFxQjtJQUc5QixZQUN1QyxTQUE0QixFQUN2RCxTQUEyQixFQUMzQixVQUFzQixFQUN0QixNQUFjLEVBQ2QsU0FBMEIsRUFDMUIsUUFBa0I7UUFMUyxjQUFTLEdBQVQsU0FBUyxDQUFtQjtRQUN2RCxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUMzQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxjQUFTLEdBQVQsU0FBUyxDQUFpQjtRQUMxQixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBUnBCLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO1FBZW5ELGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBSXBDLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBRXRCLFVBQUssR0FBRyxLQUFLLENBQUM7UUFYVixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxDQUFDLENBQU0sRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBVUQsUUFBUTtRQUNKLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUU1QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDO1lBRW5FLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFckMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBRXZCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVLLGVBQWU7O1lBRWpCLE1BQU0sb0JBQW9CLEdBQUcsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixFQUFFLENBQUM7WUFDdEUsTUFBTSxXQUFXLEdBQUcsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBRTNELE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFaEMsTUFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBRXZDLE1BQU0saUJBQWlCLEdBQUcsQ0FBQSxNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHFCQUFxQixHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFJLE9BQU8sQ0FBQztZQUV2SCxPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixFQUFFLGlCQUFpQixDQUFDLENBQUM7WUFFMUQsSUFBSSxpQkFBaUIsR0FBRyxvQkFBb0IsRUFBRTtnQkFDMUMsTUFBTSxLQUFLLEdBQUcsTUFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQztvQkFDdEMsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGVBQWU7b0JBQ2pELE9BQU8sRUFBRSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksSUFBSSxpQkFBaUIsRUFBRTtvQkFDekUsT0FBTyxFQUFFLENBQUM7NEJBQ04sSUFBSSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNOzRCQUM3QyxPQUFPLEVBQUUsR0FBRyxFQUFFO2dDQUNWLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dDQUU5QixPQUFPLEtBQUssQ0FBQzs0QkFDakIsQ0FBQzt5QkFDSixDQUFDO29CQUNGLGVBQWUsRUFBRSxLQUFLO2lCQUN6QixDQUFDLENBQUM7Z0JBRUgsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ25CO2lCQUNJO2dCQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzlCO1FBQ0wsQ0FBQztLQUFBO0NBQ0osQ0FBQTs7NENBbkVRLE1BQU0sU0FBQyxpQkFBaUI7WUFDTixnQkFBZ0I7WUFDZixVQUFVO1lBQ2QsTUFBTTtZQUNILGVBQWU7WUFDaEIsUUFBUTs7QUFScEI7SUFBVCxNQUFNLEVBQUU7eURBQTBDO0FBRDFDLHFCQUFxQjtJQUxqQyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsb0JBQW9CO1FBQzlCLGdEQUE2Qzs7S0FFaEQsQ0FBQztJQUtPLFdBQUEsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUE7R0FKckIscUJBQXFCLENBdUVqQztTQXZFWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5qZWN0LCBPbkluaXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQXBwVmVyc2lvbiB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvYXBwLXZlcnNpb24vbmd4JztcbmltcG9ydCB7IE1hcmtldCB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvbWFya2V0L25neCc7XG5pbXBvcnQgeyBBbGVydENvbnRyb2xsZXIsIFBsYXRmb3JtIH0gZnJvbSAnQGlvbmljL2FuZ3VsYXInO1xuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgRklSRUJBU0VYX1NFUlZJQ0UsIElGaXJlYmFzZXhTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvSVNoYXJlZE1vZHVsZXMuc2VydmljZSc7XG5cblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LXJlbW90ZS1jb25maWcnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9yZW1vdGUtY29uZmlnLmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9yZW1vdGUtY29uZmlnLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIFJlbW90ZUNvbmZpZ0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQE91dHB1dCgpIHdoZW5GaW5pc2ggPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgQEluamVjdChGSVJFQkFTRVhfU0VSVklDRSkgcHJpdmF0ZSBmaXJlYmFzZXg6IElGaXJlYmFzZXhTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSBhcHBWZXJzaW9uOiBBcHBWZXJzaW9uLFxuICAgICAgICBwcml2YXRlIG1hcmtldDogTWFya2V0LFxuICAgICAgICBwcml2YXRlIGFsZXJ0Q3RybDogQWxlcnRDb250cm9sbGVyLFxuICAgICAgICBwcml2YXRlIHBsYXRmb3JtOiBQbGF0Zm9ybVxuICAgICkge1xuICAgICAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoWydHRU5FUkFMJ10pXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKHQ6IGFueSkgPT4gdGhpcy50cmFuc2xhdGlvbnMgPSB0KTtcbiAgICB9XG5cbiAgICBkZXN0cm95ZWQkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcblxuICAgIHRyYW5zbGF0aW9uczogYW55O1xuXG4gICAgY29uZmlnTWVzc2FnZSA9ICcuLi4nO1xuXG4gICAgaXNJb3MgPSBmYWxzZTtcblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnBsYXRmb3JtLnJlYWR5KCkudGhlbigoKSA9PiB7XG5cbiAgICAgICAgICAgIHRoaXMuY29uZmlnTWVzc2FnZSA9IHRoaXMudHJhbnNsYXRpb25zLkdFTkVSQUwubG9hZGluZ1JlbW90ZUNvbmZpZztcblxuICAgICAgICAgICAgdGhpcy5pc0lvcyA9IHRoaXMucGxhdGZvcm0uaXMoJ2lvcycpO1xuXG4gICAgICAgICAgICB0aGlzLmNoZWNrRm9yVXBkYXRlcygpO1xuXG4gICAgICAgICAgICB0aGlzLmNvbmZpZ01lc3NhZ2UgPSAnJztcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYXN5bmMgY2hlY2tGb3JVcGRhdGVzKCkge1xuXG4gICAgICAgIGNvbnN0IGN1cnJlbnRWZXJzaW9uTnVtYmVyID0gYXdhaXQgdGhpcy5hcHBWZXJzaW9uLmdldFZlcnNpb25OdW1iZXIoKTtcbiAgICAgICAgY29uc3QgcGFja2FnZU5hbWUgPSBhd2FpdCB0aGlzLmFwcFZlcnNpb24uZ2V0UGFja2FnZU5hbWUoKTtcblxuICAgICAgICBhd2FpdCB0aGlzLmZpcmViYXNleC5mZXRjaCg2MDApO1xuXG4gICAgICAgIGF3YWl0IHRoaXMuZmlyZWJhc2V4LmFjdGl2YXRlRmV0Y2hlZCgpO1xuXG4gICAgICAgIGNvbnN0IG1vc3RSZWNlbnRWZXJzaW9uID0gYXdhaXQgdGhpcy5maXJlYmFzZXguZ2V0VmFsdWUoJ21vc3RfcmVjZW50X3ZlcnNpb24nICsgKHRoaXMuaXNJb3MgPyAnX2lvcycgOiAnJykpIHx8ICcwLjAuMSc7XG5cbiAgICAgICAgY29uc29sZS5sb2coJy0tLSBtb3N0X3JlY2VudF92ZXJzaW9uJywgbW9zdFJlY2VudFZlcnNpb24pO1xuXG4gICAgICAgIGlmIChtb3N0UmVjZW50VmVyc2lvbiA+IGN1cnJlbnRWZXJzaW9uTnVtYmVyKSB7XG4gICAgICAgICAgICBjb25zdCBhbGVydCA9IGF3YWl0IHRoaXMuYWxlcnRDdHJsLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgaGVhZGVyOiB0aGlzLnRyYW5zbGF0aW9ucy5HRU5FUkFMLmFwcFVwZGF0ZUhlYWRlcixcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiBgJHt0aGlzLnRyYW5zbGF0aW9ucy5HRU5FUkFMLmFwcFVwZGF0ZU1zZ30gJHttb3N0UmVjZW50VmVyc2lvbn1gLFxuICAgICAgICAgICAgICAgIGJ1dHRvbnM6IFt7XG4gICAgICAgICAgICAgICAgICAgIHRleHQ6IHRoaXMudHJhbnNsYXRpb25zLkdFTkVSQUwuQUNUSU9OLnVwZGF0ZSxcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlcjogKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tYXJrZXQub3BlbihwYWNrYWdlTmFtZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1dLFxuICAgICAgICAgICAgICAgIGJhY2tkcm9wRGlzbWlzczogZmFsc2VcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBhbGVydC5wcmVzZW50KCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLndoZW5GaW5pc2guZW1pdCh0cnVlKTtcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==