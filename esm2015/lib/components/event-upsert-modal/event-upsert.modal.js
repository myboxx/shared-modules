import { __awaiter, __decorate, __param } from "tslib";
import { Component, Inject, Input, Optional } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ContactModel } from '@boxx/contacts-core';
import { EventModel, EventsStore, IEventReminder } from '@boxx/events-core';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import * as moment_ from 'moment';
import { Subject } from 'rxjs';
import { takeUntil, withLatestFrom } from 'rxjs/operators';
import { AppSettingsService, LOCAL_NOTIFICATIONS_SERVICE } from '../../providers/global-params';
import { ContactPickerModalComponent } from '../contact-picker-modal/contact-picker.modal';
import { PlacePickerModalComponent } from '../place-picker-modal/place-picker.modal';
// To prevent compiling errors
const moment = moment_;
let EventUpsertModalComponent = class EventUpsertModalComponent {
    constructor(localNotifSrvc, store, loadingCtrl, modalCtrl, alertCtrl, translate, toastController, moduleService) {
        this.localNotifSrvc = localNotifSrvc;
        this.store = store;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.toastController = toastController;
        this.moduleService = moduleService;
        this.destroyed$ = new Subject();
        // startTime = moment().toISOString(true);
        // endTime = moment().add(1, 'hours').toISOString(true);
        this.monthNames = moment.months().toString();
        this.evAttendees = [];
        this.validationMessages = {
            title: this.moduleService.getAppConstants().VALIDATION_MESSAGES.title,
            place: this.moduleService.getAppConstants().VALIDATION_MESSAGES.place,
            datetime_from: this.moduleService.getAppConstants().VALIDATION_MESSAGES.datetime_from,
            datetime_to: this.moduleService.getAppConstants().VALIDATION_MESSAGES.datetime_to,
        };
        this.translate.get(['GENERAL', 'FORM', 'EVENTS'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t) => this.translations = t);
    }
    ngOnInit() {
        this.eventForm = new FormGroup({
            title: new FormControl('', Validators.required),
            place: new FormControl(),
            datetime_from: new FormControl(moment(this.period.from).toISOString(true), Validators.required),
            datetime_to: new FormControl(moment(this.period.to).add(1, 'hours').toISOString(true)),
            attendees: new FormControl(this.attendees || []),
            description: new FormControl(''),
            latitude: new FormControl(''),
            longitude: new FormControl('')
        });
        this.evAttendees = this.eventForm.get('attendees').value;
        if (this.action === 'UPDATE') {
            this.store.selectedEvent$()
                .pipe(takeUntil(this.destroyed$))
                .subscribe(id => {
                this.eventId = id;
                this.store.EventById$(id)
                    .pipe(takeUntil(this.destroyed$))
                    .subscribe(m => {
                    this.evAttendees = m.attendees.map(c => new ContactModel({
                        id: c.contactId, name: c.name, lastName: c.lastName, email: c.email, phone: c.phone
                    }));
                    this.eventForm.patchValue({
                        title: m.title,
                        place: m.place,
                        datetime_from: moment(m.datetimeFrom).toISOString(true),
                        datetime_to: moment(m.datetimeTo).toISOString(true),
                        attendees: this.evAttendees,
                        description: m.description
                    });
                });
            });
        }
        else {
            // only to reset the state.success property
            this.store.EventById$(null);
        }
        this._handleLoading();
        this._handleSuccess();
        this._handleError();
    }
    _handleLoading() {
        this.loading$ = this.store.Loading$;
        this.loading$
            .pipe(takeUntil(this.destroyed$), withLatestFrom(this.store.Error$))
            .subscribe(([loading, error]) => __awaiter(this, void 0, void 0, function* () {
            if (loading) {
                this.loader = yield this.loadingCtrl.create();
                this.loader.present();
            }
            else {
                if (this.loader) {
                    this.loadingCtrl.dismiss().then(() => {
                        this.loader = null;
                    });
                }
            }
        }));
    }
    _handleError() {
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe((e) => __awaiter(this, void 0, void 0, function* () {
            if (e) {
                const toast = yield this.toastController.create({
                    message: this.translations.EVENTS.ERRORS[e.after.toLowerCase()],
                    duration: 5000,
                    color: 'danger'
                });
                toast.present();
            }
        }));
    }
    _handleSuccess() {
        this.store.Success$
            .pipe(takeUntil(this.destroyed$))
            .subscribe((success) => __awaiter(this, void 0, void 0, function* () {
            if (success) {
                const toast = yield this.toastController.create({
                    message: this.translations.EVENTS.SUCCESS[success.after.toLowerCase()],
                    duration: 3000,
                    color: 'success'
                });
                toast.present().then(() => this.modalCtrl.dismiss({ eventId: this.eventId }));
                if (success.after === 'CREATE' || success.after === 'UPDATE') {
                    const momentStartTime = moment(this.eventForm.get('datetime_from').value);
                    const reminder = {
                        id: success.after === 'CREATE' ? success.data.id : this.eventId,
                        title: 'Evento próximo',
                        text: this.eventForm.get('title').value,
                        date: momentStartTime.subtract(10, 'minutes').set({ second: 0, millisecond: 0 }).toDate()
                    };
                    if (this.localNotifSrvc) {
                        if (success.after === 'CREATE') {
                            this.localNotifSrvc.setReminder(reminder);
                        }
                        else {
                            this.localNotifSrvc.updateReminder(reminder);
                        }
                    }
                }
            }
        }));
    }
    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
    upsertEvent() {
        this.eventForm.get('attendees')
            .patchValue(this.eventForm.get('attendees').value.map((c) => ({ contact_id: c.id })));
        this.action === 'CREATE' ?
            this.store.createEvent(this.eventForm.value) : this.store.updateEvent(this.eventId, this.eventForm.value);
    }
    openContactPicker() {
        return __awaiter(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: ContactPickerModalComponent,
                componentProps: {
                    currentAttendees: this.evAttendees.map(c => c.id)
                }
            });
            yield modal.present();
            const { data } = yield modal.onWillDismiss();
            if (data) {
                this.eventForm.patchValue({ attendees: [...data] });
                this.evAttendees = [...data];
            }
        });
    }
    removeAttendee(contact) {
        return __awaiter(this, void 0, void 0, function* () {
            this.translate.get('EVENTS.confirmRemoveAttendee', { name: `${contact.name} ${contact.lastName}` })
                .pipe(takeUntil(this.destroyed$))
                .subscribe((msg) => __awaiter(this, void 0, void 0, function* () {
                (yield this.alertCtrl.create({
                    header: this.translations.GENERAL.ACTION.confirm,
                    message: msg,
                    buttons: [{
                            text: this.translations.GENERAL.ACTION.ok,
                            handler: () => {
                                this.evAttendees = this.evAttendees.filter(c => c.id !== contact.id);
                                this.eventForm.patchValue({
                                    attendees: this.evAttendees
                                });
                            }
                        }, {
                            text: this.translations.GENERAL.ACTION.cancel,
                            role: 'cancel',
                            cssClass: 'primary',
                        }]
                })).present();
            }));
        });
    }
    showAttendeeDetails(contact) {
        return __awaiter(this, void 0, void 0, function* () {
            (yield this.alertCtrl.create({
                header: `${contact.name} ${contact.lastName}`,
                message: `<b>Email</b>: ${contact.email}<br><b>${this.translations.FORM.LABEL.phone}</b>: ${contact.phone}`,
                buttons: [{
                        text: this.translations.GENERAL.ACTION.ok,
                        role: 'cancel',
                        cssClass: 'primary',
                    }]
            })).present();
        });
    }
    openSearchPlace() {
        return __awaiter(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: PlacePickerModalComponent,
            });
            yield modal.present();
            const { data } = yield modal.onWillDismiss();
            if (data) {
                this.eventForm.patchValue({ place: data.description });
            }
        });
    }
    close() {
        this.modalCtrl.dismiss( /*{someData: ...} */);
    }
    dateChange(value, isStart = true) {
        const currentStartDate = moment(this.eventForm.value.datetime_from);
        const currentEndDate = moment(this.eventForm.value.datetime_to);
        if (isStart) {
            const startValue = moment(value);
            if (startValue > currentEndDate) {
                this.eventForm.patchValue({ datetime_to: currentStartDate.add(1, 'hours').toISOString(true) });
            }
        }
        else {
            const endValue = moment(value);
            if (endValue < currentStartDate) {
                this.eventForm.patchValue({ datetime_from: currentEndDate.subtract(1, 'hours').toISOString(true) });
            }
        }
    }
};
EventUpsertModalComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Optional }, { type: Inject, args: [LOCAL_NOTIFICATIONS_SERVICE,] }] },
    { type: EventsStore },
    { type: LoadingController },
    { type: ModalController },
    { type: AlertController },
    { type: TranslateService },
    { type: ToastController },
    { type: AppSettingsService }
];
__decorate([
    Input()
], EventUpsertModalComponent.prototype, "period", void 0);
__decorate([
    Input()
], EventUpsertModalComponent.prototype, "action", void 0);
__decorate([
    Input()
], EventUpsertModalComponent.prototype, "attendees", void 0);
EventUpsertModalComponent = __decorate([
    Component({
        selector: 'boxx-event-upsert',
        template: "<ion-header mode=\"md\" class=\"ion-no-border\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\">\n                {{'GENERAL.ACTION.'+ (action == 'CREATE' ? 'create' : 'update') | translate}} {{'GENERAL.event' |\n            translate}}\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <ion-buttons slot=\"end\">\n            <ion-button [disabled]=\"true\">\n                <ion-icon></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding-horizontal\">\n\n    <ion-list [formGroup]=\"eventForm\">\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.eventName\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-input formControlName=\"title\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.title\">\n            <div class=\"error-feedback\"\n                *ngIf=\"eventForm.get('title').hasError(validation.type) && eventForm.get('title').invalid && (eventForm.get('title').dirty || eventForm.get('title').touched)\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item custom-item ion-no-padding\">\n            <ion-label>\n                <p translate (click)=\"openSearchPlace()\">\n                    <ion-text color=\"primary\" translate>FORM.LABEL.place</ion-text>\n                    <ion-icon name=\"search-outline\" color=\"primary\" style=\"margin-left: 8px;\"></ion-icon>\n                </p>\n                <p [hidden]=\"eventForm.get('place').value\" (click)=\"openSearchPlace()\">\n                    <ion-text color=\"medium\" translate>EVENTS.tapToSearchPlace</ion-text>\n                </p>\n                <p [hidden]=\"!eventForm.get('place').value\" (click)=\"openSearchPlace()\" class=\"ion-text-wrap\">\n                    <ion-text color=\"medium\">{{eventForm.get('place').value}}</ion-text>\n                </p>\n            </ion-label>\n        </ion-item>\n        <!--<div *ngFor=\"let validation of validationMessages.place\">\n      <div class=\"error-feedback\"\n        *ngIf=\"eventForm.get('place').hasError(validation.type) && eventForm.get('place').invalid\">\n        <p>{{validation.message | translate: validation.params}}</p>\n      </div>\n    </div>-->\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                GENERAL.from\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-datetime #myStartPicker cancelText=\"{{'GENERAL.ACTION.cancel' | translate}}\" doneText=\"Ok\"\n                formControlName=\"datetime_from\" displayFormat=\"DD MMMM YYYY - HH:mm\"\n                value=\"{{eventForm.get('datetime_from').value}}\" (ionChange)=\"dateChange(myStartPicker.value)\"\n                placeholder=\"{{'GENERAL.ACTION.selectDate' | translate}}\" monthNames=\"{{monthNames}}\"></ion-datetime>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.datetime_from\">\n            <div class=\"error-feedback\"\n                *ngIf=\"eventForm.get('datetime_from').hasError(validation.type) && eventForm.get('datetime_from').invalid && (eventForm.get('datetime_from').dirty || eventForm.get('datetime_from').touched)\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                GENERAL.to\n                <!--<ion-text color=\"danger\">*</ion-text>-->\n            </ion-label>\n            <ion-datetime #myEndPicker cancelText=\"{{'GENERAL.ACTION.cancel' | translate}}\" doneText=\"Ok\"\n                formControlName=\"datetime_to\" (ionChange)=\"dateChange(myEndPicker.value, false)\"\n                displayFormat=\"DD MMMM YYYY - HH:mm\" value=\"{{eventForm.get('datetime_to').value}}\"\n                placeholder=\"{{'GENERAL.ACTION.selectDate' | translate}}\" monthNames=\"{{monthNames}}\"></ion-datetime>\n        </ion-item>\n        <!--<div *ngFor=\"let validation of validationMessages.datetime_to\">\n      <div class=\"error-feedback\"\n        *ngIf=\"eventForm.get('datetime_to').hasError(validation.type) && eventForm.get('datetime_to').invalid\">\n        <p>{{validation.message | translate: validation.params}}</p>\n      </div>\n    </div>-->\n\n        <ion-item class=\"boxx-input-item custom-item ion-no-padding\">\n            <ion-label class=\"attendee-chip\" color=\"primary\">\n                <p (click)=\"openContactPicker()\" class=\"vertical-center\">\n                    <ion-text color=\"primary\" translate>FORM.LABEL.attendees</ion-text>\n                    <ion-icon name=\"add-circle\" color=\"primary\"></ion-icon>\n                </p>\n                <p [hidden]=\"evAttendees.length > 0\" (click)=\"openContactPicker()\">\n                    <ion-text color=\"medium\" translate>EVENTS.tapToSelectContact</ion-text>\n                </p>\n                <ion-chip *ngFor=\"let contact of evAttendees\">\n                    <ion-avatar>\n                        <div>{{contact.name[0]}}{{contact.lastName[0]}}</div>\n                    </ion-avatar>\n                    <ion-label (click)=\"showAttendeeDetails(contact)\">{{contact.name}} {{contact.lastName}}</ion-label>\n                    <ion-icon name=\"close-circle-outline\" (click)=\"removeAttendee(contact)\"></ion-icon>\n                </ion-chip>\n            </ion-label>\n        </ion-item>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.description\n                <!--<ion-text color=\"danger\">*</ion-text>-->\n            </ion-label>\n            <ion-textarea formControlName=\"description\"></ion-textarea>\n        </ion-item>\n    </ion-list>\n\n    <div class=\"ion-margin-vertical\">\n        <ion-button color=\"primary\" expand=\"block\" [disabled]=\"eventForm.invalid\" (click)=\"upsertEvent()\">\n            {{(action == 'CREATE' ? 'GENERAL.ACTION.create': 'GENERAL.ACTION.update') | translate}} {{'GENERAL.event' |\n            translate}}\n        </ion-button>\n    </div>\n\n</ion-content>",
        styles: ["@charset \"UTF-8\";:host ion-toolbar.md ion-title{padding:0}:host .custom-item ion-label{padding-left:16px}:host .custom-item ion-label .attendee-chip p:first-child{display:flex;justify-content:space-between}:host .custom-item ion-label .attendee-chip p:first-child ion-icon{width:20px;height:20px}:host .custom-item ion-chip ion-label{padding-left:0}ion-label.attendee-chip p:first-child{display:flex;justify-content:space-between}ion-label.attendee-chip p:first-child ion-icon{width:20px!important;height:20px!important}"]
    }),
    __param(0, Optional()), __param(0, Inject(LOCAL_NOTIFICATIONS_SERVICE))
], EventUpsertModalComponent);
export { EventUpsertModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnQtdXBzZXJ0Lm1vZGFsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ldmVudC11cHNlcnQtbW9kYWwvZXZlbnQtdXBzZXJ0Lm1vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQXFCLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN0RixPQUFPLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNwRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDbkQsT0FBTyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDNUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxlQUFlLEVBQUUsZUFBZSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDdEcsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkQsT0FBTyxLQUFLLE9BQU8sTUFBTSxRQUFRLENBQUM7QUFDbEMsT0FBTyxFQUFjLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMzQyxPQUFPLEVBQUUsU0FBUyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSwyQkFBMkIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBRWhHLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQzNGLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBR3JGLDhCQUE4QjtBQUM5QixNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUM7QUFPdkIsSUFBYSx5QkFBeUIsR0FBdEMsTUFBYSx5QkFBeUI7SUFFbEMsWUFDNkQsY0FBMEMsRUFDM0YsS0FBa0IsRUFDbkIsV0FBOEIsRUFDN0IsU0FBMEIsRUFDMUIsU0FBMEIsRUFDMUIsU0FBMkIsRUFDM0IsZUFBZ0MsRUFDaEMsYUFBaUM7UUFQZ0IsbUJBQWMsR0FBZCxjQUFjLENBQTRCO1FBQzNGLFVBQUssR0FBTCxLQUFLLENBQWE7UUFDbkIsZ0JBQVcsR0FBWCxXQUFXLENBQW1CO1FBQzdCLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBQzFCLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBQzFCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBQzNCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyxrQkFBYSxHQUFiLGFBQWEsQ0FBb0I7UUFhN0MsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFLcEMsMENBQTBDO1FBQzFDLHdEQUF3RDtRQUV4RCxlQUFVLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRXhDLGdCQUFXLEdBQXdCLEVBQUUsQ0FBQztRQU10Qyx1QkFBa0IsR0FBRztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO1lBQ3JFLEtBQUssRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDLG1CQUFtQixDQUFDLEtBQUs7WUFDckUsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFLENBQUMsbUJBQW1CLENBQUMsYUFBYTtZQUNyRixXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXO1NBQ3BGLENBQUM7UUEvQkUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2FBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxDQUFDLENBQU0sRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBOEJELFFBQVE7UUFFSixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksU0FBUyxDQUFDO1lBQzNCLEtBQUssRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLFFBQVEsQ0FBQztZQUMvQyxLQUFLLEVBQUUsSUFBSSxXQUFXLEVBQUU7WUFDeEIsYUFBYSxFQUFFLElBQUksV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQy9GLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0RixTQUFTLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUM7WUFDaEQsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQztZQUNoQyxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDO1lBQzdCLFNBQVMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUM7U0FDakMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFFekQsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLFFBQVEsRUFBRTtZQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRTtpQkFDdEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7aUJBQ2hDLFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFBRTtnQkFDWixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztnQkFFbEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO3FCQUNwQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztxQkFDaEMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUVYLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQzlCLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxZQUFZLENBQUM7d0JBQ2xCLEVBQUUsRUFBRSxDQUFDLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUMsS0FBSztxQkFDdEYsQ0FBQyxDQUNMLENBQUM7b0JBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUM7d0JBQ3RCLEtBQUssRUFBRSxDQUFDLENBQUMsS0FBSzt3QkFDZCxLQUFLLEVBQUUsQ0FBQyxDQUFDLEtBQUs7d0JBQ2QsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQzt3QkFDdkQsV0FBVyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQzt3QkFDbkQsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXO3dCQUMzQixXQUFXLEVBQUUsQ0FBQyxDQUFDLFdBQVc7cUJBQzdCLENBQUMsQ0FBQztnQkFDUCxDQUFDLENBQUMsQ0FBQztZQUNYLENBQUMsQ0FBQyxDQUFDO1NBQ1Y7YUFDSTtZQUNELDJDQUEyQztZQUMzQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUMvQjtRQUVELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFTyxjQUFjO1FBQ2xCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7UUFFcEMsSUFBSSxDQUFDLFFBQVE7YUFDUixJQUFJLENBQ0QsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFDMUIsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQ3BDO2FBQ0EsU0FBUyxDQUFDLENBQU8sQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLEVBQUUsRUFBRTtZQUNsQyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDOUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUN6QjtpQkFDSTtnQkFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7b0JBQ2IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO3dCQUNqQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztvQkFDdkIsQ0FBQyxDQUFDLENBQUM7aUJBQ047YUFDSjtRQUNMLENBQUMsQ0FBQSxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU8sWUFBWTtRQUNoQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU07YUFDWixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTLENBQUMsQ0FBTyxDQUFDLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsRUFBRTtnQkFDSCxNQUFNLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDO29CQUM1QyxPQUFPLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQy9ELFFBQVEsRUFBRSxJQUFJO29CQUNkLEtBQUssRUFBRSxRQUFRO2lCQUNsQixDQUFDLENBQUM7Z0JBQ0gsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ25CO1FBQ0wsQ0FBQyxDQUFBLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTyxjQUFjO1FBQ2xCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUTthQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxDQUFPLE9BQU8sRUFBRSxFQUFFO1lBQ3pCLElBQUksT0FBTyxFQUFFO2dCQUNULE1BQU0sS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUM7b0JBQzVDLE9BQU8sRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDdEUsUUFBUSxFQUFFLElBQUk7b0JBQ2QsS0FBSyxFQUFFLFNBQVM7aUJBQ25CLENBQUMsQ0FBQztnQkFFSCxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBRTlFLElBQUksT0FBTyxDQUFDLEtBQUssS0FBSyxRQUFRLElBQUksT0FBTyxDQUFDLEtBQUssS0FBSyxRQUFRLEVBQUU7b0JBQzFELE1BQU0sZUFBZSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFFMUUsTUFBTSxRQUFRLEdBQW1CO3dCQUM3QixFQUFFLEVBQUUsT0FBTyxDQUFDLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFFLE9BQU8sQ0FBQyxJQUFtQixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU87d0JBQy9FLEtBQUssRUFBRSxnQkFBZ0I7d0JBQ3ZCLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLO3dCQUN2QyxJQUFJLEVBQUUsZUFBZSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxXQUFXLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLEVBQUU7cUJBQzVGLENBQUM7b0JBRUYsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFDO3dCQUNwQixJQUFJLE9BQU8sQ0FBQyxLQUFLLEtBQUssUUFBUSxFQUFFOzRCQUM1QixJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQzt5QkFDN0M7NkJBQ0k7NEJBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7eUJBQ2hEO3FCQUNKO2lCQUNKO2FBQ0o7UUFDTCxDQUFDLENBQUEsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDO2FBQzFCLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBZSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUV4RyxJQUFJLENBQUMsTUFBTSxLQUFLLFFBQVEsQ0FBQyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNsSCxDQUFDO0lBRUssaUJBQWlCOztZQUNuQixNQUFNLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO2dCQUN0QyxTQUFTLEVBQUUsMkJBQTJCO2dCQUN0QyxjQUFjLEVBQUU7b0JBQ1osZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2lCQUNwRDthQUNKLENBQUMsQ0FBQztZQUVILE1BQU0sS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBRXRCLE1BQU0sRUFBRSxJQUFJLEVBQUUsR0FBRyxNQUFNLEtBQUssQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUU3QyxJQUFJLElBQUksRUFBRTtnQkFDTixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNwRCxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQzthQUNoQztRQUNMLENBQUM7S0FBQTtJQUVLLGNBQWMsQ0FBQyxPQUFxQjs7WUFDdEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsOEJBQThCLEVBQUUsRUFBRSxJQUFJLEVBQUUsR0FBRyxPQUFPLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDO2lCQUM5RixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDaEMsU0FBUyxDQUFDLENBQU8sR0FBRyxFQUFFLEVBQUU7Z0JBQ3JCLENBQUMsTUFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQztvQkFDekIsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPO29CQUNoRCxPQUFPLEVBQUUsR0FBRztvQkFDWixPQUFPLEVBQUUsQ0FBQzs0QkFDTixJQUFJLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7NEJBQ3pDLE9BQU8sRUFBRSxHQUFHLEVBQUU7Z0NBQ1YsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dDQUNyRSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQztvQ0FDdEIsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXO2lDQUM5QixDQUFDLENBQUM7NEJBQ1AsQ0FBQzt5QkFDSixFQUFFOzRCQUNDLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTTs0QkFDN0MsSUFBSSxFQUFFLFFBQVE7NEJBQ2QsUUFBUSxFQUFFLFNBQVM7eUJBQ3RCLENBQUM7aUJBQ0wsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbEIsQ0FBQyxDQUFBLENBQUMsQ0FBQztRQUNYLENBQUM7S0FBQTtJQUVLLG1CQUFtQixDQUFDLE9BQXFCOztZQUMzQyxDQUFDLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7Z0JBQ3pCLE1BQU0sRUFBRSxHQUFHLE9BQU8sQ0FBQyxJQUFJLElBQUksT0FBTyxDQUFDLFFBQVEsRUFBRTtnQkFDN0MsT0FBTyxFQUFFLGlCQUFpQixPQUFPLENBQUMsS0FBSyxVQUFVLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsT0FBTyxDQUFDLEtBQUssRUFBRTtnQkFDM0csT0FBTyxFQUFFLENBQUM7d0JBQ04sSUFBSSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUN6QyxJQUFJLEVBQUUsUUFBUTt3QkFDZCxRQUFRLEVBQUUsU0FBUztxQkFFdEIsQ0FBQzthQUNMLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2xCLENBQUM7S0FBQTtJQUVLLGVBQWU7O1lBQ2pCLE1BQU0sS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7Z0JBQ3RDLFNBQVMsRUFBRSx5QkFBeUI7YUFFdkMsQ0FBQyxDQUFDO1lBRUgsTUFBTSxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7WUFFdEIsTUFBTSxFQUFFLElBQUksRUFBRSxHQUFHLE1BQU0sS0FBSyxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBRTdDLElBQUksSUFBSSxFQUFFO2dCQUNOLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO2FBQzFEO1FBQ0wsQ0FBQztLQUFBO0lBRUQsS0FBSztRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLLEVBQUUsT0FBTyxHQUFHLElBQUk7UUFDNUIsTUFBTSxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDcEUsTUFBTSxjQUFjLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2hFLElBQUksT0FBTyxFQUFFO1lBQ1QsTUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pDLElBQUksVUFBVSxHQUFHLGNBQWMsRUFBRTtnQkFDN0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ2xHO1NBQ0o7YUFBTTtZQUNILE1BQU0sUUFBUSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMvQixJQUFJLFFBQVEsR0FBRyxnQkFBZ0IsRUFBRTtnQkFDN0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFBRSxhQUFhLEVBQUUsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUN2RztTQUNKO0lBQ0wsQ0FBQztDQUNKLENBQUE7OzRDQS9RUSxRQUFRLFlBQUksTUFBTSxTQUFDLDJCQUEyQjtZQUNoQyxXQUFXO1lBQ04saUJBQWlCO1lBQ2xCLGVBQWU7WUFDZixlQUFlO1lBQ2YsZ0JBQWdCO1lBQ1YsZUFBZTtZQUNqQixrQkFBa0I7O0FBT3BDO0lBQVIsS0FBSyxFQUFFO3lEQUFnQztBQUMvQjtJQUFSLEtBQUssRUFBRTt5REFBNkI7QUFDNUI7SUFBUixLQUFLLEVBQUU7NERBQWdDO0FBbkIvQix5QkFBeUI7SUFMckMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLG1CQUFtQjtRQUM3QixrMk5BQXdDOztLQUUzQyxDQUFDO0lBSU8sV0FBQSxRQUFRLEVBQUUsQ0FBQSxFQUFFLFdBQUEsTUFBTSxDQUFDLDJCQUEyQixDQUFDLENBQUE7R0FIM0MseUJBQXlCLENBa1JyQztTQWxSWSx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEluamVjdCwgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0LCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIEZvcm1Hcm91cCwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IENvbnRhY3RNb2RlbCB9IGZyb20gJ0Bib3h4L2NvbnRhY3RzLWNvcmUnO1xuaW1wb3J0IHsgRXZlbnRNb2RlbCwgRXZlbnRzU3RvcmUsIElFdmVudFJlbWluZGVyIH0gZnJvbSAnQGJveHgvZXZlbnRzLWNvcmUnO1xuaW1wb3J0IHsgQWxlcnRDb250cm9sbGVyLCBMb2FkaW5nQ29udHJvbGxlciwgTW9kYWxDb250cm9sbGVyLCBUb2FzdENvbnRyb2xsZXIgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQgKiBhcyBtb21lbnRfIGZyb20gJ21vbWVudCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YWtlVW50aWwsIHdpdGhMYXRlc3RGcm9tIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQXBwU2V0dGluZ3NTZXJ2aWNlLCBMT0NBTF9OT1RJRklDQVRJT05TX1NFUlZJQ0UgfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvZ2xvYmFsLXBhcmFtcyc7XG5pbXBvcnQgeyBJTG9jYWxOb3RpZmljYXRpb25zU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC1tb2R1bGVzLm1vZHVsZSc7XG5pbXBvcnQgeyBDb250YWN0UGlja2VyTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi9jb250YWN0LXBpY2tlci1tb2RhbC9jb250YWN0LXBpY2tlci5tb2RhbCc7XG5pbXBvcnQgeyBQbGFjZVBpY2tlck1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vcGxhY2UtcGlja2VyLW1vZGFsL3BsYWNlLXBpY2tlci5tb2RhbCc7XG5cblxuLy8gVG8gcHJldmVudCBjb21waWxpbmcgZXJyb3JzXG5jb25zdCBtb21lbnQgPSBtb21lbnRfO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtZXZlbnQtdXBzZXJ0JyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vZXZlbnQtdXBzZXJ0Lm1vZGFsLmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL2V2ZW50LXVwc2VydC5tb2RhbC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIEV2ZW50VXBzZXJ0TW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgQE9wdGlvbmFsKCkgQEluamVjdChMT0NBTF9OT1RJRklDQVRJT05TX1NFUlZJQ0UpIHByaXZhdGUgbG9jYWxOb3RpZlNydmM6IElMb2NhbE5vdGlmaWNhdGlvbnNTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIHN0b3JlOiBFdmVudHNTdG9yZSxcbiAgICAgICAgcHVibGljIGxvYWRpbmdDdHJsOiBMb2FkaW5nQ29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSBtb2RhbEN0cmw6IE1vZGFsQ29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSBhbGVydEN0cmw6IEFsZXJ0Q29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgdG9hc3RDb250cm9sbGVyOiBUb2FzdENvbnRyb2xsZXIsXG4gICAgICAgIHByaXZhdGUgbW9kdWxlU2VydmljZTogQXBwU2V0dGluZ3NTZXJ2aWNlXG4gICAgKSB7XG5cbiAgICAgICAgdGhpcy50cmFuc2xhdGUuZ2V0KFsnR0VORVJBTCcsICdGT1JNJywgJ0VWRU5UUyddKVxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuZGVzdHJveWVkJCkpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKCh0OiBhbnkpID0+IHRoaXMudHJhbnNsYXRpb25zID0gdCk7XG4gICAgfVxuICAgIEBJbnB1dCgpIHBlcmlvZDogeyBmcm9tOiBhbnksIHRvOiBhbnkgfTtcbiAgICBASW5wdXQoKSBhY3Rpb246ICdDUkVBVEUnIHwgJ1VQREFURSc7XG4gICAgQElucHV0KCkgYXR0ZW5kZWVzOiBBcnJheTxDb250YWN0TW9kZWw+O1xuXG4gICAgZXZlbnQkOiBPYnNlcnZhYmxlPEV2ZW50TW9kZWw+O1xuICAgIGxvYWRpbmckOiBPYnNlcnZhYmxlPGJvb2xlYW4+O1xuICAgIGRlc3Ryb3llZCQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xuICAgIGV2ZW50SWQ6IG51bWJlcjtcblxuICAgIGxvYWRlcjogYW55O1xuXG4gICAgLy8gc3RhcnRUaW1lID0gbW9tZW50KCkudG9JU09TdHJpbmcodHJ1ZSk7XG4gICAgLy8gZW5kVGltZSA9IG1vbWVudCgpLmFkZCgxLCAnaG91cnMnKS50b0lTT1N0cmluZyh0cnVlKTtcblxuICAgIG1vbnRoTmFtZXMgPSBtb21lbnQubW9udGhzKCkudG9TdHJpbmcoKTtcblxuICAgIGV2QXR0ZW5kZWVzOiBBcnJheTxDb250YWN0TW9kZWw+ID0gW107XG5cbiAgICBldmVudEZvcm06IEZvcm1Hcm91cDtcblxuICAgIHRyYW5zbGF0aW9uczogYW55O1xuXG4gICAgdmFsaWRhdGlvbk1lc3NhZ2VzID0ge1xuICAgICAgICB0aXRsZTogdGhpcy5tb2R1bGVTZXJ2aWNlLmdldEFwcENvbnN0YW50cygpLlZBTElEQVRJT05fTUVTU0FHRVMudGl0bGUsXG4gICAgICAgIHBsYWNlOiB0aGlzLm1vZHVsZVNlcnZpY2UuZ2V0QXBwQ29uc3RhbnRzKCkuVkFMSURBVElPTl9NRVNTQUdFUy5wbGFjZSxcbiAgICAgICAgZGF0ZXRpbWVfZnJvbTogdGhpcy5tb2R1bGVTZXJ2aWNlLmdldEFwcENvbnN0YW50cygpLlZBTElEQVRJT05fTUVTU0FHRVMuZGF0ZXRpbWVfZnJvbSxcbiAgICAgICAgZGF0ZXRpbWVfdG86IHRoaXMubW9kdWxlU2VydmljZS5nZXRBcHBDb25zdGFudHMoKS5WQUxJREFUSU9OX01FU1NBR0VTLmRhdGV0aW1lX3RvLFxuICAgIH07XG5cbiAgICBuZ09uSW5pdCgpIHtcblxuICAgICAgICB0aGlzLmV2ZW50Rm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xuICAgICAgICAgICAgdGl0bGU6IG5ldyBGb3JtQ29udHJvbCgnJywgVmFsaWRhdG9ycy5yZXF1aXJlZCksXG4gICAgICAgICAgICBwbGFjZTogbmV3IEZvcm1Db250cm9sKCksXG4gICAgICAgICAgICBkYXRldGltZV9mcm9tOiBuZXcgRm9ybUNvbnRyb2wobW9tZW50KHRoaXMucGVyaW9kLmZyb20pLnRvSVNPU3RyaW5nKHRydWUpLCBWYWxpZGF0b3JzLnJlcXVpcmVkKSxcbiAgICAgICAgICAgIGRhdGV0aW1lX3RvOiBuZXcgRm9ybUNvbnRyb2wobW9tZW50KHRoaXMucGVyaW9kLnRvKS5hZGQoMSwgJ2hvdXJzJykudG9JU09TdHJpbmcodHJ1ZSkpLFxuICAgICAgICAgICAgYXR0ZW5kZWVzOiBuZXcgRm9ybUNvbnRyb2wodGhpcy5hdHRlbmRlZXMgfHwgW10pLFxuICAgICAgICAgICAgZGVzY3JpcHRpb246IG5ldyBGb3JtQ29udHJvbCgnJyksXG4gICAgICAgICAgICBsYXRpdHVkZTogbmV3IEZvcm1Db250cm9sKCcnKSxcbiAgICAgICAgICAgIGxvbmdpdHVkZTogbmV3IEZvcm1Db250cm9sKCcnKVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmV2QXR0ZW5kZWVzID0gdGhpcy5ldmVudEZvcm0uZ2V0KCdhdHRlbmRlZXMnKS52YWx1ZTtcblxuICAgICAgICBpZiAodGhpcy5hY3Rpb24gPT09ICdVUERBVEUnKSB7XG4gICAgICAgICAgICB0aGlzLnN0b3JlLnNlbGVjdGVkRXZlbnQkKClcbiAgICAgICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSlcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGlkID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ldmVudElkID0gaWQ7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdG9yZS5FdmVudEJ5SWQkKGlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuZGVzdHJveWVkJCkpXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKG0gPT4ge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ldkF0dGVuZGVlcyA9IG0uYXR0ZW5kZWVzLm1hcChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYyA9PiBuZXcgQ29udGFjdE1vZGVsKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBjLmNvbnRhY3RJZCwgbmFtZTogYy5uYW1lLCBsYXN0TmFtZTogYy5sYXN0TmFtZSwgZW1haWw6IGMuZW1haWwsIHBob25lOiBjLnBob25lXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZXZlbnRGb3JtLnBhdGNoVmFsdWUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogbS50aXRsZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2U6IG0ucGxhY2UsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGV0aW1lX2Zyb206IG1vbWVudChtLmRhdGV0aW1lRnJvbSkudG9JU09TdHJpbmcodHJ1ZSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGV0aW1lX3RvOiBtb21lbnQobS5kYXRldGltZVRvKS50b0lTT1N0cmluZyh0cnVlKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0ZW5kZWVzOiB0aGlzLmV2QXR0ZW5kZWVzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbS5kZXNjcmlwdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAvLyBvbmx5IHRvIHJlc2V0IHRoZSBzdGF0ZS5zdWNjZXNzIHByb3BlcnR5XG4gICAgICAgICAgICB0aGlzLnN0b3JlLkV2ZW50QnlJZCQobnVsbCk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLl9oYW5kbGVMb2FkaW5nKCk7XG4gICAgICAgIHRoaXMuX2hhbmRsZVN1Y2Nlc3MoKTtcbiAgICAgICAgdGhpcy5faGFuZGxlRXJyb3IoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIF9oYW5kbGVMb2FkaW5nKCkge1xuICAgICAgICB0aGlzLmxvYWRpbmckID0gdGhpcy5zdG9yZS5Mb2FkaW5nJDtcblxuICAgICAgICB0aGlzLmxvYWRpbmckXG4gICAgICAgICAgICAucGlwZShcbiAgICAgICAgICAgICAgICB0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSxcbiAgICAgICAgICAgICAgICB3aXRoTGF0ZXN0RnJvbSh0aGlzLnN0b3JlLkVycm9yJCksXG4gICAgICAgICAgICApXG4gICAgICAgICAgICAuc3Vic2NyaWJlKGFzeW5jIChbbG9hZGluZywgZXJyb3JdKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGxvYWRpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXIgPSBhd2FpdCB0aGlzLmxvYWRpbmdDdHJsLmNyZWF0ZSgpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlci5wcmVzZW50KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5sb2FkZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ0N0cmwuZGlzbWlzcygpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfaGFuZGxlRXJyb3IoKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuRXJyb3IkXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoYXN5bmMgKGUpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoZSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCB0b2FzdCA9IGF3YWl0IHRoaXMudG9hc3RDb250cm9sbGVyLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiB0aGlzLnRyYW5zbGF0aW9ucy5FVkVOVFMuRVJST1JTW2UuYWZ0ZXIudG9Mb3dlckNhc2UoKV0sXG4gICAgICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogNTAwMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAnZGFuZ2VyJ1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgdG9hc3QucHJlc2VudCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgX2hhbmRsZVN1Y2Nlc3MoKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuU3VjY2VzcyRcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLmRlc3Ryb3llZCQpKVxuICAgICAgICAgICAgLnN1YnNjcmliZShhc3luYyAoc3VjY2VzcykgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChzdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHRvYXN0ID0gYXdhaXQgdGhpcy50b2FzdENvbnRyb2xsZXIuY3JlYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IHRoaXMudHJhbnNsYXRpb25zLkVWRU5UUy5TVUNDRVNTW3N1Y2Nlc3MuYWZ0ZXIudG9Mb3dlckNhc2UoKV0sXG4gICAgICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogMzAwMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAnc3VjY2VzcydcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgdG9hc3QucHJlc2VudCgpLnRoZW4oKCkgPT4gdGhpcy5tb2RhbEN0cmwuZGlzbWlzcyh7IGV2ZW50SWQ6IHRoaXMuZXZlbnRJZCB9KSk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHN1Y2Nlc3MuYWZ0ZXIgPT09ICdDUkVBVEUnIHx8IHN1Y2Nlc3MuYWZ0ZXIgPT09ICdVUERBVEUnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBtb21lbnRTdGFydFRpbWUgPSBtb21lbnQodGhpcy5ldmVudEZvcm0uZ2V0KCdkYXRldGltZV9mcm9tJykudmFsdWUpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZW1pbmRlcjogSUV2ZW50UmVtaW5kZXIgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IHN1Y2Nlc3MuYWZ0ZXIgPT09ICdDUkVBVEUnID8gKHN1Y2Nlc3MuZGF0YSBhcyBFdmVudE1vZGVsKS5pZCA6IHRoaXMuZXZlbnRJZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0V2ZW50byBwcsOzeGltbycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogdGhpcy5ldmVudEZvcm0uZ2V0KCd0aXRsZScpLnZhbHVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGU6IG1vbWVudFN0YXJ0VGltZS5zdWJ0cmFjdCgxMCwgJ21pbnV0ZXMnKS5zZXQoeyBzZWNvbmQ6IDAsIG1pbGxpc2Vjb25kOiAwIH0pLnRvRGF0ZSgpXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5sb2NhbE5vdGlmU3J2Yyl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHN1Y2Nlc3MuYWZ0ZXIgPT09ICdDUkVBVEUnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9jYWxOb3RpZlNydmMuc2V0UmVtaW5kZXIocmVtaW5kZXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2NhbE5vdGlmU3J2Yy51cGRhdGVSZW1pbmRlcihyZW1pbmRlcik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMuZGVzdHJveWVkJC5uZXh0KHRydWUpO1xuICAgICAgICB0aGlzLmRlc3Ryb3llZCQuY29tcGxldGUoKTtcbiAgICB9XG5cbiAgICB1cHNlcnRFdmVudCgpIHtcbiAgICAgICAgdGhpcy5ldmVudEZvcm0uZ2V0KCdhdHRlbmRlZXMnKVxuICAgICAgICAgICAgLnBhdGNoVmFsdWUodGhpcy5ldmVudEZvcm0uZ2V0KCdhdHRlbmRlZXMnKS52YWx1ZS5tYXAoKGM6IENvbnRhY3RNb2RlbCkgPT4gKHsgY29udGFjdF9pZDogYy5pZCB9KSkpO1xuXG4gICAgICAgIHRoaXMuYWN0aW9uID09PSAnQ1JFQVRFJyA/XG4gICAgICAgICAgICB0aGlzLnN0b3JlLmNyZWF0ZUV2ZW50KHRoaXMuZXZlbnRGb3JtLnZhbHVlKSA6IHRoaXMuc3RvcmUudXBkYXRlRXZlbnQodGhpcy5ldmVudElkLCB0aGlzLmV2ZW50Rm9ybS52YWx1ZSk7XG4gICAgfVxuXG4gICAgYXN5bmMgb3BlbkNvbnRhY3RQaWNrZXIoKSB7XG4gICAgICAgIGNvbnN0IG1vZGFsID0gYXdhaXQgdGhpcy5tb2RhbEN0cmwuY3JlYXRlKHtcbiAgICAgICAgICAgIGNvbXBvbmVudDogQ29udGFjdFBpY2tlck1vZGFsQ29tcG9uZW50LFxuICAgICAgICAgICAgY29tcG9uZW50UHJvcHM6IHtcbiAgICAgICAgICAgICAgICBjdXJyZW50QXR0ZW5kZWVzOiB0aGlzLmV2QXR0ZW5kZWVzLm1hcChjID0+IGMuaWQpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGF3YWl0IG1vZGFsLnByZXNlbnQoKTtcblxuICAgICAgICBjb25zdCB7IGRhdGEgfSA9IGF3YWl0IG1vZGFsLm9uV2lsbERpc21pc3MoKTtcblxuICAgICAgICBpZiAoZGF0YSkge1xuICAgICAgICAgICAgdGhpcy5ldmVudEZvcm0ucGF0Y2hWYWx1ZSh7IGF0dGVuZGVlczogWy4uLmRhdGFdIH0pO1xuICAgICAgICAgICAgdGhpcy5ldkF0dGVuZGVlcyA9IFsuLi5kYXRhXTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFzeW5jIHJlbW92ZUF0dGVuZGVlKGNvbnRhY3Q6IENvbnRhY3RNb2RlbCkge1xuICAgICAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoJ0VWRU5UUy5jb25maXJtUmVtb3ZlQXR0ZW5kZWUnLCB7IG5hbWU6IGAke2NvbnRhY3QubmFtZX0gJHtjb250YWN0Lmxhc3ROYW1lfWAgfSlcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLmRlc3Ryb3llZCQpKVxuICAgICAgICAgICAgLnN1YnNjcmliZShhc3luYyAobXNnKSA9PiB7XG4gICAgICAgICAgICAgICAgKGF3YWl0IHRoaXMuYWxlcnRDdHJsLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGhlYWRlcjogdGhpcy50cmFuc2xhdGlvbnMuR0VORVJBTC5BQ1RJT04uY29uZmlybSxcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogbXNnLFxuICAgICAgICAgICAgICAgICAgICBidXR0b25zOiBbe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogdGhpcy50cmFuc2xhdGlvbnMuR0VORVJBTC5BQ1RJT04ub2ssXG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVyOiAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ldkF0dGVuZGVlcyA9IHRoaXMuZXZBdHRlbmRlZXMuZmlsdGVyKGMgPT4gYy5pZCAhPT0gY29udGFjdC5pZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ldmVudEZvcm0ucGF0Y2hWYWx1ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dGVuZGVlczogdGhpcy5ldkF0dGVuZGVlc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiB0aGlzLnRyYW5zbGF0aW9ucy5HRU5FUkFMLkFDVElPTi5jYW5jZWwsXG4gICAgICAgICAgICAgICAgICAgICAgICByb2xlOiAnY2FuY2VsJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNzc0NsYXNzOiAncHJpbWFyeScsXG4gICAgICAgICAgICAgICAgICAgIH1dXG4gICAgICAgICAgICAgICAgfSkpLnByZXNlbnQoKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIGFzeW5jIHNob3dBdHRlbmRlZURldGFpbHMoY29udGFjdDogQ29udGFjdE1vZGVsKSB7XG4gICAgICAgIChhd2FpdCB0aGlzLmFsZXJ0Q3RybC5jcmVhdGUoe1xuICAgICAgICAgICAgaGVhZGVyOiBgJHtjb250YWN0Lm5hbWV9ICR7Y29udGFjdC5sYXN0TmFtZX1gLFxuICAgICAgICAgICAgbWVzc2FnZTogYDxiPkVtYWlsPC9iPjogJHtjb250YWN0LmVtYWlsfTxicj48Yj4ke3RoaXMudHJhbnNsYXRpb25zLkZPUk0uTEFCRUwucGhvbmV9PC9iPjogJHtjb250YWN0LnBob25lfWAsXG4gICAgICAgICAgICBidXR0b25zOiBbe1xuICAgICAgICAgICAgICAgIHRleHQ6IHRoaXMudHJhbnNsYXRpb25zLkdFTkVSQUwuQUNUSU9OLm9rLFxuICAgICAgICAgICAgICAgIHJvbGU6ICdjYW5jZWwnLFxuICAgICAgICAgICAgICAgIGNzc0NsYXNzOiAncHJpbWFyeScsXG4gICAgICAgICAgICAgICAgLy8gaGFuZGxlcjogKCkgPT4geyB9XG4gICAgICAgICAgICB9XVxuICAgICAgICB9KSkucHJlc2VudCgpO1xuICAgIH1cblxuICAgIGFzeW5jIG9wZW5TZWFyY2hQbGFjZSgpIHtcbiAgICAgICAgY29uc3QgbW9kYWwgPSBhd2FpdCB0aGlzLm1vZGFsQ3RybC5jcmVhdGUoe1xuICAgICAgICAgICAgY29tcG9uZW50OiBQbGFjZVBpY2tlck1vZGFsQ29tcG9uZW50LFxuICAgICAgICAgICAgLy8gY29tcG9uZW50UHJvcHM6IHt9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGF3YWl0IG1vZGFsLnByZXNlbnQoKTtcblxuICAgICAgICBjb25zdCB7IGRhdGEgfSA9IGF3YWl0IG1vZGFsLm9uV2lsbERpc21pc3MoKTtcblxuICAgICAgICBpZiAoZGF0YSkge1xuICAgICAgICAgICAgdGhpcy5ldmVudEZvcm0ucGF0Y2hWYWx1ZSh7IHBsYWNlOiBkYXRhLmRlc2NyaXB0aW9uIH0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgY2xvc2UoKSB7XG4gICAgICAgIHRoaXMubW9kYWxDdHJsLmRpc21pc3MoLyp7c29tZURhdGE6IC4uLn0gKi8pO1xuICAgIH1cblxuICAgIGRhdGVDaGFuZ2UodmFsdWUsIGlzU3RhcnQgPSB0cnVlKSB7XG4gICAgICAgIGNvbnN0IGN1cnJlbnRTdGFydERhdGUgPSBtb21lbnQodGhpcy5ldmVudEZvcm0udmFsdWUuZGF0ZXRpbWVfZnJvbSk7XG4gICAgICAgIGNvbnN0IGN1cnJlbnRFbmREYXRlID0gbW9tZW50KHRoaXMuZXZlbnRGb3JtLnZhbHVlLmRhdGV0aW1lX3RvKTtcbiAgICAgICAgaWYgKGlzU3RhcnQpIHtcbiAgICAgICAgICAgIGNvbnN0IHN0YXJ0VmFsdWUgPSBtb21lbnQodmFsdWUpO1xuICAgICAgICAgICAgaWYgKHN0YXJ0VmFsdWUgPiBjdXJyZW50RW5kRGF0ZSkge1xuICAgICAgICAgICAgICAgIHRoaXMuZXZlbnRGb3JtLnBhdGNoVmFsdWUoeyBkYXRldGltZV90bzogY3VycmVudFN0YXJ0RGF0ZS5hZGQoMSwgJ2hvdXJzJykudG9JU09TdHJpbmcodHJ1ZSkgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb25zdCBlbmRWYWx1ZSA9IG1vbWVudCh2YWx1ZSk7XG4gICAgICAgICAgICBpZiAoZW5kVmFsdWUgPCBjdXJyZW50U3RhcnREYXRlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5ldmVudEZvcm0ucGF0Y2hWYWx1ZSh7IGRhdGV0aW1lX2Zyb206IGN1cnJlbnRFbmREYXRlLnN1YnRyYWN0KDEsICdob3VycycpLnRvSVNPU3RyaW5nKHRydWUpIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuIl19