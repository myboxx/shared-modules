import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { AppVersionService } from '../../services/NativeAppVersion.service';
let NativeVersionInfoComponent = class NativeVersionInfoComponent {
    constructor(appVersionService) {
        this.appVersionService = appVersionService;
    }
    ngOnInit() { }
};
NativeVersionInfoComponent.ctorParameters = () => [
    { type: AppVersionService }
];
NativeVersionInfoComponent = __decorate([
    Component({
        selector: 'boxx-native-version-info',
        template: "<div class=\"app-version-number\">\n    <div *ngIf=\"(appVersionService.whenReady$ | async) as appVersionService\">\n        {{appVersionService.versionNumber && appVersionService.versionNumber !== 'PWA' ? 'v'+appVersionService.versionNumber : 'PWA'}}\n    </div>\n</div>",
        styles: [""]
    })
], NativeVersionInfoComponent);
export { NativeVersionInfoComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF0aXZlLXZlcnNpb24taW5mby5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtbW9kdWxlcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL25hdGl2ZS12ZXJzaW9uLWluZm8vbmF0aXZlLXZlcnNpb24taW5mby5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDbEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFPNUUsSUFBYSwwQkFBMEIsR0FBdkMsTUFBYSwwQkFBMEI7SUFFckMsWUFDUyxpQkFBb0M7UUFBcEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtJQUN6QyxDQUFDO0lBRUwsUUFBUSxLQUFJLENBQUM7Q0FFZCxDQUFBOztZQUw2QixpQkFBaUI7O0FBSGxDLDBCQUEwQjtJQUx0QyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsMEJBQTBCO1FBQ3BDLDJSQUFtRDs7S0FFcEQsQ0FBQztHQUNXLDBCQUEwQixDQVF0QztTQVJZLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBcHBWZXJzaW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL05hdGl2ZUFwcFZlcnNpb24uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2JveHgtbmF0aXZlLXZlcnNpb24taW5mbycsXG4gIHRlbXBsYXRlVXJsOiAnLi9uYXRpdmUtdmVyc2lvbi1pbmZvLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbmF0aXZlLXZlcnNpb24taW5mby5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBOYXRpdmVWZXJzaW9uSW5mb0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGFwcFZlcnNpb25TZXJ2aWNlOiBBcHBWZXJzaW9uU2VydmljZVxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge31cblxufVxuIl19