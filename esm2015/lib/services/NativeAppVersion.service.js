import { __awaiter, __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@ionic-native/app-version/ngx/index";
import * as i2 from "@ionic/angular";
let AppVersionService = class AppVersionService {
    constructor(appVersion, platform) {
        this.appVersion = appVersion;
        this.platform = platform;
        this.whenReadySubject = new BehaviorSubject({
            appName: undefined,
            packageName: undefined,
            versionCode: undefined,
            versionNumber: 'PWA'
        });
        this.whenReady$ = this.whenReadySubject.asObservable();
        this.platform.ready().then(() => __awaiter(this, void 0, void 0, function* () {
            if (this.platform.is('cordova') || this.platform.is('capacitor')) {
                this.appNameVal = yield this.appVersion.getAppName();
                this.packageNameVal = yield this.appVersion.getPackageName();
                this.versionCodeVal = yield this.appVersion.getVersionCode();
                this.versionNumberVal = yield this.appVersion.getVersionNumber();
            }
            this.whenReadySubject.next({
                appName: this.appNameVal,
                packageName: this.packageNameVal,
                versionCode: this.versionCodeVal,
                versionNumber: this.versionNumberVal
            });
        }));
    }
    appName() {
        return this.appNameVal;
    }
    packageName() {
        return this.packageNameVal;
    }
    versionCode() {
        return this.versionCodeVal;
    }
    versionNumber() {
        return this.versionNumberVal;
    }
};
AppVersionService.ctorParameters = () => [
    { type: AppVersion },
    { type: Platform }
];
AppVersionService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AppVersionService_Factory() { return new AppVersionService(i0.ɵɵinject(i1.AppVersion), i0.ɵɵinject(i2.Platform)); }, token: AppVersionService, providedIn: "root" });
AppVersionService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], AppVersionService);
export { AppVersionService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTmF0aXZlQXBwVmVyc2lvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvTmF0aXZlQXBwVmVyc2lvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUMzRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDMUMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7OztBQUt2QyxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQUUxQixZQUNZLFVBQXNCLEVBQ3RCLFFBQWtCO1FBRGxCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQXdCdEIscUJBQWdCLEdBQUcsSUFBSSxlQUFlLENBSzNDO1lBQ0MsT0FBTyxFQUFFLFNBQVM7WUFDbEIsV0FBVyxFQUFFLFNBQVM7WUFDdEIsV0FBVyxFQUFFLFNBQVM7WUFDdEIsYUFBYSxFQUFFLEtBQUs7U0FDdkIsQ0FBQyxDQUFDO1FBRUksZUFBVSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQWxDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBUyxFQUFFO1lBQ2xDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUM7Z0JBQzdELElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNyRCxJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDN0QsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQzdELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzthQUNwRTtZQUVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZCLE9BQU8sRUFBRSxJQUFJLENBQUMsVUFBVTtnQkFDeEIsV0FBVyxFQUFFLElBQUksQ0FBQyxjQUFjO2dCQUNoQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGNBQWM7Z0JBQ2hDLGFBQWEsRUFBRSxJQUFJLENBQUMsZ0JBQWdCO2FBQ3ZDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQSxDQUFDLENBQUM7SUFDUCxDQUFDO0lBcUJELE9BQU87UUFDSCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0IsQ0FBQztJQUVELFdBQVc7UUFDUCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0IsQ0FBQztJQUVELFdBQVc7UUFDUCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0IsQ0FBQztJQUVELGFBQWE7UUFDVCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztJQUNqQyxDQUFDO0NBQ0osQ0FBQTs7WUF0RDJCLFVBQVU7WUFDWixRQUFROzs7QUFKckIsaUJBQWlCO0lBSDdCLFVBQVUsQ0FBQztRQUNSLFVBQVUsRUFBRSxNQUFNO0tBQ3JCLENBQUM7R0FDVyxpQkFBaUIsQ0F5RDdCO1NBekRZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEFwcFZlcnNpb24gfSBmcm9tICdAaW9uaWMtbmF0aXZlL2FwcC12ZXJzaW9uL25neCc7XG5pbXBvcnQgeyBQbGF0Zm9ybSB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEFwcFZlcnNpb25TZXJ2aWNlIHtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIGFwcFZlcnNpb246IEFwcFZlcnNpb24sXG4gICAgICAgIHByaXZhdGUgcGxhdGZvcm06IFBsYXRmb3JtXG4gICAgKSB7XG4gICAgICAgIHRoaXMucGxhdGZvcm0ucmVhZHkoKS50aGVuKGFzeW5jICgpID0+IHtcbiAgICAgICAgICAgIGlmICh0aGlzLnBsYXRmb3JtLmlzKCdjb3Jkb3ZhJykgfHwgdGhpcy5wbGF0Zm9ybS5pcygnY2FwYWNpdG9yJykpe1xuICAgICAgICAgICAgICAgIHRoaXMuYXBwTmFtZVZhbCA9IGF3YWl0IHRoaXMuYXBwVmVyc2lvbi5nZXRBcHBOYW1lKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5wYWNrYWdlTmFtZVZhbCA9IGF3YWl0IHRoaXMuYXBwVmVyc2lvbi5nZXRQYWNrYWdlTmFtZSgpO1xuICAgICAgICAgICAgICAgIHRoaXMudmVyc2lvbkNvZGVWYWwgPSBhd2FpdCB0aGlzLmFwcFZlcnNpb24uZ2V0VmVyc2lvbkNvZGUoKTtcbiAgICAgICAgICAgICAgICB0aGlzLnZlcnNpb25OdW1iZXJWYWwgPSBhd2FpdCB0aGlzLmFwcFZlcnNpb24uZ2V0VmVyc2lvbk51bWJlcigpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLndoZW5SZWFkeVN1YmplY3QubmV4dCh7XG4gICAgICAgICAgICAgICAgYXBwTmFtZTogdGhpcy5hcHBOYW1lVmFsLFxuICAgICAgICAgICAgICAgIHBhY2thZ2VOYW1lOiB0aGlzLnBhY2thZ2VOYW1lVmFsLFxuICAgICAgICAgICAgICAgIHZlcnNpb25Db2RlOiB0aGlzLnZlcnNpb25Db2RlVmFsLFxuICAgICAgICAgICAgICAgIHZlcnNpb25OdW1iZXI6IHRoaXMudmVyc2lvbk51bWJlclZhbFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXBwTmFtZVZhbDogc3RyaW5nO1xuICAgIHByaXZhdGUgcGFja2FnZU5hbWVWYWw6IHN0cmluZztcbiAgICBwcml2YXRlIHZlcnNpb25Db2RlVmFsOiBzdHJpbmcgfCBudW1iZXI7XG4gICAgcHJpdmF0ZSB2ZXJzaW9uTnVtYmVyVmFsOiBzdHJpbmc7XG5cbiAgICBwcml2YXRlIHdoZW5SZWFkeVN1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PHtcbiAgICAgICAgYXBwTmFtZTogc3RyaW5nLFxuICAgICAgICBwYWNrYWdlTmFtZTogc3RyaW5nLFxuICAgICAgICB2ZXJzaW9uQ29kZTogc3RyaW5nIHwgbnVtYmVyLFxuICAgICAgICB2ZXJzaW9uTnVtYmVyOiBzdHJpbmdcbiAgICB9Pih7XG4gICAgICAgIGFwcE5hbWU6IHVuZGVmaW5lZCxcbiAgICAgICAgcGFja2FnZU5hbWU6IHVuZGVmaW5lZCxcbiAgICAgICAgdmVyc2lvbkNvZGU6IHVuZGVmaW5lZCxcbiAgICAgICAgdmVyc2lvbk51bWJlcjogJ1BXQSdcbiAgICB9KTtcblxuICAgIHB1YmxpYyB3aGVuUmVhZHkkID0gdGhpcy53aGVuUmVhZHlTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuXG4gICAgYXBwTmFtZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBwTmFtZVZhbDtcbiAgICB9XG5cbiAgICBwYWNrYWdlTmFtZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucGFja2FnZU5hbWVWYWw7XG4gICAgfVxuXG4gICAgdmVyc2lvbkNvZGUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnZlcnNpb25Db2RlVmFsO1xuICAgIH1cblxuICAgIHZlcnNpb25OdW1iZXIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnZlcnNpb25OdW1iZXJWYWw7XG4gICAgfVxufVxuIl19