import { InjectionToken } from '@angular/core';
import { SharedModulesOptionsInterface } from '../shared-modules.module';
export declare class AppSettingsService {
    constructor(settings: SharedModulesOptionsInterface);
    private appConstants;
    private setAppConstants;
    getAppConstants(): any;
}
export declare const LOCAL_NOTIFICATIONS_SERVICE: InjectionToken<unknown>;
