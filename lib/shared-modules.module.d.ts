import { InjectionToken, ModuleWithProviders, Provider } from '@angular/core';
import { IEventReminder } from '@boxx/events-core';
import { AppSettingsService } from './providers/global-params';
export declare const AppSettingsObject: InjectionToken<unknown>;
export declare function createAppSettingsService(settings: SharedModulesOptionsInterface): AppSettingsService;
export interface SharedModulesOptionsInterface {
    appConstants: any;
    providers: Provider[];
}
export interface ILocalNotificationsService {
    setReminder(reminder: IEventReminder): any;
    removeReminder(id: number): any;
    updateReminder(reminder: IEventReminder): any;
    checkIdScheduled(id: number): any;
    init(): any;
    stop(): any;
}
export declare class SharedModulesModule {
    static forRoot(config: SharedModulesOptionsInterface): ModuleWithProviders<SharedModulesModule>;
}
