import { AppVersion } from '@ionic-native/app-version/ngx';
import { Platform } from '@ionic/angular';
export declare class AppVersionService {
    private appVersion;
    private platform;
    constructor(appVersion: AppVersion, platform: Platform);
    private appNameVal;
    private packageNameVal;
    private versionCodeVal;
    private versionNumberVal;
    private whenReadySubject;
    whenReady$: import("rxjs").Observable<{
        appName: string;
        packageName: string;
        versionCode: string | number;
        versionNumber: string;
    }>;
    appName(): string;
    packageName(): string;
    versionCode(): string | number;
    versionNumber(): string;
}
