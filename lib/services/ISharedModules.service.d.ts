import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
export interface IFirebasexService {
    fetch(cacheExpirationSeconds?: number): Promise<any>;
    activateFetched(): Promise<any>;
    getValue(value: string): Promise<any>;
    setAnalyticsCollectionEnabled(val: boolean): Promise<any>;
    setCrashlyticsCollectionEnabled(val: boolean): Promise<any>;
    getToken(): Promise<any>;
    getAPNSToken(): Promise<any>;
    onMessageReceived(): Observable<any>;
    setScreenName(name: string): Promise<any>;
    logEvent(type: string, data: any): Promise<any>;
    setUserId(id: string): Promise<any>;
    setUserProperty(name: string, value: any): Promise<any>;
}
export declare const FIREBASEX_SERVICE: InjectionToken<IFirebasexService>;
