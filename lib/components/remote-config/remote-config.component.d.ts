import { EventEmitter, OnInit } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { AlertController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { IFirebasexService } from '../../services/ISharedModules.service';
export declare class RemoteConfigComponent implements OnInit {
    private firebasex;
    private translate;
    private appVersion;
    private market;
    private alertCtrl;
    private platform;
    whenFinish: EventEmitter<boolean>;
    constructor(firebasex: IFirebasexService, translate: TranslateService, appVersion: AppVersion, market: Market, alertCtrl: AlertController, platform: Platform);
    destroyed$: Subject<boolean>;
    translations: any;
    configMessage: string;
    isIos: boolean;
    ngOnInit(): void;
    checkForUpdates(): Promise<void>;
}
