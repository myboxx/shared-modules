import { OnDestroy, OnInit } from '@angular/core';
import { ContactModel, ContactStore } from '@boxx/contacts-core';
import { AlertController, IonSearchbar, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
export declare class ContactPickerModalComponent implements OnInit, OnDestroy {
    private modalCtrl;
    private store;
    private alertCtrl;
    private translate;
    searchBar: IonSearchbar;
    currentAttendees: number[];
    constructor(modalCtrl: ModalController, store: ContactStore, alertCtrl: AlertController, translate: TranslateService);
    searchValue: string;
    contactList: Array<{
        isChecked: boolean;
        contact: ContactModel;
    }>;
    searchResults: Array<{
        isChecked: boolean;
        contact: ContactModel;
    }>;
    isFiltering: boolean;
    isIndeterminate: boolean;
    masterCheck: boolean;
    destroyed$: Subject<boolean>;
    isLoading$: Observable<boolean>;
    translations: any;
    ngOnInit(): void;
    ngOnDestroy(): void;
    checkMaster(): void;
    checkEvent(): void;
    close(): Promise<void>;
    select(): Promise<boolean>;
    private _getSelectedContacts;
    hasChanges(): boolean;
    search(searchTerm: string): void;
    showAll(): void;
}
