import { OnInit } from '@angular/core';
import { AppVersionService } from '../../services/NativeAppVersion.service';
export declare class NativeVersionInfoComponent implements OnInit {
    appVersionService: AppVersionService;
    constructor(appVersionService: AppVersionService);
    ngOnInit(): void;
}
