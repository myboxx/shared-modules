import { OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ContactModel, ContactStore } from '@boxx/contacts-core';
import { LoadingController, ModalController, PopoverController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { AppSettingsService } from '../../providers/global-params';
export declare class ContactUpsertModalComponent implements OnInit, OnDestroy {
    private modalCtrl;
    private store;
    private translate;
    private popoverController;
    mdalCtrl: ModalController;
    private loadingCtrl;
    private moduleService;
    contact: ContactModel;
    mode: 'CREATE' | 'UPDATE';
    constructor(modalCtrl: ModalController, store: ContactStore, translate: TranslateService, popoverController: PopoverController, mdalCtrl: ModalController, loadingCtrl: LoadingController, moduleService: AppSettingsService);
    destroyed$: Subject<boolean>;
    contactForm: FormGroup;
    types: {
        key: string;
        value: string;
    }[];
    validationMessages: {
        name: any;
        last_name: any;
        phone: any;
        email: any;
        address: any;
    };
    ngOnInit(): void;
    ngOnDestroy(): void;
    close(): void;
    update(): void;
    create(): void;
    presentPopover(): Promise<void>;
    private _appendCountryCode;
}
