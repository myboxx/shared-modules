import { OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
export declare class CountryCodeSelectorPopoverComponent implements OnInit {
    popoverController: PopoverController;
    codes: Array<number>;
    constructor(popoverController: PopoverController);
    ngOnInit(): void;
    select(code: number): void;
}
