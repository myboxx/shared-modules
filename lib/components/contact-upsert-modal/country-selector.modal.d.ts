import { OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ICountryCodes } from '@boxx/contacts-core';
export declare class CountrySelectorModalComponent implements OnInit {
    popoverController: ModalController;
    countries: Array<ICountryCodes>;
    constructor(popoverController: ModalController);
    items: any[];
    ngOnInit(): void;
    select(country: ICountryCodes): void;
    search(searchTerm: string): void;
    close(): void;
}
