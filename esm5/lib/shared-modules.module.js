import { __decorate, __read, __spread } from "tslib";
import { CommonModule } from '@angular/common';
import { InjectionToken, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { CalendarSheetComponent } from './components/calendar-sheet/calendar-sheet.component';
import { ContactPickerModalComponent } from './components/contact-picker-modal/contact-picker.modal';
import { ContactUpsertModalComponent } from './components/contact-upsert-modal/contact-upsert.modal';
import { CountryCodeSelectorPopoverComponent } from './components/contact-upsert-modal/country-codes-selector.popover';
import { CountrySelectorModalComponent } from './components/contact-upsert-modal/country-selector.modal';
import { EventUpsertModalComponent } from './components/event-upsert-modal/event-upsert.modal';
import { NativeVersionInfoComponent } from './components/native-version-info/native-version-info.component';
import { PlacePickerModalComponent } from './components/place-picker-modal/place-picker.modal';
import { RemoteConfigComponent } from './components/remote-config/remote-config.component';
import { AppSettingsService } from './providers/global-params';
import { AppVersionService } from './services/NativeAppVersion.service';
export var AppSettingsObject = new InjectionToken('AppSettingsObject');
export function createAppSettingsService(settings) {
    return new AppSettingsService(settings);
}
var SharedModulesModule = /** @class */ (function () {
    function SharedModulesModule() {
    }
    SharedModulesModule_1 = SharedModulesModule;
    SharedModulesModule.forRoot = function (config) {
        return {
            ngModule: SharedModulesModule_1,
            providers: __spread([
                { provide: AppSettingsObject, useValue: config },
                {
                    provide: AppSettingsService,
                    useFactory: (createAppSettingsService),
                    deps: [AppSettingsObject]
                }
            ], config.providers, [
                AppVersionService,
                AppVersion,
                Market
            ])
        };
    };
    var SharedModulesModule_1;
    SharedModulesModule = SharedModulesModule_1 = __decorate([
        NgModule({
            declarations: [
                CalendarSheetComponent,
                ContactPickerModalComponent,
                ContactUpsertModalComponent,
                CountryCodeSelectorPopoverComponent,
                CountrySelectorModalComponent,
                EventUpsertModalComponent,
                PlacePickerModalComponent,
                NativeVersionInfoComponent,
                RemoteConfigComponent,
            ],
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                IonicModule,
                TranslateModule.forChild()
            ],
            exports: [
                CalendarSheetComponent,
                ContactPickerModalComponent,
                ContactUpsertModalComponent,
                CountryCodeSelectorPopoverComponent,
                CountrySelectorModalComponent,
                EventUpsertModalComponent,
                PlacePickerModalComponent,
                NativeVersionInfoComponent,
                RemoteConfigComponent,
            ]
        })
    ], SharedModulesModule);
    return SharedModulesModule;
}());
export { SharedModulesModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLW1vZHVsZXMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkLW1vZHVsZXMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGNBQWMsRUFBdUIsUUFBUSxFQUFZLE1BQU0sZUFBZSxDQUFDO0FBQ3hGLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUVsRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDM0QsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDOUYsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sd0RBQXdELENBQUM7QUFDckcsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sd0RBQXdELENBQUM7QUFDckcsT0FBTyxFQUFFLG1DQUFtQyxFQUFFLE1BQU0sa0VBQWtFLENBQUM7QUFDdkgsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sMERBQTBELENBQUM7QUFDekcsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDL0YsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sZ0VBQWdFLENBQUM7QUFDNUcsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDL0YsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDM0YsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDL0QsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFFeEUsTUFBTSxDQUFDLElBQU0saUJBQWlCLEdBQUcsSUFBSSxjQUFjLENBQUMsbUJBQW1CLENBQUMsQ0FBQztBQUV6RSxNQUFNLFVBQVUsd0JBQXdCLENBQUMsUUFBdUM7SUFDNUUsT0FBTyxJQUFJLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQzVDLENBQUM7QUFvREQ7SUFBQTtJQW1CQyxDQUFDOzRCQW5CVyxtQkFBbUI7SUFDckIsMkJBQU8sR0FBZCxVQUFlLE1BQXFDO1FBRWhELE9BQU87WUFDSCxRQUFRLEVBQUUscUJBQW1CO1lBQzdCLFNBQVM7Z0JBQ0wsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRTtnQkFDaEQ7b0JBQ0ksT0FBTyxFQUFFLGtCQUFrQjtvQkFDM0IsVUFBVSxFQUFFLENBQUMsd0JBQXdCLENBQUM7b0JBQ3RDLElBQUksRUFBRSxDQUFDLGlCQUFpQixDQUFDO2lCQUM1QjtlQUNFLE1BQU0sQ0FBQyxTQUFTO2dCQUNuQixpQkFBaUI7Z0JBQ2pCLFVBQVU7Z0JBQ1YsTUFBTTtjQUNUO1NBQ0osQ0FBQztJQUNOLENBQUM7O0lBbEJRLG1CQUFtQjtRQS9CL0IsUUFBUSxDQUFDO1lBQ1IsWUFBWSxFQUFFO2dCQUNaLHNCQUFzQjtnQkFDdEIsMkJBQTJCO2dCQUMzQiwyQkFBMkI7Z0JBQzNCLG1DQUFtQztnQkFDbkMsNkJBQTZCO2dCQUM3Qix5QkFBeUI7Z0JBQ3pCLHlCQUF5QjtnQkFDekIsMEJBQTBCO2dCQUMxQixxQkFBcUI7YUFDdEI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsWUFBWTtnQkFDWixXQUFXO2dCQUNYLG1CQUFtQjtnQkFDbkIsV0FBVztnQkFDWCxlQUFlLENBQUMsUUFBUSxFQUFFO2FBQzdCO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLHNCQUFzQjtnQkFDdEIsMkJBQTJCO2dCQUMzQiwyQkFBMkI7Z0JBQzNCLG1DQUFtQztnQkFDbkMsNkJBQTZCO2dCQUM3Qix5QkFBeUI7Z0JBQ3pCLHlCQUF5QjtnQkFDekIsMEJBQTBCO2dCQUMxQixxQkFBcUI7YUFDdEI7U0FDRixDQUFDO09BQ1csbUJBQW1CLENBbUI5QjtJQUFELDBCQUFDO0NBQUEsQUFuQkYsSUFtQkU7U0FuQlcsbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEluamVjdGlvblRva2VuLCBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSwgUHJvdmlkZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgSUV2ZW50UmVtaW5kZXIgfSBmcm9tICdAYm94eC9ldmVudHMtY29yZSc7XG5pbXBvcnQgeyBBcHBWZXJzaW9uIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9hcHAtdmVyc2lvbi9uZ3gnO1xuaW1wb3J0IHsgTWFya2V0IH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9tYXJrZXQvbmd4JztcbmltcG9ydCB7IElvbmljTW9kdWxlIH0gZnJvbSAnQGlvbmljL2FuZ3VsYXInO1xuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQgeyBDYWxlbmRhclNoZWV0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NhbGVuZGFyLXNoZWV0L2NhbGVuZGFyLXNoZWV0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb250YWN0UGlja2VyTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29udGFjdC1waWNrZXItbW9kYWwvY29udGFjdC1waWNrZXIubW9kYWwnO1xuaW1wb3J0IHsgQ29udGFjdFVwc2VydE1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NvbnRhY3QtdXBzZXJ0LW1vZGFsL2NvbnRhY3QtdXBzZXJ0Lm1vZGFsJztcbmltcG9ydCB7IENvdW50cnlDb2RlU2VsZWN0b3JQb3BvdmVyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NvbnRhY3QtdXBzZXJ0LW1vZGFsL2NvdW50cnktY29kZXMtc2VsZWN0b3IucG9wb3Zlcic7XG5pbXBvcnQgeyBDb3VudHJ5U2VsZWN0b3JNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb250YWN0LXVwc2VydC1tb2RhbC9jb3VudHJ5LXNlbGVjdG9yLm1vZGFsJztcbmltcG9ydCB7IEV2ZW50VXBzZXJ0TW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZXZlbnQtdXBzZXJ0LW1vZGFsL2V2ZW50LXVwc2VydC5tb2RhbCc7XG5pbXBvcnQgeyBOYXRpdmVWZXJzaW9uSW5mb0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9uYXRpdmUtdmVyc2lvbi1pbmZvL25hdGl2ZS12ZXJzaW9uLWluZm8uY29tcG9uZW50JztcbmltcG9ydCB7IFBsYWNlUGlja2VyTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvcGxhY2UtcGlja2VyLW1vZGFsL3BsYWNlLXBpY2tlci5tb2RhbCc7XG5pbXBvcnQgeyBSZW1vdGVDb25maWdDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvcmVtb3RlLWNvbmZpZy9yZW1vdGUtY29uZmlnLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBBcHBTZXR0aW5nc1NlcnZpY2UgfSBmcm9tICcuL3Byb3ZpZGVycy9nbG9iYWwtcGFyYW1zJztcbmltcG9ydCB7IEFwcFZlcnNpb25TZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9OYXRpdmVBcHBWZXJzaW9uLnNlcnZpY2UnO1xuXG5leHBvcnQgY29uc3QgQXBwU2V0dGluZ3NPYmplY3QgPSBuZXcgSW5qZWN0aW9uVG9rZW4oJ0FwcFNldHRpbmdzT2JqZWN0Jyk7XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVBcHBTZXR0aW5nc1NlcnZpY2Uoc2V0dGluZ3M6IFNoYXJlZE1vZHVsZXNPcHRpb25zSW50ZXJmYWNlKSB7XG4gICAgcmV0dXJuIG5ldyBBcHBTZXR0aW5nc1NlcnZpY2Uoc2V0dGluZ3MpO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIFNoYXJlZE1vZHVsZXNPcHRpb25zSW50ZXJmYWNlIHtcbiAgICBhcHBDb25zdGFudHM6IGFueTtcbiAgICBwcm92aWRlcnM6IFByb3ZpZGVyW107XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUxvY2FsTm90aWZpY2F0aW9uc1NlcnZpY2Uge1xuICAgIHNldFJlbWluZGVyKHJlbWluZGVyOiBJRXZlbnRSZW1pbmRlcik7XG5cbiAgICByZW1vdmVSZW1pbmRlcihpZDogbnVtYmVyKTtcblxuICAgIHVwZGF0ZVJlbWluZGVyKHJlbWluZGVyOiBJRXZlbnRSZW1pbmRlcik7XG5cbiAgICBjaGVja0lkU2NoZWR1bGVkKGlkOiBudW1iZXIpO1xuXG4gICAgaW5pdCgpO1xuXG4gICAgc3RvcCgpO1xufVxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBDYWxlbmRhclNoZWV0Q29tcG9uZW50LFxuICAgIENvbnRhY3RQaWNrZXJNb2RhbENvbXBvbmVudCxcbiAgICBDb250YWN0VXBzZXJ0TW9kYWxDb21wb25lbnQsXG4gICAgQ291bnRyeUNvZGVTZWxlY3RvclBvcG92ZXJDb21wb25lbnQsXG4gICAgQ291bnRyeVNlbGVjdG9yTW9kYWxDb21wb25lbnQsXG4gICAgRXZlbnRVcHNlcnRNb2RhbENvbXBvbmVudCxcbiAgICBQbGFjZVBpY2tlck1vZGFsQ29tcG9uZW50LFxuICAgIE5hdGl2ZVZlcnNpb25JbmZvQ29tcG9uZW50LFxuICAgIFJlbW90ZUNvbmZpZ0NvbXBvbmVudCxcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgICAgQ29tbW9uTW9kdWxlLFxuICAgICAgRm9ybXNNb2R1bGUsXG4gICAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgICAgSW9uaWNNb2R1bGUsXG4gICAgICBUcmFuc2xhdGVNb2R1bGUuZm9yQ2hpbGQoKVxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgQ2FsZW5kYXJTaGVldENvbXBvbmVudCxcbiAgICBDb250YWN0UGlja2VyTW9kYWxDb21wb25lbnQsXG4gICAgQ29udGFjdFVwc2VydE1vZGFsQ29tcG9uZW50LFxuICAgIENvdW50cnlDb2RlU2VsZWN0b3JQb3BvdmVyQ29tcG9uZW50LFxuICAgIENvdW50cnlTZWxlY3Rvck1vZGFsQ29tcG9uZW50LFxuICAgIEV2ZW50VXBzZXJ0TW9kYWxDb21wb25lbnQsXG4gICAgUGxhY2VQaWNrZXJNb2RhbENvbXBvbmVudCxcbiAgICBOYXRpdmVWZXJzaW9uSW5mb0NvbXBvbmVudCxcbiAgICBSZW1vdGVDb25maWdDb21wb25lbnQsXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgU2hhcmVkTW9kdWxlc01vZHVsZSB7XG4gICAgc3RhdGljIGZvclJvb3QoY29uZmlnOiBTaGFyZWRNb2R1bGVzT3B0aW9uc0ludGVyZmFjZSk6IE1vZHVsZVdpdGhQcm92aWRlcnM8U2hhcmVkTW9kdWxlc01vZHVsZT4ge1xuXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBuZ01vZHVsZTogU2hhcmVkTW9kdWxlc01vZHVsZSxcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAgICAgICAgIHsgcHJvdmlkZTogQXBwU2V0dGluZ3NPYmplY3QsIHVzZVZhbHVlOiBjb25maWcgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGU6IEFwcFNldHRpbmdzU2VydmljZSxcbiAgICAgICAgICAgICAgICAgICAgdXNlRmFjdG9yeTogKGNyZWF0ZUFwcFNldHRpbmdzU2VydmljZSksXG4gICAgICAgICAgICAgICAgICAgIGRlcHM6IFtBcHBTZXR0aW5nc09iamVjdF1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIC4uLmNvbmZpZy5wcm92aWRlcnMsXG4gICAgICAgICAgICAgICAgQXBwVmVyc2lvblNlcnZpY2UsXG4gICAgICAgICAgICAgICAgQXBwVmVyc2lvbixcbiAgICAgICAgICAgICAgICBNYXJrZXRcbiAgICAgICAgICAgIF1cbiAgICAgICAgfTtcbiAgICB9XG4gfVxuIl19