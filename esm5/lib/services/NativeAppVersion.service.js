import { __awaiter, __decorate, __generator } from "tslib";
import { Injectable } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@ionic-native/app-version/ngx/index";
import * as i2 from "@ionic/angular";
var AppVersionService = /** @class */ (function () {
    function AppVersionService(appVersion, platform) {
        var _this = this;
        this.appVersion = appVersion;
        this.platform = platform;
        this.whenReadySubject = new BehaviorSubject({
            appName: undefined,
            packageName: undefined,
            versionCode: undefined,
            versionNumber: 'PWA'
        });
        this.whenReady$ = this.whenReadySubject.asObservable();
        this.platform.ready().then(function () { return __awaiter(_this, void 0, void 0, function () {
            var _a, _b, _c, _d;
            return __generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        if (!(this.platform.is('cordova') || this.platform.is('capacitor'))) return [3 /*break*/, 5];
                        _a = this;
                        return [4 /*yield*/, this.appVersion.getAppName()];
                    case 1:
                        _a.appNameVal = _e.sent();
                        _b = this;
                        return [4 /*yield*/, this.appVersion.getPackageName()];
                    case 2:
                        _b.packageNameVal = _e.sent();
                        _c = this;
                        return [4 /*yield*/, this.appVersion.getVersionCode()];
                    case 3:
                        _c.versionCodeVal = _e.sent();
                        _d = this;
                        return [4 /*yield*/, this.appVersion.getVersionNumber()];
                    case 4:
                        _d.versionNumberVal = _e.sent();
                        _e.label = 5;
                    case 5:
                        this.whenReadySubject.next({
                            appName: this.appNameVal,
                            packageName: this.packageNameVal,
                            versionCode: this.versionCodeVal,
                            versionNumber: this.versionNumberVal
                        });
                        return [2 /*return*/];
                }
            });
        }); });
    }
    AppVersionService.prototype.appName = function () {
        return this.appNameVal;
    };
    AppVersionService.prototype.packageName = function () {
        return this.packageNameVal;
    };
    AppVersionService.prototype.versionCode = function () {
        return this.versionCodeVal;
    };
    AppVersionService.prototype.versionNumber = function () {
        return this.versionNumberVal;
    };
    AppVersionService.ctorParameters = function () { return [
        { type: AppVersion },
        { type: Platform }
    ]; };
    AppVersionService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AppVersionService_Factory() { return new AppVersionService(i0.ɵɵinject(i1.AppVersion), i0.ɵɵinject(i2.Platform)); }, token: AppVersionService, providedIn: "root" });
    AppVersionService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], AppVersionService);
    return AppVersionService;
}());
export { AppVersionService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTmF0aXZlQXBwVmVyc2lvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvTmF0aXZlQXBwVmVyc2lvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUMzRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDMUMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7OztBQUt2QztJQUVJLDJCQUNZLFVBQXNCLEVBQ3RCLFFBQWtCO1FBRjlCLGlCQW1CQztRQWxCVyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGFBQVEsR0FBUixRQUFRLENBQVU7UUF3QnRCLHFCQUFnQixHQUFHLElBQUksZUFBZSxDQUszQztZQUNDLE9BQU8sRUFBRSxTQUFTO1lBQ2xCLFdBQVcsRUFBRSxTQUFTO1lBQ3RCLFdBQVcsRUFBRSxTQUFTO1lBQ3RCLGFBQWEsRUFBRSxLQUFLO1NBQ3ZCLENBQUMsQ0FBQztRQUVJLGVBQVUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFsQ3JELElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDOzs7Ozs2QkFDbkIsQ0FBQSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQSxFQUE1RCx3QkFBNEQ7d0JBQzVELEtBQUEsSUFBSSxDQUFBO3dCQUFjLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLEVBQUE7O3dCQUFwRCxHQUFLLFVBQVUsR0FBRyxTQUFrQyxDQUFDO3dCQUNyRCxLQUFBLElBQUksQ0FBQTt3QkFBa0IscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsRUFBQTs7d0JBQTVELEdBQUssY0FBYyxHQUFHLFNBQXNDLENBQUM7d0JBQzdELEtBQUEsSUFBSSxDQUFBO3dCQUFrQixxQkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxFQUFBOzt3QkFBNUQsR0FBSyxjQUFjLEdBQUcsU0FBc0MsQ0FBQzt3QkFDN0QsS0FBQSxJQUFJLENBQUE7d0JBQW9CLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsRUFBQTs7d0JBQWhFLEdBQUssZ0JBQWdCLEdBQUcsU0FBd0MsQ0FBQzs7O3dCQUdyRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDOzRCQUN2QixPQUFPLEVBQUUsSUFBSSxDQUFDLFVBQVU7NEJBQ3hCLFdBQVcsRUFBRSxJQUFJLENBQUMsY0FBYzs0QkFDaEMsV0FBVyxFQUFFLElBQUksQ0FBQyxjQUFjOzRCQUNoQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGdCQUFnQjt5QkFDdkMsQ0FBQyxDQUFDOzs7O2FBQ04sQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQXFCRCxtQ0FBTyxHQUFQO1FBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQzNCLENBQUM7SUFFRCx1Q0FBVyxHQUFYO1FBQ0ksT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQy9CLENBQUM7SUFFRCx1Q0FBVyxHQUFYO1FBQ0ksT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQy9CLENBQUM7SUFFRCx5Q0FBYSxHQUFiO1FBQ0ksT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7SUFDakMsQ0FBQzs7Z0JBckR1QixVQUFVO2dCQUNaLFFBQVE7OztJQUpyQixpQkFBaUI7UUFIN0IsVUFBVSxDQUFDO1lBQ1IsVUFBVSxFQUFFLE1BQU07U0FDckIsQ0FBQztPQUNXLGlCQUFpQixDQXlEN0I7NEJBakVEO0NBaUVDLEFBekRELElBeURDO1NBekRZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEFwcFZlcnNpb24gfSBmcm9tICdAaW9uaWMtbmF0aXZlL2FwcC12ZXJzaW9uL25neCc7XG5pbXBvcnQgeyBQbGF0Zm9ybSB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEFwcFZlcnNpb25TZXJ2aWNlIHtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIGFwcFZlcnNpb246IEFwcFZlcnNpb24sXG4gICAgICAgIHByaXZhdGUgcGxhdGZvcm06IFBsYXRmb3JtXG4gICAgKSB7XG4gICAgICAgIHRoaXMucGxhdGZvcm0ucmVhZHkoKS50aGVuKGFzeW5jICgpID0+IHtcbiAgICAgICAgICAgIGlmICh0aGlzLnBsYXRmb3JtLmlzKCdjb3Jkb3ZhJykgfHwgdGhpcy5wbGF0Zm9ybS5pcygnY2FwYWNpdG9yJykpe1xuICAgICAgICAgICAgICAgIHRoaXMuYXBwTmFtZVZhbCA9IGF3YWl0IHRoaXMuYXBwVmVyc2lvbi5nZXRBcHBOYW1lKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5wYWNrYWdlTmFtZVZhbCA9IGF3YWl0IHRoaXMuYXBwVmVyc2lvbi5nZXRQYWNrYWdlTmFtZSgpO1xuICAgICAgICAgICAgICAgIHRoaXMudmVyc2lvbkNvZGVWYWwgPSBhd2FpdCB0aGlzLmFwcFZlcnNpb24uZ2V0VmVyc2lvbkNvZGUoKTtcbiAgICAgICAgICAgICAgICB0aGlzLnZlcnNpb25OdW1iZXJWYWwgPSBhd2FpdCB0aGlzLmFwcFZlcnNpb24uZ2V0VmVyc2lvbk51bWJlcigpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLndoZW5SZWFkeVN1YmplY3QubmV4dCh7XG4gICAgICAgICAgICAgICAgYXBwTmFtZTogdGhpcy5hcHBOYW1lVmFsLFxuICAgICAgICAgICAgICAgIHBhY2thZ2VOYW1lOiB0aGlzLnBhY2thZ2VOYW1lVmFsLFxuICAgICAgICAgICAgICAgIHZlcnNpb25Db2RlOiB0aGlzLnZlcnNpb25Db2RlVmFsLFxuICAgICAgICAgICAgICAgIHZlcnNpb25OdW1iZXI6IHRoaXMudmVyc2lvbk51bWJlclZhbFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXBwTmFtZVZhbDogc3RyaW5nO1xuICAgIHByaXZhdGUgcGFja2FnZU5hbWVWYWw6IHN0cmluZztcbiAgICBwcml2YXRlIHZlcnNpb25Db2RlVmFsOiBzdHJpbmcgfCBudW1iZXI7XG4gICAgcHJpdmF0ZSB2ZXJzaW9uTnVtYmVyVmFsOiBzdHJpbmc7XG5cbiAgICBwcml2YXRlIHdoZW5SZWFkeVN1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PHtcbiAgICAgICAgYXBwTmFtZTogc3RyaW5nLFxuICAgICAgICBwYWNrYWdlTmFtZTogc3RyaW5nLFxuICAgICAgICB2ZXJzaW9uQ29kZTogc3RyaW5nIHwgbnVtYmVyLFxuICAgICAgICB2ZXJzaW9uTnVtYmVyOiBzdHJpbmdcbiAgICB9Pih7XG4gICAgICAgIGFwcE5hbWU6IHVuZGVmaW5lZCxcbiAgICAgICAgcGFja2FnZU5hbWU6IHVuZGVmaW5lZCxcbiAgICAgICAgdmVyc2lvbkNvZGU6IHVuZGVmaW5lZCxcbiAgICAgICAgdmVyc2lvbk51bWJlcjogJ1BXQSdcbiAgICB9KTtcblxuICAgIHB1YmxpYyB3aGVuUmVhZHkkID0gdGhpcy53aGVuUmVhZHlTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuXG4gICAgYXBwTmFtZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBwTmFtZVZhbDtcbiAgICB9XG5cbiAgICBwYWNrYWdlTmFtZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucGFja2FnZU5hbWVWYWw7XG4gICAgfVxuXG4gICAgdmVyc2lvbkNvZGUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnZlcnNpb25Db2RlVmFsO1xuICAgIH1cblxuICAgIHZlcnNpb25OdW1iZXIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnZlcnNpb25OdW1iZXJWYWw7XG4gICAgfVxufVxuIl19