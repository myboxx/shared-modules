import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
var CalendarSheetComponent = /** @class */ (function () {
    function CalendarSheetComponent() {
    }
    CalendarSheetComponent.prototype.ngOnInit = function () {
        this.dayName = this.dayName.toUpperCase().replace('.', '');
        this.monthName = this.monthName.toUpperCase().replace('.', '');
    };
    __decorate([
        Input()
    ], CalendarSheetComponent.prototype, "dayName", void 0);
    __decorate([
        Input()
    ], CalendarSheetComponent.prototype, "monthDay", void 0);
    __decorate([
        Input()
    ], CalendarSheetComponent.prototype, "monthName", void 0);
    __decorate([
        Input()
    ], CalendarSheetComponent.prototype, "headerIonColor", void 0);
    CalendarSheetComponent = __decorate([
        Component({
            selector: 'boxx-calendar-sheet',
            template: "<ion-card>\n    <ion-card-header color=\"{{headerIonColor || 'primary'}}\">\n        <ion-card-subtitle>{{dayName}}</ion-card-subtitle>\n    </ion-card-header>\n\n    <ion-card-content>\n        <p>\n            <strong>{{monthDay}}</strong>\n        </p>\n        <p>{{monthName}}</p>\n    </ion-card-content>\n</ion-card>",
            styles: [":host{margin:0}:host ion-card{line-height:1.5;padding-right:0;border-radius:8px;margin-left:0;margin-right:17px}:host ion-card.ios{margin:8px 17px 8px 0}:host ion-card-header{padding:0;text-align:center}:host ion-card-content{min-width:40px;text-align:center;font-size:x-small;line-height:1;padding:2px 8px 4px}:host ion-card-content strong{font-size:16px}:host ion-card-content p{padding:0;font-size:12px;font-weight:600}:host .card-content-md p{line-height:1.2;margin-bottom:0}:host ion-card-subtitle{margin-bottom:0;padding:2px 0;font-size:small}"]
        })
    ], CalendarSheetComponent);
    return CalendarSheetComponent;
}());
export { CalendarSheetComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FsZW5kYXItc2hlZXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jYWxlbmRhci1zaGVldC9jYWxlbmRhci1zaGVldC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT3pEO0lBT0k7SUFBZ0IsQ0FBQztJQUVqQix5Q0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQVhRO1FBQVIsS0FBSyxFQUFFOzJEQUFpQjtJQUNoQjtRQUFSLEtBQUssRUFBRTs0REFBa0I7SUFDakI7UUFBUixLQUFLLEVBQUU7NkRBQW1CO0lBRWxCO1FBQVIsS0FBSyxFQUFFO2tFQUF3QjtJQUx2QixzQkFBc0I7UUFMbEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQiwrVUFBOEM7O1NBRWpELENBQUM7T0FDVyxzQkFBc0IsQ0FjbEM7SUFBRCw2QkFBQztDQUFBLEFBZEQsSUFjQztTQWRZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtY2FsZW5kYXItc2hlZXQnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9jYWxlbmRhci1zaGVldC5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vY2FsZW5kYXItc2hlZXQuY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgQ2FsZW5kYXJTaGVldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgZGF5TmFtZTogc3RyaW5nO1xuICAgIEBJbnB1dCgpIG1vbnRoRGF5OiBzdHJpbmc7XG4gICAgQElucHV0KCkgbW9udGhOYW1lOiBzdHJpbmc7XG5cbiAgICBASW5wdXQoKSBoZWFkZXJJb25Db2xvcjogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IoKSB7IH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLmRheU5hbWUgPSB0aGlzLmRheU5hbWUudG9VcHBlckNhc2UoKS5yZXBsYWNlKCcuJywgJycpO1xuICAgICAgICB0aGlzLm1vbnRoTmFtZSA9IHRoaXMubW9udGhOYW1lLnRvVXBwZXJDYXNlKCkucmVwbGFjZSgnLicsICcnKTtcbiAgICB9XG5cbn1cbiJdfQ==