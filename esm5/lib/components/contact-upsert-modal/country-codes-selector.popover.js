import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';
var CountryCodeSelectorPopoverComponent = /** @class */ (function () {
    function CountryCodeSelectorPopoverComponent(popoverController) {
        this.popoverController = popoverController;
        this.codes = [];
    }
    CountryCodeSelectorPopoverComponent.prototype.ngOnInit = function () {
    };
    CountryCodeSelectorPopoverComponent.prototype.select = function (code) {
        this.popoverController.dismiss(code);
    };
    CountryCodeSelectorPopoverComponent.ctorParameters = function () { return [
        { type: PopoverController }
    ]; };
    __decorate([
        Input()
    ], CountryCodeSelectorPopoverComponent.prototype, "codes", void 0);
    CountryCodeSelectorPopoverComponent = __decorate([
        Component({
            selector: 'boxx-country-code-select-popover',
            template: "\n    <ion-content>\n        <ion-list-header translate>\n            GENERAL.ACTION.selectCountryCode\n        </ion-list-header>\n        <ion-item *ngFor=\"let code of codes\" (click)=\"select(code)\">\n            <ion-label>\n                +{{code}}\n            </ion-label>\n        </ion-item>\n    </ion-content>\n    "
        })
    ], CountryCodeSelectorPopoverComponent);
    return CountryCodeSelectorPopoverComponent;
}());
export { CountryCodeSelectorPopoverComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRyeS1jb2Rlcy1zZWxlY3Rvci5wb3BvdmVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb250YWN0LXVwc2VydC1tb2RhbC9jb3VudHJ5LWNvZGVzLXNlbGVjdG9yLnBvcG92ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBa0JuRDtJQUdJLDZDQUNXLGlCQUFvQztRQUFwQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBSHRDLFVBQUssR0FBa0IsRUFBRSxDQUFDO0lBSS9CLENBQUM7SUFFTCxzREFBUSxHQUFSO0lBRUEsQ0FBQztJQUVELG9EQUFNLEdBQU4sVUFBTyxJQUFZO1FBQ2YsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QyxDQUFDOztnQkFUNkIsaUJBQWlCOztJQUh0QztRQUFSLEtBQUssRUFBRTtzRUFBMkI7SUFEMUIsbUNBQW1DO1FBZi9DLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxrQ0FBa0M7WUFDNUMsUUFBUSxFQUFFLDJVQVdUO1NBQ0osQ0FBQztPQUNXLG1DQUFtQyxDQWMvQztJQUFELDBDQUFDO0NBQUEsQUFkRCxJQWNDO1NBZFksbUNBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQb3BvdmVyQ29udHJvbGxlciB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcblxuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtY291bnRyeS1jb2RlLXNlbGVjdC1wb3BvdmVyJyxcbiAgICB0ZW1wbGF0ZTogYFxuICAgIDxpb24tY29udGVudD5cbiAgICAgICAgPGlvbi1saXN0LWhlYWRlciB0cmFuc2xhdGU+XG4gICAgICAgICAgICBHRU5FUkFMLkFDVElPTi5zZWxlY3RDb3VudHJ5Q29kZVxuICAgICAgICA8L2lvbi1saXN0LWhlYWRlcj5cbiAgICAgICAgPGlvbi1pdGVtICpuZ0Zvcj1cImxldCBjb2RlIG9mIGNvZGVzXCIgKGNsaWNrKT1cInNlbGVjdChjb2RlKVwiPlxuICAgICAgICAgICAgPGlvbi1sYWJlbD5cbiAgICAgICAgICAgICAgICAre3tjb2RlfX1cbiAgICAgICAgICAgIDwvaW9uLWxhYmVsPlxuICAgICAgICA8L2lvbi1pdGVtPlxuICAgIDwvaW9uLWNvbnRlbnQ+XG4gICAgYFxufSlcbmV4cG9ydCBjbGFzcyBDb3VudHJ5Q29kZVNlbGVjdG9yUG9wb3ZlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgY29kZXM6IEFycmF5PG51bWJlcj4gPSBbXTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwdWJsaWMgcG9wb3ZlckNvbnRyb2xsZXI6IFBvcG92ZXJDb250cm9sbGVyXG4gICAgKSB7IH1cblxuICAgIG5nT25Jbml0KCkge1xuXG4gICAgfVxuXG4gICAgc2VsZWN0KGNvZGU6IG51bWJlcikge1xuICAgICAgICB0aGlzLnBvcG92ZXJDb250cm9sbGVyLmRpc21pc3MoY29kZSk7XG4gICAgfVxufVxuIl19