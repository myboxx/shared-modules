import { __awaiter, __decorate, __generator } from "tslib";
import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ContactModel, ContactStore, COUNTRY_CALLING_CODES, ICountryCodes } from '@boxx/contacts-core';
import { LoadingController, ModalController, PopoverController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppSettingsService } from '../../providers/global-params';
import { CountryCodeSelectorPopoverComponent } from './country-codes-selector.popover';
import { CountrySelectorModalComponent } from './country-selector.modal';
var ContactUpsertModalComponent = /** @class */ (function () {
    function ContactUpsertModalComponent(modalCtrl, store, translate, popoverController, mdalCtrl, loadingCtrl, moduleService) {
        var _this = this;
        this.modalCtrl = modalCtrl;
        this.store = store;
        this.translate = translate;
        this.popoverController = popoverController;
        this.mdalCtrl = mdalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.moduleService = moduleService;
        this.destroyed$ = new Subject();
        this.types = [
            { key: 'CLIENT', value: 'Client' },
            { key: 'PROSPECT', value: 'Prospect' },
            { key: 'NOT_SPECIFIED', value: 'Not qualified' }
        ];
        this.validationMessages = {
            name: this.moduleService.getAppConstants().VALIDATION_MESSAGES.name,
            last_name: this.moduleService.getAppConstants().VALIDATION_MESSAGES.last_name,
            phone: this.moduleService.getAppConstants().VALIDATION_MESSAGES.phone,
            email: this.moduleService.getAppConstants().VALIDATION_MESSAGES.email,
            address: this.moduleService.getAppConstants().VALIDATION_MESSAGES.address
        };
        this.translate.get(['GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (t) {
            _this.types[0].value = t.GENERAL.client;
            _this.types[1].value = t.GENERAL.prospect;
            _this.types[2].value = t.GENERAL.not_specified;
        });
    }
    ContactUpsertModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.contactForm = new FormGroup({
            name: new FormControl(this.contact.name, Validators.required),
            last_name: new FormControl(this.contact.lastName, Validators.required),
            phone: new FormControl(this.contact.phone, [Validators.minLength(10), Validators.maxLength(10), Validators.required, Validators.pattern('[0-9]*')]),
            email: new FormControl(this.contact.email, Validators.pattern(this.moduleService.getAppConstants().EMAILPATTERN)),
            street_address: new FormControl(this.contact.streetAddress),
            type: new FormControl(this.contact.type),
            city: new FormControl(this.contact.city),
            id: new FormControl(this.contact.id),
            country_code: new FormControl(COUNTRY_CALLING_CODES[1]),
            phone_code: new FormControl(this.contact.phoneCode || COUNTRY_CALLING_CODES[1].callingCodes[0]),
        });
        this.store.CountryCodes$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (countries) { return __awaiter(_this, void 0, void 0, function () {
            var countryCode;
            var _this = this;
            return __generator(this, function (_a) {
                if (!countries.length) {
                    return [2 /*return*/, this.store.fetchCountryCodes()];
                }
                countryCode = countries.filter(function (c) { return c.alpha3Code === _this.contact.countryCode; })[0] || COUNTRY_CALLING_CODES[1];
                this.contactForm.patchValue({ country_code: countryCode });
                return [2 /*return*/];
            });
        }); });
    };
    ContactUpsertModalComponent.prototype.ngOnDestroy = function () {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    };
    ContactUpsertModalComponent.prototype.close = function () {
        this.modalCtrl.dismiss( /*{someData: ...} */);
    };
    ContactUpsertModalComponent.prototype.update = function () {
        // this._appendCountryCode();
        var form = this.contactForm.value;
        delete form.phone_code;
        form.country_code = form.country_code.alpha3Code;
        this.store.updateContact(form);
        this.modalCtrl.dismiss( /*{someData: ...} */);
    };
    ContactUpsertModalComponent.prototype.create = function () {
        // this._appendCountryCode();
        var form = this.contactForm.value;
        delete form.phone_code;
        form.country_code = form.country_code.alpha3Code;
        this.store.createContact(form);
        this.modalCtrl.dismiss( /*{someData: ...} */);
    };
    ContactUpsertModalComponent.prototype.presentPopover = function ( /*ev: any*/) {
        return __awaiter(this, void 0, void 0, function () {
            var loader;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create()];
                    case 1:
                        loader = _a.sent();
                        loader.present();
                        this.store.CountryCodes$
                            .pipe(takeUntil(this.destroyed$))
                            .subscribe(function (countries) { return __awaiter(_this, void 0, void 0, function () {
                            var popover, response, selCountry, data;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!countries.length) {
                                            return [2 /*return*/, this.store.fetchCountryCodes()];
                                        }
                                        return [4 /*yield*/, this.mdalCtrl.create({
                                                component: CountrySelectorModalComponent,
                                                componentProps: { countries: countries },
                                            })];
                                    case 1:
                                        popover = _a.sent();
                                        return [4 /*yield*/, popover.present()];
                                    case 2:
                                        _a.sent();
                                        loader.dismiss();
                                        return [4 /*yield*/, popover.onDidDismiss()];
                                    case 3:
                                        response = _a.sent();
                                        selCountry = response.data;
                                        if (!selCountry) return [3 /*break*/, 8];
                                        if (!(selCountry.callingCodes.length > 1)) return [3 /*break*/, 7];
                                        return [4 /*yield*/, this.popoverController.create({
                                                component: CountryCodeSelectorPopoverComponent,
                                                componentProps: { codes: selCountry.callingCodes },
                                                // event: ev,
                                                translucent: true,
                                                backdropDismiss: false
                                            })];
                                    case 4:
                                        popover = _a.sent();
                                        return [4 /*yield*/, popover.present()];
                                    case 5:
                                        _a.sent();
                                        return [4 /*yield*/, popover.onDidDismiss()];
                                    case 6:
                                        data = (_a.sent()).data;
                                        this.contactForm.patchValue({ country_code: selCountry, phone_code: "+" + data });
                                        return [3 /*break*/, 8];
                                    case 7:
                                        this.contactForm.patchValue({ country_code: selCountry, phone_code: "+" + selCountry.callingCodes[0] });
                                        _a.label = 8;
                                    case 8: return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    ContactUpsertModalComponent.prototype._appendCountryCode = function () {
        var country = this.contactForm.get('country_code').value;
        switch (country.alpha3Code) {
            case 'MEX':
                this.contactForm.patchValue({
                    phone: ("+" + country.callingCodes[0] + " 1 " + this.contactForm.get('phone').value).replace(/ /g, '')
                });
                break;
            case 'ARG':
                this.contactForm.patchValue({
                    phone: ("+" + country.callingCodes[0] + " 9 " + this.contactForm.get('phone').value).replace(/ /g, '')
                });
        }
        this.contactForm.get('country_code').disable();
    };
    ContactUpsertModalComponent.ctorParameters = function () { return [
        { type: ModalController },
        { type: ContactStore },
        { type: TranslateService },
        { type: PopoverController },
        { type: ModalController },
        { type: LoadingController },
        { type: AppSettingsService }
    ]; };
    __decorate([
        Input()
    ], ContactUpsertModalComponent.prototype, "contact", void 0);
    __decorate([
        Input()
    ], ContactUpsertModalComponent.prototype, "mode", void 0);
    ContactUpsertModalComponent = __decorate([
        Component({
            selector: 'boxx-contact-upsert-modal',
            template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                GENERAL.contacts\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n    <ion-list class=\"ion-no-margin ion-no-padding\" [formGroup]=\"contactForm\">\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.name\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-input formControlName=\"name\" type=\"text\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.name\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('name').hasError(validation.type) && contactForm.get('name').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.lastName\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-input formControlName=\"last_name\" type=\"text\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.last_name\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('last_name').hasError(validation.type) && contactForm.get('last_name').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.type\n            </ion-label>\n            <ion-select placeholder=\"{{'GENERAL.ACTION.select' | translate}}\" formControlName=\"type\"\n                [okText]=\"'GENERAL.ACTION.ok' | translate\" [cancelText]=\"'GENERAL.ACTION.cancel' | translate\"\n                interface=\"popover\">\n                <ion-select-option *ngFor=\"let cType of types\" [value]=\"cType.key\">{{cType.value}}</ion-select-option>\n            </ion-select>\n        </ion-item>\n\n        <ion-item (click)=\"presentPopover()\" class=\"boxx-input-item\">\n            <ion-label>\n                <p>\n                    <ion-text color=\"primary\" translate>FORM.LABEL.country</ion-text>\n                </p>\n                <img alt=\"country flag\" src=\"{{contactForm.get('country_code').value.flag}}\" style=\"width: 32px;\">\n                {{contactForm.get('country_code').value.translations.es || contactForm.get('country_code').value.name}}\n            </ion-label>\n        </ion-item>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.phone\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-row style=\"width: 100%;\">\n                <ion-col size=\"2\" style=\"display: grid; align-content: center;\">\n                    <small>{{contactForm.get('phone_code').value}}</small>\n                </ion-col>\n                <ion-col size=\"10\">\n                    <ion-input formControlName=\"phone\" type=\"tel\" id=\"phone-input\" minLength=\"10\" maxLength=\"10\"\n                        placeholder=\"123 4567 890\">\n                    </ion-input>\n                </ion-col>\n            </ion-row>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.phone\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('phone').hasError(validation.type) && contactForm.get('phone').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.email\n            </ion-label>\n            <ion-input formControlName=\"email\" type=\"email\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.email\">\n            <div class=\"error-feedback\"\n                *ngIf=\"contactForm.get('email').hasError(validation.type) && contactForm.get('email').invalid\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.address\n            </ion-label>\n            <ion-input formControlName=\"street_address\" type=\"text\"></ion-input>\n        </ion-item>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.city\n            </ion-label>\n            <ion-input formControlName=\"city\" type=\"text\"></ion-input>\n        </ion-item>\n    </ion-list>\n\n    <div class=\"ion-margin-vertical\">\n        <ion-button color=\"primary\" expand=\"block\" (click)=\"update()\" *ngIf=\"mode == 'UPDATE'\"\n            [disabled]=\"contactForm.invalid\" translate>\n            GENERAL.ACTION.update\n        </ion-button>\n        <ion-button color=\"primary\" expand=\"block\" (click)=\"create()\" *ngIf=\"mode == 'CREATE'\"\n            [disabled]=\"contactForm.invalid\" translate>\n            GENERAL.ACTION.create\n        </ion-button>\n    </div>\n\n</ion-content>",
            styles: [":host ion-toolbar.md ion-title{padding:0}:host #phone-input{background-color:#efefef;padding-left:8px!important}"]
        })
    ], ContactUpsertModalComponent);
    return ContactUpsertModalComponent;
}());
export { ContactUpsertModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdC11cHNlcnQubW9kYWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtbW9kdWxlcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvbnRhY3QtdXBzZXJ0LW1vZGFsL2NvbnRhY3QtdXBzZXJ0Lm1vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBcUIsTUFBTSxlQUFlLENBQUM7QUFDcEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEUsT0FBTyxFQUFFLFlBQVksRUFBRSxZQUFZLEVBQUUscUJBQXFCLEVBQUUsYUFBYSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkcsT0FBTyxFQUFFLGlCQUFpQixFQUFFLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ25FLE9BQU8sRUFBRSxtQ0FBbUMsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBT3pFO0lBSUkscUNBQ1ksU0FBMEIsRUFDMUIsS0FBbUIsRUFDbkIsU0FBMkIsRUFDM0IsaUJBQW9DLEVBQ3JDLFFBQXlCLEVBQ3hCLFdBQThCLEVBQzlCLGFBQWlDO1FBUDdDLGlCQWdCQztRQWZXLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBQzFCLFVBQUssR0FBTCxLQUFLLENBQWM7UUFDbkIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0Isc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNyQyxhQUFRLEdBQVIsUUFBUSxDQUFpQjtRQUN4QixnQkFBVyxHQUFYLFdBQVcsQ0FBbUI7UUFDOUIsa0JBQWEsR0FBYixhQUFhLENBQW9CO1FBVzdDLGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBSXBDLFVBQUssR0FBRztZQUNKLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFO1lBQ2xDLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFO1lBQ3RDLEVBQUUsR0FBRyxFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsZUFBZSxFQUFFO1NBQ25ELENBQUM7UUFFRix1QkFBa0IsR0FBRztZQUNqQixJQUFJLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJO1lBQ25FLFNBQVMsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDLG1CQUFtQixDQUFDLFNBQVM7WUFDN0UsS0FBSyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFLENBQUMsbUJBQW1CLENBQUMsS0FBSztZQUNyRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO1lBQ3JFLE9BQU8sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDLG1CQUFtQixDQUFDLE9BQU87U0FDNUUsQ0FBQztRQXpCRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxVQUFDLENBQU07WUFDZCxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQztZQUN2QyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUN6QyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQztRQUNsRCxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFvQkQsOENBQVEsR0FBUjtRQUFBLGlCQTRCQztRQTFCRyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksU0FBUyxDQUFDO1lBQzdCLElBQUksRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQzdELFNBQVMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ3RFLEtBQUssRUFBRSxJQUFJLFdBQVcsQ0FDbEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQ2xCLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUMxRztZQUNELEtBQUssRUFBRSxJQUFJLFdBQVcsQ0FDbEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDLFlBQVksQ0FBQyxDQUM1RjtZQUNELGNBQWMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQztZQUMzRCxJQUFJLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7WUFDeEMsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ3hDLEVBQUUsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztZQUNwQyxZQUFZLEVBQUUsSUFBSSxXQUFXLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkQsVUFBVSxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxJQUFJLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNsRyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWE7YUFDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUyxDQUFDLFVBQU8sU0FBUzs7OztnQkFDdkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7b0JBQUUsc0JBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxFQUFDO2lCQUFFO2dCQUUzRCxXQUFXLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxVQUFVLEtBQUssS0FBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQXpDLENBQXlDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEgsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQzs7O2FBQzlELENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxpREFBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQsMkNBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELDRDQUFNLEdBQU47UUFDSSw2QkFBNkI7UUFFN0IsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7UUFFcEMsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBRXZCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7UUFFakQsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUMsb0JBQW9CLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQsNENBQU0sR0FBTjtRQUNJLDZCQUE2QjtRQUM3QixJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztRQUVwQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7UUFFdkIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQztRQUVqRCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBQyxvQkFBb0IsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFSyxvREFBYyxHQUFwQixXQUFxQixXQUFXOzs7Ozs7NEJBQ2IscUJBQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsRUFBQTs7d0JBQXhDLE1BQU0sR0FBRyxTQUErQjt3QkFFOUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUVqQixJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWE7NkJBQ25CLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzZCQUNoQyxTQUFTLENBQUMsVUFBTyxTQUFTOzs7Ozt3Q0FDdkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7NENBQUUsc0JBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxFQUFDO3lDQUFFO3dDQUU5QyxxQkFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztnREFDMUMsU0FBUyxFQUFFLDZCQUE2QjtnREFDeEMsY0FBYyxFQUFFLEVBQUUsU0FBUyxXQUFBLEVBQUU7NkNBRWhDLENBQUMsRUFBQTs7d0NBSkUsT0FBTyxHQUFRLFNBSWpCO3dDQUVGLHFCQUFNLE9BQU8sQ0FBQyxPQUFPLEVBQUUsRUFBQTs7d0NBQXZCLFNBQXVCLENBQUM7d0NBRXhCLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3Q0FFQSxxQkFBTSxPQUFPLENBQUMsWUFBWSxFQUFFLEVBQUE7O3dDQUF2QyxRQUFRLEdBQUcsU0FBNEI7d0NBQ3ZDLFVBQVUsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDOzZDQUU3QixVQUFVLEVBQVYsd0JBQVU7NkNBQ04sQ0FBQSxVQUFVLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUEsRUFBbEMsd0JBQWtDO3dDQUN4QixxQkFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDO2dEQUMxQyxTQUFTLEVBQUUsbUNBQW1DO2dEQUM5QyxjQUFjLEVBQUUsRUFBRSxLQUFLLEVBQUUsVUFBVSxDQUFDLFlBQVksRUFBRTtnREFDbEQsYUFBYTtnREFDYixXQUFXLEVBQUUsSUFBSTtnREFDakIsZUFBZSxFQUFFLEtBQUs7NkNBQ3pCLENBQUMsRUFBQTs7d0NBTkYsT0FBTyxHQUFHLFNBTVIsQ0FBQzt3Q0FDSCxxQkFBTSxPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUE7O3dDQUF2QixTQUF1QixDQUFDO3dDQUNQLHFCQUFNLE9BQU8sQ0FBQyxZQUFZLEVBQUUsRUFBQTs7d0NBQXJDLElBQUksR0FBSyxDQUFBLFNBQTRCLENBQUEsS0FBakM7d0NBRVosSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxNQUFJLElBQU0sRUFBRSxDQUFDLENBQUM7Ozt3Q0FHbEYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxNQUFJLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFHLEVBQUUsQ0FBQyxDQUFDOzs7Ozs2QkFHbkgsQ0FBQyxDQUFDOzs7OztLQUNWO0lBRU8sd0RBQWtCLEdBQTFCO1FBQ0ksSUFBTSxPQUFPLEdBQWtCLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUUxRSxRQUFRLE9BQU8sQ0FBQyxVQUFVLEVBQUU7WUFDeEIsS0FBSyxLQUFLO2dCQUNOLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDO29CQUN4QixLQUFLLEVBQUUsQ0FBQSxNQUFJLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLFdBQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBTyxDQUFBLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUM7aUJBQ2xHLENBQUMsQ0FBQztnQkFDSCxNQUFNO1lBQ1YsS0FBSyxLQUFLO2dCQUNOLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDO29CQUN4QixLQUFLLEVBQUUsQ0FBQSxNQUFJLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLFdBQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBTyxDQUFBLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUM7aUJBQ2xHLENBQUMsQ0FBQztTQUNWO1FBRUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkQsQ0FBQzs7Z0JBL0pzQixlQUFlO2dCQUNuQixZQUFZO2dCQUNSLGdCQUFnQjtnQkFDUixpQkFBaUI7Z0JBQzNCLGVBQWU7Z0JBQ1gsaUJBQWlCO2dCQUNmLGtCQUFrQjs7SUFWcEM7UUFBUixLQUFLLEVBQUU7Z0VBQXVCO0lBQ3RCO1FBQVIsS0FBSyxFQUFFOzZEQUEyQjtJQUYxQiwyQkFBMkI7UUFMdkMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLDJCQUEyQjtZQUNyQyxpaE1BQTBDOztTQUU3QyxDQUFDO09BQ1csMkJBQTJCLENBcUt2QztJQUFELGtDQUFDO0NBQUEsQUFyS0QsSUFxS0M7U0FyS1ksMkJBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25EZXN0cm95LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Db250cm9sLCBGb3JtR3JvdXAsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBDb250YWN0TW9kZWwsIENvbnRhY3RTdG9yZSwgQ09VTlRSWV9DQUxMSU5HX0NPREVTLCBJQ291bnRyeUNvZGVzIH0gZnJvbSAnQGJveHgvY29udGFjdHMtY29yZSc7XG5pbXBvcnQgeyBMb2FkaW5nQ29udHJvbGxlciwgTW9kYWxDb250cm9sbGVyLCBQb3BvdmVyQ29udHJvbGxlciB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEFwcFNldHRpbmdzU2VydmljZSB9IGZyb20gJy4uLy4uL3Byb3ZpZGVycy9nbG9iYWwtcGFyYW1zJztcbmltcG9ydCB7IENvdW50cnlDb2RlU2VsZWN0b3JQb3BvdmVyQ29tcG9uZW50IH0gZnJvbSAnLi9jb3VudHJ5LWNvZGVzLXNlbGVjdG9yLnBvcG92ZXInO1xuaW1wb3J0IHsgQ291bnRyeVNlbGVjdG9yTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2NvdW50cnktc2VsZWN0b3IubW9kYWwnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtY29udGFjdC11cHNlcnQtbW9kYWwnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9jb250YWN0LXVwc2VydC5tb2RhbC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9jb250YWN0LXVwc2VydC5tb2RhbC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIENvbnRhY3RVcHNlcnRNb2RhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgICBASW5wdXQoKSBjb250YWN0OiBDb250YWN0TW9kZWw7XG4gICAgQElucHV0KCkgbW9kZTogJ0NSRUFURScgfCAnVVBEQVRFJztcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIG1vZGFsQ3RybDogTW9kYWxDb250cm9sbGVyLFxuICAgICAgICBwcml2YXRlIHN0b3JlOiBDb250YWN0U3RvcmUsXG4gICAgICAgIHByaXZhdGUgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIHBvcG92ZXJDb250cm9sbGVyOiBQb3BvdmVyQ29udHJvbGxlcixcbiAgICAgICAgcHVibGljIG1kYWxDdHJsOiBNb2RhbENvbnRyb2xsZXIsXG4gICAgICAgIHByaXZhdGUgbG9hZGluZ0N0cmw6IExvYWRpbmdDb250cm9sbGVyLFxuICAgICAgICBwcml2YXRlIG1vZHVsZVNlcnZpY2U6IEFwcFNldHRpbmdzU2VydmljZVxuICAgICkge1xuICAgICAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoWydHRU5FUkFMJ10pXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKHQ6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMudHlwZXNbMF0udmFsdWUgPSB0LkdFTkVSQUwuY2xpZW50O1xuICAgICAgICAgICAgICAgIHRoaXMudHlwZXNbMV0udmFsdWUgPSB0LkdFTkVSQUwucHJvc3BlY3Q7XG4gICAgICAgICAgICAgICAgdGhpcy50eXBlc1syXS52YWx1ZSA9IHQuR0VORVJBTC5ub3Rfc3BlY2lmaWVkO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZGVzdHJveWVkJCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XG5cbiAgICBjb250YWN0Rm9ybTogRm9ybUdyb3VwO1xuXG4gICAgdHlwZXMgPSBbXG4gICAgICAgIHsga2V5OiAnQ0xJRU5UJywgdmFsdWU6ICdDbGllbnQnIH0sXG4gICAgICAgIHsga2V5OiAnUFJPU1BFQ1QnLCB2YWx1ZTogJ1Byb3NwZWN0JyB9LFxuICAgICAgICB7IGtleTogJ05PVF9TUEVDSUZJRUQnLCB2YWx1ZTogJ05vdCBxdWFsaWZpZWQnIH1cbiAgICBdO1xuXG4gICAgdmFsaWRhdGlvbk1lc3NhZ2VzID0ge1xuICAgICAgICBuYW1lOiB0aGlzLm1vZHVsZVNlcnZpY2UuZ2V0QXBwQ29uc3RhbnRzKCkuVkFMSURBVElPTl9NRVNTQUdFUy5uYW1lLFxuICAgICAgICBsYXN0X25hbWU6IHRoaXMubW9kdWxlU2VydmljZS5nZXRBcHBDb25zdGFudHMoKS5WQUxJREFUSU9OX01FU1NBR0VTLmxhc3RfbmFtZSxcbiAgICAgICAgcGhvbmU6IHRoaXMubW9kdWxlU2VydmljZS5nZXRBcHBDb25zdGFudHMoKS5WQUxJREFUSU9OX01FU1NBR0VTLnBob25lLFxuICAgICAgICBlbWFpbDogdGhpcy5tb2R1bGVTZXJ2aWNlLmdldEFwcENvbnN0YW50cygpLlZBTElEQVRJT05fTUVTU0FHRVMuZW1haWwsXG4gICAgICAgIGFkZHJlc3M6IHRoaXMubW9kdWxlU2VydmljZS5nZXRBcHBDb25zdGFudHMoKS5WQUxJREFUSU9OX01FU1NBR0VTLmFkZHJlc3NcbiAgICB9O1xuXG4gICAgbmdPbkluaXQoKSB7XG5cbiAgICAgICAgdGhpcy5jb250YWN0Rm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xuICAgICAgICAgICAgbmFtZTogbmV3IEZvcm1Db250cm9sKHRoaXMuY29udGFjdC5uYW1lLCBWYWxpZGF0b3JzLnJlcXVpcmVkKSxcbiAgICAgICAgICAgIGxhc3RfbmFtZTogbmV3IEZvcm1Db250cm9sKHRoaXMuY29udGFjdC5sYXN0TmFtZSwgVmFsaWRhdG9ycy5yZXF1aXJlZCksXG4gICAgICAgICAgICBwaG9uZTogbmV3IEZvcm1Db250cm9sKFxuICAgICAgICAgICAgICAgIHRoaXMuY29udGFjdC5waG9uZSxcbiAgICAgICAgICAgICAgICBbVmFsaWRhdG9ycy5taW5MZW5ndGgoMTApLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMCksIFZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMucGF0dGVybignWzAtOV0qJyldXG4gICAgICAgICAgICApLFxuICAgICAgICAgICAgZW1haWw6IG5ldyBGb3JtQ29udHJvbChcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRhY3QuZW1haWwsIFZhbGlkYXRvcnMucGF0dGVybih0aGlzLm1vZHVsZVNlcnZpY2UuZ2V0QXBwQ29uc3RhbnRzKCkuRU1BSUxQQVRURVJOKVxuICAgICAgICAgICAgKSxcbiAgICAgICAgICAgIHN0cmVldF9hZGRyZXNzOiBuZXcgRm9ybUNvbnRyb2wodGhpcy5jb250YWN0LnN0cmVldEFkZHJlc3MpLFxuICAgICAgICAgICAgdHlwZTogbmV3IEZvcm1Db250cm9sKHRoaXMuY29udGFjdC50eXBlKSxcbiAgICAgICAgICAgIGNpdHk6IG5ldyBGb3JtQ29udHJvbCh0aGlzLmNvbnRhY3QuY2l0eSksXG4gICAgICAgICAgICBpZDogbmV3IEZvcm1Db250cm9sKHRoaXMuY29udGFjdC5pZCksXG4gICAgICAgICAgICBjb3VudHJ5X2NvZGU6IG5ldyBGb3JtQ29udHJvbChDT1VOVFJZX0NBTExJTkdfQ09ERVNbMV0pLFxuICAgICAgICAgICAgcGhvbmVfY29kZTogbmV3IEZvcm1Db250cm9sKHRoaXMuY29udGFjdC5waG9uZUNvZGUgfHwgQ09VTlRSWV9DQUxMSU5HX0NPREVTWzFdLmNhbGxpbmdDb2Rlc1swXSksXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuc3RvcmUuQ291bnRyeUNvZGVzJFxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuZGVzdHJveWVkJCkpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKGFzeW5jIChjb3VudHJpZXMpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoIWNvdW50cmllcy5sZW5ndGgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuZmV0Y2hDb3VudHJ5Q29kZXMoKTsgfVxuXG4gICAgICAgICAgICAgICAgY29uc3QgY291bnRyeUNvZGUgPSBjb3VudHJpZXMuZmlsdGVyKGMgPT4gYy5hbHBoYTNDb2RlID09PSB0aGlzLmNvbnRhY3QuY291bnRyeUNvZGUpWzBdIHx8IENPVU5UUllfQ0FMTElOR19DT0RFU1sxXTtcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRhY3RGb3JtLnBhdGNoVmFsdWUoeyBjb3VudHJ5X2NvZGU6IGNvdW50cnlDb2RlIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMuZGVzdHJveWVkJC5uZXh0KHRydWUpO1xuICAgICAgICB0aGlzLmRlc3Ryb3llZCQuY29tcGxldGUoKTtcbiAgICB9XG5cbiAgICBjbG9zZSgpIHtcbiAgICAgICAgdGhpcy5tb2RhbEN0cmwuZGlzbWlzcygvKntzb21lRGF0YTogLi4ufSAqLyk7XG4gICAgfVxuXG4gICAgdXBkYXRlKCkge1xuICAgICAgICAvLyB0aGlzLl9hcHBlbmRDb3VudHJ5Q29kZSgpO1xuXG4gICAgICAgIGNvbnN0IGZvcm0gPSB0aGlzLmNvbnRhY3RGb3JtLnZhbHVlO1xuXG4gICAgICAgIGRlbGV0ZSBmb3JtLnBob25lX2NvZGU7XG5cbiAgICAgICAgZm9ybS5jb3VudHJ5X2NvZGUgPSBmb3JtLmNvdW50cnlfY29kZS5hbHBoYTNDb2RlO1xuXG4gICAgICAgIHRoaXMuc3RvcmUudXBkYXRlQ29udGFjdChmb3JtKTtcbiAgICAgICAgdGhpcy5tb2RhbEN0cmwuZGlzbWlzcygvKntzb21lRGF0YTogLi4ufSAqLyk7XG4gICAgfVxuXG4gICAgY3JlYXRlKCkge1xuICAgICAgICAvLyB0aGlzLl9hcHBlbmRDb3VudHJ5Q29kZSgpO1xuICAgICAgICBjb25zdCBmb3JtID0gdGhpcy5jb250YWN0Rm9ybS52YWx1ZTtcblxuICAgICAgICBkZWxldGUgZm9ybS5waG9uZV9jb2RlO1xuXG4gICAgICAgIGZvcm0uY291bnRyeV9jb2RlID0gZm9ybS5jb3VudHJ5X2NvZGUuYWxwaGEzQ29kZTtcblxuICAgICAgICB0aGlzLnN0b3JlLmNyZWF0ZUNvbnRhY3QoZm9ybSk7XG4gICAgICAgIHRoaXMubW9kYWxDdHJsLmRpc21pc3MoLyp7c29tZURhdGE6IC4uLn0gKi8pO1xuICAgIH1cblxuICAgIGFzeW5jIHByZXNlbnRQb3BvdmVyKC8qZXY6IGFueSovKSB7XG4gICAgICAgIGNvbnN0IGxvYWRlciA9IGF3YWl0IHRoaXMubG9hZGluZ0N0cmwuY3JlYXRlKCk7XG5cbiAgICAgICAgbG9hZGVyLnByZXNlbnQoKTtcblxuICAgICAgICB0aGlzLnN0b3JlLkNvdW50cnlDb2RlcyRcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLmRlc3Ryb3llZCQpKVxuICAgICAgICAgICAgLnN1YnNjcmliZShhc3luYyAoY291bnRyaWVzKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKCFjb3VudHJpZXMubGVuZ3RoKSB7IHJldHVybiB0aGlzLnN0b3JlLmZldGNoQ291bnRyeUNvZGVzKCk7IH1cblxuICAgICAgICAgICAgICAgIGxldCBwb3BvdmVyOiBhbnkgPSBhd2FpdCB0aGlzLm1kYWxDdHJsLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudDogQ291bnRyeVNlbGVjdG9yTW9kYWxDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudFByb3BzOiB7IGNvdW50cmllcyB9LFxuICAgICAgICAgICAgICAgICAgICAvLyBldmVudDogZXYsXG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICBhd2FpdCBwb3BvdmVyLnByZXNlbnQoKTtcblxuICAgICAgICAgICAgICAgIGxvYWRlci5kaXNtaXNzKCk7XG5cbiAgICAgICAgICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHBvcG92ZXIub25EaWREaXNtaXNzKCk7XG4gICAgICAgICAgICAgICAgY29uc3Qgc2VsQ291bnRyeSA9IHJlc3BvbnNlLmRhdGE7XG5cbiAgICAgICAgICAgICAgICBpZiAoc2VsQ291bnRyeSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoc2VsQ291bnRyeS5jYWxsaW5nQ29kZXMubGVuZ3RoID4gMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9wb3ZlciA9IGF3YWl0IHRoaXMucG9wb3ZlckNvbnRyb2xsZXIuY3JlYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb21wb25lbnQ6IENvdW50cnlDb2RlU2VsZWN0b3JQb3BvdmVyQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudFByb3BzOiB7IGNvZGVzOiBzZWxDb3VudHJ5LmNhbGxpbmdDb2RlcyB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGV2ZW50OiBldixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2x1Y2VudDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZHJvcERpc21pc3M6IGZhbHNlXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGF3YWl0IHBvcG92ZXIucHJlc2VudCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgeyBkYXRhIH0gPSBhd2FpdCBwb3BvdmVyLm9uRGlkRGlzbWlzcygpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRhY3RGb3JtLnBhdGNoVmFsdWUoeyBjb3VudHJ5X2NvZGU6IHNlbENvdW50cnksIHBob25lX2NvZGU6IGArJHtkYXRhfWAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRhY3RGb3JtLnBhdGNoVmFsdWUoeyBjb3VudHJ5X2NvZGU6IHNlbENvdW50cnksIHBob25lX2NvZGU6IGArJHtzZWxDb3VudHJ5LmNhbGxpbmdDb2Rlc1swXX1gIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfYXBwZW5kQ291bnRyeUNvZGUoKSB7XG4gICAgICAgIGNvbnN0IGNvdW50cnk6IElDb3VudHJ5Q29kZXMgPSB0aGlzLmNvbnRhY3RGb3JtLmdldCgnY291bnRyeV9jb2RlJykudmFsdWU7XG5cbiAgICAgICAgc3dpdGNoIChjb3VudHJ5LmFscGhhM0NvZGUpIHtcbiAgICAgICAgICAgIGNhc2UgJ01FWCc6XG4gICAgICAgICAgICAgICAgdGhpcy5jb250YWN0Rm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgICAgICAgICAgICAgICAgcGhvbmU6IGArJHtjb3VudHJ5LmNhbGxpbmdDb2Rlc1swXX0gMSAke3RoaXMuY29udGFjdEZvcm0uZ2V0KCdwaG9uZScpLnZhbHVlfWAucmVwbGFjZSgvIC9nLCAnJylcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ0FSRyc6XG4gICAgICAgICAgICAgICAgdGhpcy5jb250YWN0Rm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgICAgICAgICAgICAgICAgcGhvbmU6IGArJHtjb3VudHJ5LmNhbGxpbmdDb2Rlc1swXX0gOSAke3RoaXMuY29udGFjdEZvcm0uZ2V0KCdwaG9uZScpLnZhbHVlfWAucmVwbGFjZSgvIC9nLCAnJylcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuY29udGFjdEZvcm0uZ2V0KCdjb3VudHJ5X2NvZGUnKS5kaXNhYmxlKCk7XG4gICAgfVxufVxuIl19