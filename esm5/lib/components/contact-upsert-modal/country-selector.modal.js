import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
var CountrySelectorModalComponent = /** @class */ (function () {
    function CountrySelectorModalComponent(popoverController) {
        this.popoverController = popoverController;
        this.countries = [];
        this.items = [];
    }
    CountrySelectorModalComponent.prototype.ngOnInit = function () {
        this.items = this.countries;
    };
    CountrySelectorModalComponent.prototype.select = function (country) {
        this.popoverController.dismiss(country);
    };
    CountrySelectorModalComponent.prototype.search = function (searchTerm) {
        this.items = this.countries.filter(function (country) {
            return (country.translations.es || country.name || '')
                .toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') // replace accented chars
                .indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    CountrySelectorModalComponent.prototype.close = function () {
        this.popoverController.dismiss();
    };
    CountrySelectorModalComponent.ctorParameters = function () { return [
        { type: ModalController }
    ]; };
    __decorate([
        Input()
    ], CountrySelectorModalComponent.prototype, "countries", void 0);
    CountrySelectorModalComponent = __decorate([
        Component({
            selector: 'boxx-country-selector-modal',
            template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                GENERAL.ACTION.selectCountry\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #CountryPicker showCancelButton=\"never\" cancelButtonIcon=\"close-circle-outline\"\n            (ionChange)=\"search($event.detail.value)\" placeholder=\"{{'GENERAL.ACTION.searchCountry' | translate}}\"></ion-searchbar>\n    </ion-item-divider>\n\n    <ion-virtual-scroll [items]=\"items\" approxItemHeight=\"64px\">\n        <ion-item *virtualItem=\"let country\" (click)=\"select(country)\">\n            <ion-label>\n                <ion-img [src]=\"country.flag\" [alt]=\"country.name\" style=\"width: 24px; display: inline-block;\">\n                </ion-img>\n                {{country.translations.es || country.name}}\n            </ion-label>\n        </ion-item>\n    </ion-virtual-scroll>\n</ion-content>",
            styles: ["ion-toolbar.md ion-title{padding:0}"]
        })
    ], CountrySelectorModalComponent);
    return CountrySelectorModalComponent;
}());
export { CountrySelectorModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRyeS1zZWxlY3Rvci5tb2RhbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3NoYXJlZC1tb2R1bGVzLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29udGFjdC11cHNlcnQtbW9kYWwvY291bnRyeS1zZWxlY3Rvci5tb2RhbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBU2pEO0lBR0ksdUNBQ1csaUJBQWtDO1FBQWxDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBaUI7UUFIcEMsY0FBUyxHQUF5QixFQUFFLENBQUM7UUFNOUMsVUFBSyxHQUFHLEVBQUUsQ0FBQztJQUZQLENBQUM7SUFJTCxnREFBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ2hDLENBQUM7SUFFRCw4Q0FBTSxHQUFOLFVBQU8sT0FBc0I7UUFDekIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsOENBQU0sR0FBTixVQUFPLFVBQWtCO1FBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsVUFBQSxPQUFPO1lBQ3RDLE9BQU8sQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsSUFBSSxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztpQkFDakQsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLENBQUMsQ0FBQyx5QkFBeUI7aUJBQ3hGLE9BQU8sQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw2Q0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3JDLENBQUM7O2dCQXZCNkIsZUFBZTs7SUFIcEM7UUFBUixLQUFLLEVBQUU7b0VBQXNDO0lBRHJDLDZCQUE2QjtRQUx6QyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsNkJBQTZCO1lBQ3ZDLDI5Q0FBNEM7O1NBRS9DLENBQUM7T0FDVyw2QkFBNkIsQ0E0QnpDO0lBQUQsb0NBQUM7Q0FBQSxBQTVCRCxJQTRCQztTQTVCWSw2QkFBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1vZGFsQ29udHJvbGxlciB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcbmltcG9ydCB7IElDb3VudHJ5Q29kZXMgfSBmcm9tICdAYm94eC9jb250YWN0cy1jb3JlJztcblxuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtY291bnRyeS1zZWxlY3Rvci1tb2RhbCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NvdW50cnktc2VsZWN0b3IubW9kYWwuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vY291bnRyeS1zZWxlY3Rvci5tb2RhbC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQ291bnRyeVNlbGVjdG9yTW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIEBJbnB1dCgpIGNvdW50cmllczogQXJyYXk8SUNvdW50cnlDb2Rlcz4gPSBbXTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwdWJsaWMgcG9wb3ZlckNvbnRyb2xsZXI6IE1vZGFsQ29udHJvbGxlclxuICAgICkgeyB9XG5cbiAgICBpdGVtcyA9IFtdO1xuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMuaXRlbXMgPSB0aGlzLmNvdW50cmllcztcbiAgICB9XG5cbiAgICBzZWxlY3QoY291bnRyeTogSUNvdW50cnlDb2Rlcykge1xuICAgICAgICB0aGlzLnBvcG92ZXJDb250cm9sbGVyLmRpc21pc3MoY291bnRyeSk7XG4gICAgfVxuXG4gICAgc2VhcmNoKHNlYXJjaFRlcm06IHN0cmluZykge1xuICAgICAgICB0aGlzLml0ZW1zID0gdGhpcy5jb3VudHJpZXMuZmlsdGVyKGNvdW50cnkgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIChjb3VudHJ5LnRyYW5zbGF0aW9ucy5lcyB8fCBjb3VudHJ5Lm5hbWUgfHwgJycpXG4gICAgICAgICAgICAgICAgLnRvTG93ZXJDYXNlKCkubm9ybWFsaXplKCdORkQnKS5yZXBsYWNlKC9bXFx1MDMwMC1cXHUwMzZmXS9nLCAnJykgLy8gcmVwbGFjZSBhY2NlbnRlZCBjaGFyc1xuICAgICAgICAgICAgICAgIC5pbmRleE9mKHNlYXJjaFRlcm0udG9Mb3dlckNhc2UoKSkgPiAtMTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgY2xvc2UoKSB7XG4gICAgICAgIHRoaXMucG9wb3ZlckNvbnRyb2xsZXIuZGlzbWlzcygpO1xuICAgIH1cbn1cbiJdfQ==