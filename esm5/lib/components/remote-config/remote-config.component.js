import { __awaiter, __decorate, __generator, __param } from "tslib";
import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { AlertController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FIREBASEX_SERVICE } from '../../services/ISharedModules.service';
var RemoteConfigComponent = /** @class */ (function () {
    function RemoteConfigComponent(firebasex, translate, appVersion, market, alertCtrl, platform) {
        var _this = this;
        this.firebasex = firebasex;
        this.translate = translate;
        this.appVersion = appVersion;
        this.market = market;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.whenFinish = new EventEmitter();
        this.destroyed$ = new Subject();
        this.configMessage = '...';
        this.isIos = false;
        this.translate.get(['GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (t) { return _this.translations = t; });
    }
    RemoteConfigComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.configMessage = _this.translations.GENERAL.loadingRemoteConfig;
            _this.isIos = _this.platform.is('ios');
            _this.checkForUpdates();
            _this.configMessage = '';
        });
    };
    RemoteConfigComponent.prototype.checkForUpdates = function () {
        return __awaiter(this, void 0, void 0, function () {
            var currentVersionNumber, packageName, mostRecentVersion, alert_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.appVersion.getVersionNumber()];
                    case 1:
                        currentVersionNumber = _a.sent();
                        return [4 /*yield*/, this.appVersion.getPackageName()];
                    case 2:
                        packageName = _a.sent();
                        return [4 /*yield*/, this.firebasex.fetch(600)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.firebasex.activateFetched()];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, this.firebasex.getValue('most_recent_version' + (this.isIos ? '_ios' : ''))];
                    case 5:
                        mostRecentVersion = (_a.sent()) || '0.0.1';
                        console.log('--- most_recent_version', mostRecentVersion);
                        if (!(mostRecentVersion > currentVersionNumber)) return [3 /*break*/, 7];
                        return [4 /*yield*/, this.alertCtrl.create({
                                header: this.translations.GENERAL.appUpdateHeader,
                                message: this.translations.GENERAL.appUpdateMsg + " " + mostRecentVersion,
                                buttons: [{
                                        text: this.translations.GENERAL.ACTION.update,
                                        handler: function () {
                                            _this.market.open(packageName);
                                            return false;
                                        }
                                    }],
                                backdropDismiss: false
                            })];
                    case 6:
                        alert_1 = _a.sent();
                        alert_1.present();
                        return [3 /*break*/, 8];
                    case 7:
                        this.whenFinish.emit(true);
                        _a.label = 8;
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    RemoteConfigComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [FIREBASEX_SERVICE,] }] },
        { type: TranslateService },
        { type: AppVersion },
        { type: Market },
        { type: AlertController },
        { type: Platform }
    ]; };
    __decorate([
        Output()
    ], RemoteConfigComponent.prototype, "whenFinish", void 0);
    RemoteConfigComponent = __decorate([
        Component({
            selector: 'boxx-remote-config',
            template: "<div>\n  {{configMessage}}\n</div>\n",
            styles: [""]
        }),
        __param(0, Inject(FIREBASEX_SERVICE))
    ], RemoteConfigComponent);
    return RemoteConfigComponent;
}());
export { RemoteConfigComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVtb3RlLWNvbmZpZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtbW9kdWxlcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3JlbW90ZS1jb25maWcvcmVtb3RlLWNvbmZpZy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBVSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzNELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxpQkFBaUIsRUFBcUIsTUFBTSx1Q0FBdUMsQ0FBQztBQVE3RjtJQUdJLCtCQUN1QyxTQUE0QixFQUN2RCxTQUEyQixFQUMzQixVQUFzQixFQUN0QixNQUFjLEVBQ2QsU0FBMEIsRUFDMUIsUUFBa0I7UUFOOUIsaUJBV0M7UUFWc0MsY0FBUyxHQUFULFNBQVMsQ0FBbUI7UUFDdkQsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0IsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFDMUIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQVJwQixlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQVcsQ0FBQztRQWVuRCxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztRQUlwQyxrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUV0QixVQUFLLEdBQUcsS0FBSyxDQUFDO1FBWFYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTLENBQUMsVUFBQyxDQUFNLElBQUssT0FBQSxLQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsRUFBckIsQ0FBcUIsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFVRCx3Q0FBUSxHQUFSO1FBQUEsaUJBV0M7UUFWRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQztZQUV2QixLQUFJLENBQUMsYUFBYSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDO1lBRW5FLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFckMsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBRXZCLEtBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVLLCtDQUFlLEdBQXJCOzs7Ozs7NEJBRWlDLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsRUFBQTs7d0JBQS9ELG9CQUFvQixHQUFHLFNBQXdDO3dCQUNqRCxxQkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxFQUFBOzt3QkFBcEQsV0FBVyxHQUFHLFNBQXNDO3dCQUUxRCxxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBQTs7d0JBQS9CLFNBQStCLENBQUM7d0JBRWhDLHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxFQUFFLEVBQUE7O3dCQUF0QyxTQUFzQyxDQUFDO3dCQUViLHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHFCQUFxQixHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFBOzt3QkFBckcsaUJBQWlCLEdBQUcsQ0FBQSxTQUFpRixLQUFJLE9BQU87d0JBRXRILE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQzs2QkFFdEQsQ0FBQSxpQkFBaUIsR0FBRyxvQkFBb0IsQ0FBQSxFQUF4Qyx3QkFBd0M7d0JBQzFCLHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO2dDQUN0QyxNQUFNLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsZUFBZTtnQ0FDakQsT0FBTyxFQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksU0FBSSxpQkFBbUI7Z0NBQ3pFLE9BQU8sRUFBRSxDQUFDO3dDQUNOLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTTt3Q0FDN0MsT0FBTyxFQUFFOzRDQUNMLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDOzRDQUU5QixPQUFPLEtBQUssQ0FBQzt3Q0FDakIsQ0FBQztxQ0FDSixDQUFDO2dDQUNGLGVBQWUsRUFBRSxLQUFLOzZCQUN6QixDQUFDLEVBQUE7O3dCQVpJLFVBQVEsU0FZWjt3QkFFRixPQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7Ozt3QkFHaEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Ozs7OztLQUVsQzs7Z0RBbEVJLE1BQU0sU0FBQyxpQkFBaUI7Z0JBQ04sZ0JBQWdCO2dCQUNmLFVBQVU7Z0JBQ2QsTUFBTTtnQkFDSCxlQUFlO2dCQUNoQixRQUFROztJQVJwQjtRQUFULE1BQU0sRUFBRTs2REFBMEM7SUFEMUMscUJBQXFCO1FBTGpDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsZ0RBQTZDOztTQUVoRCxDQUFDO1FBS08sV0FBQSxNQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtPQUpyQixxQkFBcUIsQ0F1RWpDO0lBQUQsNEJBQUM7Q0FBQSxBQXZFRCxJQXVFQztTQXZFWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5qZWN0LCBPbkluaXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQXBwVmVyc2lvbiB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvYXBwLXZlcnNpb24vbmd4JztcbmltcG9ydCB7IE1hcmtldCB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvbWFya2V0L25neCc7XG5pbXBvcnQgeyBBbGVydENvbnRyb2xsZXIsIFBsYXRmb3JtIH0gZnJvbSAnQGlvbmljL2FuZ3VsYXInO1xuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgRklSRUJBU0VYX1NFUlZJQ0UsIElGaXJlYmFzZXhTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvSVNoYXJlZE1vZHVsZXMuc2VydmljZSc7XG5cblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LXJlbW90ZS1jb25maWcnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9yZW1vdGUtY29uZmlnLmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9yZW1vdGUtY29uZmlnLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIFJlbW90ZUNvbmZpZ0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQE91dHB1dCgpIHdoZW5GaW5pc2ggPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgQEluamVjdChGSVJFQkFTRVhfU0VSVklDRSkgcHJpdmF0ZSBmaXJlYmFzZXg6IElGaXJlYmFzZXhTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSBhcHBWZXJzaW9uOiBBcHBWZXJzaW9uLFxuICAgICAgICBwcml2YXRlIG1hcmtldDogTWFya2V0LFxuICAgICAgICBwcml2YXRlIGFsZXJ0Q3RybDogQWxlcnRDb250cm9sbGVyLFxuICAgICAgICBwcml2YXRlIHBsYXRmb3JtOiBQbGF0Zm9ybVxuICAgICkge1xuICAgICAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoWydHRU5FUkFMJ10pXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKHQ6IGFueSkgPT4gdGhpcy50cmFuc2xhdGlvbnMgPSB0KTtcbiAgICB9XG5cbiAgICBkZXN0cm95ZWQkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcblxuICAgIHRyYW5zbGF0aW9uczogYW55O1xuXG4gICAgY29uZmlnTWVzc2FnZSA9ICcuLi4nO1xuXG4gICAgaXNJb3MgPSBmYWxzZTtcblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnBsYXRmb3JtLnJlYWR5KCkudGhlbigoKSA9PiB7XG5cbiAgICAgICAgICAgIHRoaXMuY29uZmlnTWVzc2FnZSA9IHRoaXMudHJhbnNsYXRpb25zLkdFTkVSQUwubG9hZGluZ1JlbW90ZUNvbmZpZztcblxuICAgICAgICAgICAgdGhpcy5pc0lvcyA9IHRoaXMucGxhdGZvcm0uaXMoJ2lvcycpO1xuXG4gICAgICAgICAgICB0aGlzLmNoZWNrRm9yVXBkYXRlcygpO1xuXG4gICAgICAgICAgICB0aGlzLmNvbmZpZ01lc3NhZ2UgPSAnJztcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYXN5bmMgY2hlY2tGb3JVcGRhdGVzKCkge1xuXG4gICAgICAgIGNvbnN0IGN1cnJlbnRWZXJzaW9uTnVtYmVyID0gYXdhaXQgdGhpcy5hcHBWZXJzaW9uLmdldFZlcnNpb25OdW1iZXIoKTtcbiAgICAgICAgY29uc3QgcGFja2FnZU5hbWUgPSBhd2FpdCB0aGlzLmFwcFZlcnNpb24uZ2V0UGFja2FnZU5hbWUoKTtcblxuICAgICAgICBhd2FpdCB0aGlzLmZpcmViYXNleC5mZXRjaCg2MDApO1xuXG4gICAgICAgIGF3YWl0IHRoaXMuZmlyZWJhc2V4LmFjdGl2YXRlRmV0Y2hlZCgpO1xuXG4gICAgICAgIGNvbnN0IG1vc3RSZWNlbnRWZXJzaW9uID0gYXdhaXQgdGhpcy5maXJlYmFzZXguZ2V0VmFsdWUoJ21vc3RfcmVjZW50X3ZlcnNpb24nICsgKHRoaXMuaXNJb3MgPyAnX2lvcycgOiAnJykpIHx8ICcwLjAuMSc7XG5cbiAgICAgICAgY29uc29sZS5sb2coJy0tLSBtb3N0X3JlY2VudF92ZXJzaW9uJywgbW9zdFJlY2VudFZlcnNpb24pO1xuXG4gICAgICAgIGlmIChtb3N0UmVjZW50VmVyc2lvbiA+IGN1cnJlbnRWZXJzaW9uTnVtYmVyKSB7XG4gICAgICAgICAgICBjb25zdCBhbGVydCA9IGF3YWl0IHRoaXMuYWxlcnRDdHJsLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgaGVhZGVyOiB0aGlzLnRyYW5zbGF0aW9ucy5HRU5FUkFMLmFwcFVwZGF0ZUhlYWRlcixcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiBgJHt0aGlzLnRyYW5zbGF0aW9ucy5HRU5FUkFMLmFwcFVwZGF0ZU1zZ30gJHttb3N0UmVjZW50VmVyc2lvbn1gLFxuICAgICAgICAgICAgICAgIGJ1dHRvbnM6IFt7XG4gICAgICAgICAgICAgICAgICAgIHRleHQ6IHRoaXMudHJhbnNsYXRpb25zLkdFTkVSQUwuQUNUSU9OLnVwZGF0ZSxcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlcjogKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tYXJrZXQub3BlbihwYWNrYWdlTmFtZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1dLFxuICAgICAgICAgICAgICAgIGJhY2tkcm9wRGlzbWlzczogZmFsc2VcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBhbGVydC5wcmVzZW50KCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLndoZW5GaW5pc2guZW1pdCh0cnVlKTtcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==