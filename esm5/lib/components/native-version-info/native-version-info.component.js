import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { AppVersionService } from '../../services/NativeAppVersion.service';
var NativeVersionInfoComponent = /** @class */ (function () {
    function NativeVersionInfoComponent(appVersionService) {
        this.appVersionService = appVersionService;
    }
    NativeVersionInfoComponent.prototype.ngOnInit = function () { };
    NativeVersionInfoComponent.ctorParameters = function () { return [
        { type: AppVersionService }
    ]; };
    NativeVersionInfoComponent = __decorate([
        Component({
            selector: 'boxx-native-version-info',
            template: "<div class=\"app-version-number\">\n    <div *ngIf=\"(appVersionService.whenReady$ | async) as appVersionService\">\n        {{appVersionService.versionNumber && appVersionService.versionNumber !== 'PWA' ? 'v'+appVersionService.versionNumber : 'PWA'}}\n    </div>\n</div>",
            styles: [""]
        })
    ], NativeVersionInfoComponent);
    return NativeVersionInfoComponent;
}());
export { NativeVersionInfoComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF0aXZlLXZlcnNpb24taW5mby5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtbW9kdWxlcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL25hdGl2ZS12ZXJzaW9uLWluZm8vbmF0aXZlLXZlcnNpb24taW5mby5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDbEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFPNUU7SUFFRSxvQ0FDUyxpQkFBb0M7UUFBcEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtJQUN6QyxDQUFDO0lBRUwsNkNBQVEsR0FBUixjQUFZLENBQUM7O2dCQUhlLGlCQUFpQjs7SUFIbEMsMEJBQTBCO1FBTHRDLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSwwQkFBMEI7WUFDcEMsMlJBQW1EOztTQUVwRCxDQUFDO09BQ1csMEJBQTBCLENBUXRDO0lBQUQsaUNBQUM7Q0FBQSxBQVJELElBUUM7U0FSWSwwQkFBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQXBwVmVyc2lvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9OYXRpdmVBcHBWZXJzaW9uLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdib3h4LW5hdGl2ZS12ZXJzaW9uLWluZm8nLFxuICB0ZW1wbGF0ZVVybDogJy4vbmF0aXZlLXZlcnNpb24taW5mby5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL25hdGl2ZS12ZXJzaW9uLWluZm8uY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgTmF0aXZlVmVyc2lvbkluZm9Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBhcHBWZXJzaW9uU2VydmljZTogQXBwVmVyc2lvblNlcnZpY2VcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHt9XG5cbn1cbiJdfQ==