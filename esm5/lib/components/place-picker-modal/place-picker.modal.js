import { __awaiter, __decorate, __generator } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { IonSearchbar, ModalController } from '@ionic/angular';
import { Subject } from 'rxjs';
var PlacePickerModalComponent = /** @class */ (function () {
    function PlacePickerModalComponent(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.destroyed$ = new Subject();
        this.searchResults = [];
        this.mapsService = new google.maps.places.AutocompleteService();
    }
    PlacePickerModalComponent.prototype.ngOnDestroy = function () {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    };
    PlacePickerModalComponent.prototype.ionViewDidEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.searchBar.setFocus();
        }, 200);
    };
    PlacePickerModalComponent.prototype.onInput = function (query) {
        var _this = this;
        this.searchResults = [];
        if (!query) {
            return;
        }
        var config = {
            types: [],
            input: query
        };
        this.mapsService.getPlacePredictions(config, function (predictions, status) {
            if (predictions && predictions.length > 0) {
                predictions.forEach(function (prediction) {
                    _this.searchResults.push(prediction);
                });
            }
            else {
                var locationDesc = {
                    description: query,
                    place_id: 'default_place_id'
                };
                _this.searchResults.push(locationDesc);
            }
        });
    };
    PlacePickerModalComponent.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.modalCtrl.dismiss( /*{someData: ...} */);
                return [2 /*return*/];
            });
        });
    };
    PlacePickerModalComponent.prototype.chooseItem = function (item) {
        this.modalCtrl.dismiss({ description: item.description, id: item.place_id });
    };
    PlacePickerModalComponent.prototype.select = function () { };
    PlacePickerModalComponent.ctorParameters = function () { return [
        { type: ModalController }
    ]; };
    __decorate([
        ViewChild('placePickerSB', { static: false })
    ], PlacePickerModalComponent.prototype, "searchBar", void 0);
    PlacePickerModalComponent = __decorate([
        Component({
            selector: 'boxx-place-picker-modal',
            template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                FORM.LABEL.place\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #placePickerSB [debounce]=\"300\" showCancelButton=\"never\"\n            (ionInput)=\"onInput($event.target.value)\" cancelButtonIcon=\"close-circle-outline\"\n            placeholder='{{\"EVENTS.enterAddress\" | translate}}'></ion-searchbar>\n    </ion-item-divider>\n\n    <ion-list class=\"padding-right\">\n        <ion-item *ngFor=\"let item of searchResults\" (click)=\"chooseItem(item)\">\n            <p>{{ item.description }}</p>\n        </ion-item>\n    </ion-list>\n</ion-content>",
            styles: ["ion-toolbar.md ion-title{padding:0}"]
        })
    ], PlacePickerModalComponent);
    return PlacePickerModalComponent;
}());
export { PlacePickerModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxhY2UtcGlja2VyLm1vZGFsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9wbGFjZS1waWNrZXItbW9kYWwvcGxhY2UtcGlja2VyLm1vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFhLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQy9ELE9BQU8sRUFBYyxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFTM0M7SUFHSSxtQ0FBb0IsU0FBMEI7UUFBMUIsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFJOUMsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFHcEMsa0JBQWEsR0FBZSxFQUFFLENBQUM7UUFOM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDcEUsQ0FBQztJQVdELCtDQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCxtREFBZSxHQUFmO1FBQUEsaUJBSUM7UUFIRyxVQUFVLENBQUM7WUFDUCxLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzlCLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCwyQ0FBTyxHQUFQLFVBQVEsS0FBYTtRQUFyQixpQkEwQkM7UUF6QkcsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFFeEIsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNSLE9BQU87U0FDVjtRQUVELElBQU0sTUFBTSxHQUFHO1lBQ1gsS0FBSyxFQUFFLEVBQUU7WUFDVCxLQUFLLEVBQUUsS0FBSztTQUNmLENBQUM7UUFFRixJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxVQUFDLFdBQVcsRUFBRSxNQUFNO1lBQzdELElBQUksV0FBVyxJQUFJLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN2QyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUMsVUFBVTtvQkFDM0IsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3hDLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsSUFBTSxZQUFZLEdBQUc7b0JBQ2pCLFdBQVcsRUFBRSxLQUFLO29CQUNsQixRQUFRLEVBQUUsa0JBQWtCO2lCQUMvQixDQUFDO2dCQUVGLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ3pDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUsseUNBQUssR0FBWDs7O2dCQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFDLG9CQUFvQixDQUFDLENBQUM7Ozs7S0FDaEQ7SUFFRCw4Q0FBVSxHQUFWLFVBQVcsSUFBUztRQUNoQixJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUNqRixDQUFDO0lBRUQsMENBQU0sR0FBTixjQUFXLENBQUM7O2dCQTVEbUIsZUFBZTs7SUFGQztRQUE5QyxTQUFTLENBQUMsZUFBZSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDO2dFQUF5QjtJQUQ5RCx5QkFBeUI7UUFMckMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQywrdUNBQXdDOztTQUUzQyxDQUFDO09BQ1cseUJBQXlCLENBZ0VyQztJQUFELGdDQUFDO0NBQUEsQUFoRUQsSUFnRUM7U0FoRVkseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkRlc3Ryb3ksIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSW9uU2VhcmNoYmFyLCBNb2RhbENvbnRyb2xsZXIgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5cbmRlY2xhcmUgdmFyIGdvb2dsZTogYW55O1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtcGxhY2UtcGlja2VyLW1vZGFsJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vcGxhY2UtcGlja2VyLm1vZGFsLmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL3BsYWNlLXBpY2tlci5tb2RhbC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgUGxhY2VQaWNrZXJNb2RhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uRGVzdHJveSB7XG4gICAgQFZpZXdDaGlsZCgncGxhY2VQaWNrZXJTQicsIHsgc3RhdGljOiBmYWxzZSB9KSBzZWFyY2hCYXI6IElvblNlYXJjaGJhcjtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgbW9kYWxDdHJsOiBNb2RhbENvbnRyb2xsZXIpIHtcbiAgICAgICAgdGhpcy5tYXBzU2VydmljZSA9IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuQXV0b2NvbXBsZXRlU2VydmljZSgpO1xuICAgIH1cblxuICAgIGRlc3Ryb3llZCQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xuICAgIGlzTG9hZGluZyQ6IE9ic2VydmFibGU8Ym9vbGVhbj47XG5cbiAgICBzZWFyY2hSZXN1bHRzOiBBcnJheTxhbnk+ID0gW107XG5cbiAgICB0cmFuc2xhdGlvbnM6IGFueTtcblxuICAgIG1hcHNTZXJ2aWNlOiBhbnk7XG5cbiAgICBuZ09uRGVzdHJveSgpIHtcbiAgICAgICAgdGhpcy5kZXN0cm95ZWQkLm5leHQodHJ1ZSk7XG4gICAgICAgIHRoaXMuZGVzdHJveWVkJC5jb21wbGV0ZSgpO1xuICAgIH1cblxuICAgIGlvblZpZXdEaWRFbnRlcigpIHtcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNlYXJjaEJhci5zZXRGb2N1cygpO1xuICAgICAgICB9LCAyMDApO1xuICAgIH1cblxuICAgIG9uSW5wdXQocXVlcnk6IHN0cmluZykge1xuICAgICAgICB0aGlzLnNlYXJjaFJlc3VsdHMgPSBbXTtcblxuICAgICAgICBpZiAoIXF1ZXJ5KSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBjb25maWcgPSB7XG4gICAgICAgICAgICB0eXBlczogW10sIC8vIG90aGVyIHR5cGVzIGF2YWlsYWJsZSBpbiB0aGUgQVBJOiAnYWRkcmVzcyAnZXN0YWJsaXNobWVudCcsICdyZWdpb25zJywgYW5kICdjaXRpZXMnXG4gICAgICAgICAgICBpbnB1dDogcXVlcnlcbiAgICAgICAgfTtcblxuICAgICAgICB0aGlzLm1hcHNTZXJ2aWNlLmdldFBsYWNlUHJlZGljdGlvbnMoY29uZmlnLCAocHJlZGljdGlvbnMsIHN0YXR1cykgPT4ge1xuICAgICAgICAgICAgaWYgKHByZWRpY3Rpb25zICYmIHByZWRpY3Rpb25zLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICBwcmVkaWN0aW9ucy5mb3JFYWNoKChwcmVkaWN0aW9uKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoUmVzdWx0cy5wdXNoKHByZWRpY3Rpb24pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25zdCBsb2NhdGlvbkRlc2MgPSB7XG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBxdWVyeSxcbiAgICAgICAgICAgICAgICAgICAgcGxhY2VfaWQ6ICdkZWZhdWx0X3BsYWNlX2lkJ1xuICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaFJlc3VsdHMucHVzaChsb2NhdGlvbkRlc2MpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBhc3luYyBjbG9zZSgpIHtcbiAgICAgICAgdGhpcy5tb2RhbEN0cmwuZGlzbWlzcygvKntzb21lRGF0YTogLi4ufSAqLyk7XG4gICAgfVxuXG4gICAgY2hvb3NlSXRlbShpdGVtOiBhbnkpIHtcbiAgICAgICAgdGhpcy5tb2RhbEN0cmwuZGlzbWlzcyh7IGRlc2NyaXB0aW9uOiBpdGVtLmRlc2NyaXB0aW9uLCBpZDogaXRlbS5wbGFjZV9pZCB9KTtcbiAgICB9XG5cbiAgICBzZWxlY3QoKSB7IH1cbn1cbiJdfQ==