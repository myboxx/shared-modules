import { __awaiter, __decorate, __generator } from "tslib";
import { Component, Input, ViewChild } from '@angular/core';
import { ContactModel, ContactStore } from '@boxx/contacts-core';
import { AlertController, IonSearchbar, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var ContactPickerModalComponent = /** @class */ (function () {
    function ContactPickerModalComponent(modalCtrl, store, alertCtrl, translate) {
        var _this = this;
        this.modalCtrl = modalCtrl;
        this.store = store;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.searchValue = '';
        this.contactList = [];
        this.isFiltering = false;
        this.destroyed$ = new Subject();
        this.translate.get(['CONTACTS', 'GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (t) { return _this.translations = t; });
    }
    ContactPickerModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isLoading$ = this.store.Loading$;
        this.store.Contacts$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (cl) {
            _this.searchResults = _this.contactList = cl.map(function (c) {
                return ({ isChecked: _this.currentAttendees.findIndex(function (e) { return e === c.id; }) !== -1, contact: c });
            });
            setTimeout(function () {
                _this.searchBar.setFocus();
            }, 800);
        });
        this.store.HasBeenFetched$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (ok) {
            if (!ok) {
                _this.store.fetchContacts();
            }
        });
    };
    ContactPickerModalComponent.prototype.ngOnDestroy = function () {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    };
    ContactPickerModalComponent.prototype.checkMaster = function () {
        var _this = this;
        setTimeout(function () {
            _this.searchResults.forEach(function (obj) {
                obj.isChecked = _this.masterCheck;
            });
        });
    };
    ContactPickerModalComponent.prototype.checkEvent = function () {
        var totalItems = this.searchResults.length;
        var checked = 0;
        this.searchResults.map(function (obj) {
            if (obj.isChecked) {
                checked++;
            }
        });
        if (checked > 0 && checked < totalItems) {
            // If even one item is checked but not all
            this.isIndeterminate = true;
            this.masterCheck = false;
        }
        else if (checked === totalItems) {
            // If all are checked
            this.masterCheck = true;
            this.isIndeterminate = false;
        }
        else {
            // If none is checked
            this.isIndeterminate = false;
            this.masterCheck = false;
        }
    };
    ContactPickerModalComponent.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.hasChanges()) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.alertCtrl.create({
                                header: this.translations.GENERAL.ACTION.confirm,
                                message: this.translations.CONTACTS.confirmCloseWithSelectedContactsMsg,
                                buttons: [{
                                        text: this.translations.GENERAL.ACTION.ok,
                                        handler: function () {
                                            _this.modalCtrl.dismiss( /*{someData: ...} */);
                                        }
                                    }, {
                                        text: this.translations.GENERAL.ACTION.cancel,
                                        role: 'cancel',
                                        cssClass: 'primary',
                                    },]
                            })];
                    case 1:
                        (_a.sent()).present();
                        return [3 /*break*/, 3];
                    case 2:
                        this.modalCtrl.dismiss( /*{someData: ...} */);
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ContactPickerModalComponent.prototype.select = function () {
        return this.modalCtrl.dismiss(this._getSelectedContacts());
    };
    ContactPickerModalComponent.prototype._getSelectedContacts = function () {
        return this.searchResults.filter(function (c) { return c.isChecked; }).map(function (c) { return c.contact; });
    };
    ContactPickerModalComponent.prototype.hasChanges = function () {
        var _this = this;
        return this._getSelectedContacts().length !== this.currentAttendees.length ||
            this._getSelectedContacts()
                .filter(function (c) { return !_this.currentAttendees.includes(c.id); }).length > 0;
    };
    ContactPickerModalComponent.prototype.search = function (searchTerm) {
        this.searchResults = this.contactList;
        if (!searchTerm) {
            this.isFiltering = false;
            return;
        }
        this.isFiltering = true;
        this.searchResults = this.searchResults.filter(function (item) {
            return (item.contact.name + item.contact.lastName).toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
                item.contact.phone.indexOf(searchTerm) > -1 ||
                item.contact.email.indexOf(searchTerm) > -1;
        });
    };
    ContactPickerModalComponent.prototype.showAll = function () {
        var _this = this;
        this.searchValue = ' ';
        setTimeout(function () {
            _this.searchValue = '';
        }, 10);
    };
    ContactPickerModalComponent.ctorParameters = function () { return [
        { type: ModalController },
        { type: ContactStore },
        { type: AlertController },
        { type: TranslateService }
    ]; };
    __decorate([
        ViewChild('contactPickerSB', { static: false })
    ], ContactPickerModalComponent.prototype, "searchBar", void 0);
    __decorate([
        Input()
    ], ContactPickerModalComponent.prototype, "currentAttendees", void 0);
    ContactPickerModalComponent = __decorate([
        Component({
            selector: 'boxx-contact-picker-modal',
            template: "<ion-header mode=\"md\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\" translate>\n                GENERAL.contacts\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\" [hidden]=\"isFiltering\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <ion-buttons slot=\"end\" [hidden]=\"isFiltering || !hasChanges()\">\n            <ion-button (click)=\"select()\" translate>\n                <ion-icon name=\"checkmark-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <ion-buttons slot=\"end\" [hidden]=\"!isFiltering\">\n            <ion-button (click)=\"showAll()\" translate>\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #contactPickerSB showCancelButton=\"never\" cancelButtonIcon=\"close-circle-outline\"\n        (ionChange)=\"search($event.detail.value)\" [value]=\"searchValue\"\n        placeholder=\"{{'CONTACTS.searchPlaceholder'| translate}}\"></ion-searchbar>\n    </ion-item-divider>\n\n    <div *ngIf=\"isLoading$ | async\" class=\"ion-padding\">\n        <ion-item class=\"ion-no-padding\">\n            <ion-label>\n                <ion-skeleton-text animated style=\"width: 40%\"></ion-skeleton-text>\n            </ion-label>\n            <ion-thumbnail slot=\"start\" style=\"width: 20px; height: 20px; margin-right: 30px; margin-left: 4px;\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n            </ion-thumbnail>\n        </ion-item>\n        <ion-list>\n            <ion-item class=\"ion-no-padding\" *ngFor=\"let item of [].constructor(5)\">\n                <ion-label>\n                    <ion-skeleton-text animated style=\"width: 80%\"></ion-skeleton-text>\n                    <ion-skeleton-text animated style=\"width: 60%\"></ion-skeleton-text>\n                </ion-label>\n                <ion-thumbnail slot=\"start\" style=\"width: 20px; height: 20px; margin-right: 30px; margin-left: 4px;\">\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                </ion-thumbnail>\n            </ion-item>\n        </ion-list>\n    </div>\n\n    <ion-item [hidden]=\"(isLoading$ | async) || isFiltering\" class=\"padding-right\">\n        <ion-label>\n            <strong>{{\"GENERAL.total\" | translate}}: {{contactList.length}}\n                {{ (contactList.length == 1 ? \"GENERAL.contact\" : \"GENERAL.contacts\") | translate}}\n            </strong>\n        </ion-label>\n        <ion-checkbox mode=\"ios\" slot=\"start\" [(ngModel)]=\"masterCheck\" [indeterminate]=\"isIndeterminate\"\n            (click)=\"checkMaster()\"></ion-checkbox>\n    </ion-item>\n\n    <ion-list class=\"ion-padding\">\n        <ion-item class=\"ion-no-padding\" *ngFor=\"let item of searchResults\">\n            <ion-label>\n                <h3>{{item.contact.name}} {{item.contact.lastName}}</h3>\n                <p>{{item.contact.email}}</p>\n                <p>{{item.contact.phone}}</p>\n            </ion-label>\n            <ion-checkbox mode=\"ios\" slot=\"start\" [(ngModel)]=\"item.isChecked\" (ionChange)=\"checkEvent()\">\n            </ion-checkbox>\n        </ion-item>\n    </ion-list>\n</ion-content>",
            styles: ["ion-toolbar.md ion-title{padding:0}"]
        })
    ], ContactPickerModalComponent);
    return ContactPickerModalComponent;
}());
export { ContactPickerModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdC1waWNrZXIubW9kYWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtbW9kdWxlcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvbnRhY3QtcGlja2VyLW1vZGFsL2NvbnRhY3QtcGlja2VyLm1vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBcUIsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDakUsT0FBTyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDaEYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkQsT0FBTyxFQUFjLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMzQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFRM0M7SUFLSSxxQ0FDWSxTQUEwQixFQUMxQixLQUFtQixFQUNuQixTQUEwQixFQUMxQixTQUEyQjtRQUp2QyxpQkFTQztRQVJXLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBQzFCLFVBQUssR0FBTCxLQUFLLENBQWM7UUFDbkIsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFDMUIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFPdkMsZ0JBQVcsR0FBRyxFQUFFLENBQUM7UUFDakIsZ0JBQVcsR0FBeUQsRUFBRSxDQUFDO1FBR3ZFLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBSXBCLGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBYmhDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxDQUFDO2FBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxVQUFDLENBQU0sSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixDQUFDLENBQUM7SUFDdEQsQ0FBQztJQWVELDhDQUFRLEdBQVI7UUFBQSxpQkFzQkM7UUFwQkcsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztRQUV0QyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVM7YUFDZixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTLENBQUMsVUFBQSxFQUFFO1lBQ1QsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDO2dCQUM1QyxPQUFBLENBQUMsRUFBRSxTQUFTLEVBQUUsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxFQUFWLENBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUF0RixDQUFzRixDQUN6RixDQUFDO1lBQ0YsVUFBVSxDQUFDO2dCQUNQLEtBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDOUIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1osQ0FBQyxDQUFDLENBQUM7UUFFUCxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWU7YUFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUyxDQUFDLFVBQUEsRUFBRTtZQUNULElBQUksQ0FBQyxFQUFFLEVBQUU7Z0JBQ0wsS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUM5QjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELGlEQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCxpREFBVyxHQUFYO1FBQUEsaUJBTUM7UUFMRyxVQUFVLENBQUM7WUFDUCxLQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFBLEdBQUc7Z0JBQzFCLEdBQUcsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQztZQUNyQyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGdEQUFVLEdBQVY7UUFDSSxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztRQUM3QyxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsVUFBQSxHQUFHO1lBQ3RCLElBQUksR0FBRyxDQUFDLFNBQVMsRUFBRTtnQkFBRSxPQUFPLEVBQUUsQ0FBQzthQUFFO1FBQ3JDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxPQUFPLEdBQUcsQ0FBQyxJQUFJLE9BQU8sR0FBRyxVQUFVLEVBQUU7WUFDckMsMENBQTBDO1lBQzFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1NBQzVCO2FBQU0sSUFBSSxPQUFPLEtBQUssVUFBVSxFQUFFO1lBQy9CLHFCQUFxQjtZQUNyQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUN4QixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztTQUNoQzthQUFNO1lBQ0gscUJBQXFCO1lBQ3JCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1lBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1NBQzVCO0lBQ0wsQ0FBQztJQUVLLDJDQUFLLEdBQVg7Ozs7Ozs2QkFDUSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQWpCLHdCQUFpQjt3QkFDaEIscUJBQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7Z0NBQ3pCLE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTztnQ0FDaEQsT0FBTyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLG1DQUFtQztnQ0FDdkUsT0FBTyxFQUFFLENBQUM7d0NBQ04sSUFBSSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dDQUN6QyxPQUFPLEVBQUU7NENBQ0wsS0FBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUMsb0JBQW9CLENBQUMsQ0FBQzt3Q0FDakQsQ0FBQztxQ0FDSixFQUFFO3dDQUNDLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTTt3Q0FDN0MsSUFBSSxFQUFFLFFBQVE7d0NBQ2QsUUFBUSxFQUFFLFNBQVM7cUNBQ3RCLEVBQUc7NkJBQ1AsQ0FBQyxFQUFBOzt3QkFiRixDQUFDLFNBYUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDOzs7d0JBR2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUMsb0JBQW9CLENBQUMsQ0FBQzs7Ozs7O0tBRXBEO0lBRUQsNENBQU0sR0FBTjtRQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRU8sMERBQW9CLEdBQTVCO1FBQ0ksT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxTQUFTLEVBQVgsQ0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLE9BQU8sRUFBVCxDQUFTLENBQUMsQ0FBQztJQUMzRSxDQUFDO0lBRUQsZ0RBQVUsR0FBVjtRQUFBLGlCQUlDO1FBSEcsT0FBTyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU07WUFDdEUsSUFBSSxDQUFDLG9CQUFvQixFQUFFO2lCQUN0QixNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFyQyxDQUFxQyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUMzRSxDQUFDO0lBRUQsNENBQU0sR0FBTixVQUFPLFVBQWtCO1FBRXJCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUV0QyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2IsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7WUFDekIsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFFeEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUk7WUFDL0MsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDbkcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDM0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3BELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDZDQUFPLEdBQVA7UUFBQSxpQkFLQztRQUpHLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDO1FBQ3ZCLFVBQVUsQ0FBQztZQUNQLEtBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQzFCLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNYLENBQUM7O2dCQTVJc0IsZUFBZTtnQkFDbkIsWUFBWTtnQkFDUixlQUFlO2dCQUNmLGdCQUFnQjs7SUFSVTtRQUFoRCxTQUFTLENBQUMsaUJBQWlCLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7a0VBQXlCO0lBRWhFO1FBQVIsS0FBSyxFQUFFO3lFQUE0QjtJQUgzQiwyQkFBMkI7UUFMdkMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLDJCQUEyQjtZQUNyQyxncUhBQTBDOztTQUU3QyxDQUFDO09BQ1csMkJBQTJCLENBbUp2QztJQUFELGtDQUFDO0NBQUEsQUFuSkQsSUFtSkM7U0FuSlksMkJBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25EZXN0cm95LCBPbkluaXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29udGFjdE1vZGVsLCBDb250YWN0U3RvcmUgfSBmcm9tICdAYm94eC9jb250YWN0cy1jb3JlJztcbmltcG9ydCB7IEFsZXJ0Q29udHJvbGxlciwgSW9uU2VhcmNoYmFyLCBNb2RhbENvbnRyb2xsZXIgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LWNvbnRhY3QtcGlja2VyLW1vZGFsJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vY29udGFjdC1waWNrZXIubW9kYWwuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vY29udGFjdC1waWNrZXIubW9kYWwuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIENvbnRhY3RQaWNrZXJNb2RhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgICBAVmlld0NoaWxkKCdjb250YWN0UGlja2VyU0InLCB7IHN0YXRpYzogZmFsc2UgfSkgc2VhcmNoQmFyOiBJb25TZWFyY2hiYXI7XG5cbiAgICBASW5wdXQoKSBjdXJyZW50QXR0ZW5kZWVzOiBudW1iZXJbXTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIG1vZGFsQ3RybDogTW9kYWxDb250cm9sbGVyLFxuICAgICAgICBwcml2YXRlIHN0b3JlOiBDb250YWN0U3RvcmUsXG4gICAgICAgIHByaXZhdGUgYWxlcnRDdHJsOiBBbGVydENvbnRyb2xsZXIsXG4gICAgICAgIHByaXZhdGUgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlXG4gICAgKSB7XG4gICAgICAgIHRoaXMudHJhbnNsYXRlLmdldChbJ0NPTlRBQ1RTJywgJ0dFTkVSQUwnXSlcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLmRlc3Ryb3llZCQpKVxuICAgICAgICAgICAgLnN1YnNjcmliZSgodDogYW55KSA9PiB0aGlzLnRyYW5zbGF0aW9ucyA9IHQpO1xuICAgIH1cblxuICAgIHNlYXJjaFZhbHVlID0gJyc7XG4gICAgY29udGFjdExpc3Q6IEFycmF5PHsgaXNDaGVja2VkOiBib29sZWFuLCBjb250YWN0OiBDb250YWN0TW9kZWwgfT4gPSBbXTtcbiAgICBzZWFyY2hSZXN1bHRzOiBBcnJheTx7IGlzQ2hlY2tlZDogYm9vbGVhbiwgY29udGFjdDogQ29udGFjdE1vZGVsIH0+O1xuXG4gICAgaXNGaWx0ZXJpbmcgPSBmYWxzZTtcbiAgICBpc0luZGV0ZXJtaW5hdGU6IGJvb2xlYW47XG4gICAgbWFzdGVyQ2hlY2s6IGJvb2xlYW47XG5cbiAgICBkZXN0cm95ZWQkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcbiAgICBpc0xvYWRpbmckOiBPYnNlcnZhYmxlPGJvb2xlYW4+O1xuXG4gICAgdHJhbnNsYXRpb25zOiBhbnk7XG5cbiAgICBuZ09uSW5pdCgpIHtcblxuICAgICAgICB0aGlzLmlzTG9hZGluZyQgPSB0aGlzLnN0b3JlLkxvYWRpbmckO1xuXG4gICAgICAgIHRoaXMuc3RvcmUuQ29udGFjdHMkXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoY2wgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoUmVzdWx0cyA9IHRoaXMuY29udGFjdExpc3QgPSBjbC5tYXAoYyA9PlxuICAgICAgICAgICAgICAgICAgICAoeyBpc0NoZWNrZWQ6IHRoaXMuY3VycmVudEF0dGVuZGVlcy5maW5kSW5kZXgoKGUpID0+IGUgPT09IGMuaWQpICE9PSAtMSwgY29udGFjdDogYyB9KVxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoQmFyLnNldEZvY3VzKCk7XG4gICAgICAgICAgICAgICAgfSwgODAwKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuc3RvcmUuSGFzQmVlbkZldGNoZWQkXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUob2sgPT4ge1xuICAgICAgICAgICAgICAgIGlmICghb2spIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdG9yZS5mZXRjaENvbnRhY3RzKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMuZGVzdHJveWVkJC5uZXh0KHRydWUpO1xuICAgICAgICB0aGlzLmRlc3Ryb3llZCQuY29tcGxldGUoKTtcbiAgICB9XG5cbiAgICBjaGVja01hc3RlcigpIHtcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNlYXJjaFJlc3VsdHMuZm9yRWFjaChvYmogPT4ge1xuICAgICAgICAgICAgICAgIG9iai5pc0NoZWNrZWQgPSB0aGlzLm1hc3RlckNoZWNrO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGNoZWNrRXZlbnQoKSB7XG4gICAgICAgIGNvbnN0IHRvdGFsSXRlbXMgPSB0aGlzLnNlYXJjaFJlc3VsdHMubGVuZ3RoO1xuICAgICAgICBsZXQgY2hlY2tlZCA9IDA7XG4gICAgICAgIHRoaXMuc2VhcmNoUmVzdWx0cy5tYXAob2JqID0+IHtcbiAgICAgICAgICAgIGlmIChvYmouaXNDaGVja2VkKSB7IGNoZWNrZWQrKzsgfVxuICAgICAgICB9KTtcbiAgICAgICAgaWYgKGNoZWNrZWQgPiAwICYmIGNoZWNrZWQgPCB0b3RhbEl0ZW1zKSB7XG4gICAgICAgICAgICAvLyBJZiBldmVuIG9uZSBpdGVtIGlzIGNoZWNrZWQgYnV0IG5vdCBhbGxcbiAgICAgICAgICAgIHRoaXMuaXNJbmRldGVybWluYXRlID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMubWFzdGVyQ2hlY2sgPSBmYWxzZTtcbiAgICAgICAgfSBlbHNlIGlmIChjaGVja2VkID09PSB0b3RhbEl0ZW1zKSB7XG4gICAgICAgICAgICAvLyBJZiBhbGwgYXJlIGNoZWNrZWRcbiAgICAgICAgICAgIHRoaXMubWFzdGVyQ2hlY2sgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy5pc0luZGV0ZXJtaW5hdGUgPSBmYWxzZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIC8vIElmIG5vbmUgaXMgY2hlY2tlZFxuICAgICAgICAgICAgdGhpcy5pc0luZGV0ZXJtaW5hdGUgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMubWFzdGVyQ2hlY2sgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFzeW5jIGNsb3NlKCkge1xuICAgICAgICBpZiAodGhpcy5oYXNDaGFuZ2VzKCkpIHtcbiAgICAgICAgICAgIChhd2FpdCB0aGlzLmFsZXJ0Q3RybC5jcmVhdGUoe1xuICAgICAgICAgICAgICAgIGhlYWRlcjogdGhpcy50cmFuc2xhdGlvbnMuR0VORVJBTC5BQ1RJT04uY29uZmlybSxcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiB0aGlzLnRyYW5zbGF0aW9ucy5DT05UQUNUUy5jb25maXJtQ2xvc2VXaXRoU2VsZWN0ZWRDb250YWN0c01zZyxcbiAgICAgICAgICAgICAgICBidXR0b25zOiBbe1xuICAgICAgICAgICAgICAgICAgICB0ZXh0OiB0aGlzLnRyYW5zbGF0aW9ucy5HRU5FUkFMLkFDVElPTi5vayxcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlcjogKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tb2RhbEN0cmwuZGlzbWlzcygvKntzb21lRGF0YTogLi4ufSAqLyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgICAgIHRleHQ6IHRoaXMudHJhbnNsYXRpb25zLkdFTkVSQUwuQUNUSU9OLmNhbmNlbCxcbiAgICAgICAgICAgICAgICAgICAgcm9sZTogJ2NhbmNlbCcsXG4gICAgICAgICAgICAgICAgICAgIGNzc0NsYXNzOiAncHJpbWFyeScsXG4gICAgICAgICAgICAgICAgfSwgXVxuICAgICAgICAgICAgfSkpLnByZXNlbnQoKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMubW9kYWxDdHJsLmRpc21pc3MoLyp7c29tZURhdGE6IC4uLn0gKi8pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc2VsZWN0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5tb2RhbEN0cmwuZGlzbWlzcyh0aGlzLl9nZXRTZWxlY3RlZENvbnRhY3RzKCkpO1xuICAgIH1cblxuICAgIHByaXZhdGUgX2dldFNlbGVjdGVkQ29udGFjdHMoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlYXJjaFJlc3VsdHMuZmlsdGVyKGMgPT4gYy5pc0NoZWNrZWQpLm1hcChjID0+IGMuY29udGFjdCk7XG4gICAgfVxuXG4gICAgaGFzQ2hhbmdlcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2dldFNlbGVjdGVkQ29udGFjdHMoKS5sZW5ndGggIT09IHRoaXMuY3VycmVudEF0dGVuZGVlcy5sZW5ndGggfHxcbiAgICAgICAgICAgIHRoaXMuX2dldFNlbGVjdGVkQ29udGFjdHMoKVxuICAgICAgICAgICAgICAgIC5maWx0ZXIoYyA9PiAhdGhpcy5jdXJyZW50QXR0ZW5kZWVzLmluY2x1ZGVzKGMuaWQpKS5sZW5ndGggPiAwO1xuICAgIH1cblxuICAgIHNlYXJjaChzZWFyY2hUZXJtOiBzdHJpbmcpIHtcblxuICAgICAgICB0aGlzLnNlYXJjaFJlc3VsdHMgPSB0aGlzLmNvbnRhY3RMaXN0O1xuXG4gICAgICAgIGlmICghc2VhcmNoVGVybSkge1xuICAgICAgICAgICAgdGhpcy5pc0ZpbHRlcmluZyA9IGZhbHNlO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5pc0ZpbHRlcmluZyA9IHRydWU7XG5cbiAgICAgICAgdGhpcy5zZWFyY2hSZXN1bHRzID0gdGhpcy5zZWFyY2hSZXN1bHRzLmZpbHRlcihpdGVtID0+IHtcbiAgICAgICAgICAgIHJldHVybiAoaXRlbS5jb250YWN0Lm5hbWUgKyBpdGVtLmNvbnRhY3QubGFzdE5hbWUpLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihzZWFyY2hUZXJtLnRvTG93ZXJDYXNlKCkpID4gLTEgfHxcbiAgICAgICAgICAgICAgICBpdGVtLmNvbnRhY3QucGhvbmUuaW5kZXhPZihzZWFyY2hUZXJtKSA+IC0xIHx8XG4gICAgICAgICAgICAgICAgaXRlbS5jb250YWN0LmVtYWlsLmluZGV4T2Yoc2VhcmNoVGVybSkgPiAtMTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc2hvd0FsbCgpIHtcbiAgICAgICAgdGhpcy5zZWFyY2hWYWx1ZSA9ICcgJztcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNlYXJjaFZhbHVlID0gJyc7XG4gICAgICAgIH0sIDEwKTtcbiAgICB9XG59XG4iXX0=