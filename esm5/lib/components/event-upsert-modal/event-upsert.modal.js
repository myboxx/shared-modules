import { __awaiter, __decorate, __generator, __param, __read, __spread } from "tslib";
import { Component, Inject, Input, Optional } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ContactModel } from '@boxx/contacts-core';
import { EventModel, EventsStore, IEventReminder } from '@boxx/events-core';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import * as moment_ from 'moment';
import { Subject } from 'rxjs';
import { takeUntil, withLatestFrom } from 'rxjs/operators';
import { AppSettingsService, LOCAL_NOTIFICATIONS_SERVICE } from '../../providers/global-params';
import { ContactPickerModalComponent } from '../contact-picker-modal/contact-picker.modal';
import { PlacePickerModalComponent } from '../place-picker-modal/place-picker.modal';
// To prevent compiling errors
var moment = moment_;
var EventUpsertModalComponent = /** @class */ (function () {
    function EventUpsertModalComponent(localNotifSrvc, store, loadingCtrl, modalCtrl, alertCtrl, translate, toastController, moduleService) {
        var _this = this;
        this.localNotifSrvc = localNotifSrvc;
        this.store = store;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.toastController = toastController;
        this.moduleService = moduleService;
        this.destroyed$ = new Subject();
        // startTime = moment().toISOString(true);
        // endTime = moment().add(1, 'hours').toISOString(true);
        this.monthNames = moment.months().toString();
        this.evAttendees = [];
        this.validationMessages = {
            title: this.moduleService.getAppConstants().VALIDATION_MESSAGES.title,
            place: this.moduleService.getAppConstants().VALIDATION_MESSAGES.place,
            datetime_from: this.moduleService.getAppConstants().VALIDATION_MESSAGES.datetime_from,
            datetime_to: this.moduleService.getAppConstants().VALIDATION_MESSAGES.datetime_to,
        };
        this.translate.get(['GENERAL', 'FORM', 'EVENTS'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (t) { return _this.translations = t; });
    }
    EventUpsertModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.eventForm = new FormGroup({
            title: new FormControl('', Validators.required),
            place: new FormControl(),
            datetime_from: new FormControl(moment(this.period.from).toISOString(true), Validators.required),
            datetime_to: new FormControl(moment(this.period.to).add(1, 'hours').toISOString(true)),
            attendees: new FormControl(this.attendees || []),
            description: new FormControl(''),
            latitude: new FormControl(''),
            longitude: new FormControl('')
        });
        this.evAttendees = this.eventForm.get('attendees').value;
        if (this.action === 'UPDATE') {
            this.store.selectedEvent$()
                .pipe(takeUntil(this.destroyed$))
                .subscribe(function (id) {
                _this.eventId = id;
                _this.store.EventById$(id)
                    .pipe(takeUntil(_this.destroyed$))
                    .subscribe(function (m) {
                    _this.evAttendees = m.attendees.map(function (c) { return new ContactModel({
                        id: c.contactId, name: c.name, lastName: c.lastName, email: c.email, phone: c.phone
                    }); });
                    _this.eventForm.patchValue({
                        title: m.title,
                        place: m.place,
                        datetime_from: moment(m.datetimeFrom).toISOString(true),
                        datetime_to: moment(m.datetimeTo).toISOString(true),
                        attendees: _this.evAttendees,
                        description: m.description
                    });
                });
            });
        }
        else {
            // only to reset the state.success property
            this.store.EventById$(null);
        }
        this._handleLoading();
        this._handleSuccess();
        this._handleError();
    };
    EventUpsertModalComponent.prototype._handleLoading = function () {
        var _this = this;
        this.loading$ = this.store.Loading$;
        this.loading$
            .pipe(takeUntil(this.destroyed$), withLatestFrom(this.store.Error$))
            .subscribe(function (_a) {
            var _b = __read(_a, 2), loading = _b[0], error = _b[1];
            return __awaiter(_this, void 0, void 0, function () {
                var _c;
                var _this = this;
                return __generator(this, function (_d) {
                    switch (_d.label) {
                        case 0:
                            if (!loading) return [3 /*break*/, 2];
                            _c = this;
                            return [4 /*yield*/, this.loadingCtrl.create()];
                        case 1:
                            _c.loader = _d.sent();
                            this.loader.present();
                            return [3 /*break*/, 3];
                        case 2:
                            if (this.loader) {
                                this.loadingCtrl.dismiss().then(function () {
                                    _this.loader = null;
                                });
                            }
                            _d.label = 3;
                        case 3: return [2 /*return*/];
                    }
                });
            });
        });
    };
    EventUpsertModalComponent.prototype._handleError = function () {
        var _this = this;
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (e) { return __awaiter(_this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!e) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.toastController.create({
                                message: this.translations.EVENTS.ERRORS[e.after.toLowerCase()],
                                duration: 5000,
                                color: 'danger'
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        }); });
    };
    EventUpsertModalComponent.prototype._handleSuccess = function () {
        var _this = this;
        this.store.Success$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (success) { return __awaiter(_this, void 0, void 0, function () {
            var toast, momentStartTime, reminder;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!success) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.toastController.create({
                                message: this.translations.EVENTS.SUCCESS[success.after.toLowerCase()],
                                duration: 3000,
                                color: 'success'
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present().then(function () { return _this.modalCtrl.dismiss({ eventId: _this.eventId }); });
                        if (success.after === 'CREATE' || success.after === 'UPDATE') {
                            momentStartTime = moment(this.eventForm.get('datetime_from').value);
                            reminder = {
                                id: success.after === 'CREATE' ? success.data.id : this.eventId,
                                title: 'Evento próximo',
                                text: this.eventForm.get('title').value,
                                date: momentStartTime.subtract(10, 'minutes').set({ second: 0, millisecond: 0 }).toDate()
                            };
                            if (this.localNotifSrvc) {
                                if (success.after === 'CREATE') {
                                    this.localNotifSrvc.setReminder(reminder);
                                }
                                else {
                                    this.localNotifSrvc.updateReminder(reminder);
                                }
                            }
                        }
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        }); });
    };
    EventUpsertModalComponent.prototype.ngOnDestroy = function () {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    };
    EventUpsertModalComponent.prototype.upsertEvent = function () {
        this.eventForm.get('attendees')
            .patchValue(this.eventForm.get('attendees').value.map(function (c) { return ({ contact_id: c.id }); }));
        this.action === 'CREATE' ?
            this.store.createEvent(this.eventForm.value) : this.store.updateEvent(this.eventId, this.eventForm.value);
    };
    EventUpsertModalComponent.prototype.openContactPicker = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: ContactPickerModalComponent,
                            componentProps: {
                                currentAttendees: this.evAttendees.map(function (c) { return c.id; })
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, modal.onWillDismiss()];
                    case 3:
                        data = (_a.sent()).data;
                        if (data) {
                            this.eventForm.patchValue({ attendees: __spread(data) });
                            this.evAttendees = __spread(data);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    EventUpsertModalComponent.prototype.removeAttendee = function (contact) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.translate.get('EVENTS.confirmRemoveAttendee', { name: contact.name + " " + contact.lastName })
                    .pipe(takeUntil(this.destroyed$))
                    .subscribe(function (msg) { return __awaiter(_this, void 0, void 0, function () {
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.alertCtrl.create({
                                    header: this.translations.GENERAL.ACTION.confirm,
                                    message: msg,
                                    buttons: [{
                                            text: this.translations.GENERAL.ACTION.ok,
                                            handler: function () {
                                                _this.evAttendees = _this.evAttendees.filter(function (c) { return c.id !== contact.id; });
                                                _this.eventForm.patchValue({
                                                    attendees: _this.evAttendees
                                                });
                                            }
                                        }, {
                                            text: this.translations.GENERAL.ACTION.cancel,
                                            role: 'cancel',
                                            cssClass: 'primary',
                                        }]
                                })];
                            case 1:
                                (_a.sent()).present();
                                return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    EventUpsertModalComponent.prototype.showAttendeeDetails = function (contact) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: contact.name + " " + contact.lastName,
                            message: "<b>Email</b>: " + contact.email + "<br><b>" + this.translations.FORM.LABEL.phone + "</b>: " + contact.phone,
                            buttons: [{
                                    text: this.translations.GENERAL.ACTION.ok,
                                    role: 'cancel',
                                    cssClass: 'primary',
                                }]
                        })];
                    case 1:
                        (_a.sent()).present();
                        return [2 /*return*/];
                }
            });
        });
    };
    EventUpsertModalComponent.prototype.openSearchPlace = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: PlacePickerModalComponent,
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, modal.onWillDismiss()];
                    case 3:
                        data = (_a.sent()).data;
                        if (data) {
                            this.eventForm.patchValue({ place: data.description });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    EventUpsertModalComponent.prototype.close = function () {
        this.modalCtrl.dismiss( /*{someData: ...} */);
    };
    EventUpsertModalComponent.prototype.dateChange = function (value, isStart) {
        if (isStart === void 0) { isStart = true; }
        var currentStartDate = moment(this.eventForm.value.datetime_from);
        var currentEndDate = moment(this.eventForm.value.datetime_to);
        if (isStart) {
            var startValue = moment(value);
            if (startValue > currentEndDate) {
                this.eventForm.patchValue({ datetime_to: currentStartDate.add(1, 'hours').toISOString(true) });
            }
        }
        else {
            var endValue = moment(value);
            if (endValue < currentStartDate) {
                this.eventForm.patchValue({ datetime_from: currentEndDate.subtract(1, 'hours').toISOString(true) });
            }
        }
    };
    EventUpsertModalComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Optional }, { type: Inject, args: [LOCAL_NOTIFICATIONS_SERVICE,] }] },
        { type: EventsStore },
        { type: LoadingController },
        { type: ModalController },
        { type: AlertController },
        { type: TranslateService },
        { type: ToastController },
        { type: AppSettingsService }
    ]; };
    __decorate([
        Input()
    ], EventUpsertModalComponent.prototype, "period", void 0);
    __decorate([
        Input()
    ], EventUpsertModalComponent.prototype, "action", void 0);
    __decorate([
        Input()
    ], EventUpsertModalComponent.prototype, "attendees", void 0);
    EventUpsertModalComponent = __decorate([
        Component({
            selector: 'boxx-event-upsert',
            template: "<ion-header mode=\"md\" class=\"ion-no-border\">\n    <ion-toolbar color=\"primary\">\n        <div class=\"brand-icon-wrapper\">\n            <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n        </div>\n\n        <ion-title>\n            <div class=\"custom-header-wrapper\">\n                {{'GENERAL.ACTION.'+ (action == 'CREATE' ? 'create' : 'update') | translate}} {{'GENERAL.event' |\n            translate}}\n            </div>\n        </ion-title>\n\n        <ion-buttons slot=\"start\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <ion-buttons slot=\"end\">\n            <ion-button [disabled]=\"true\">\n                <ion-icon></ion-icon>\n            </ion-button>\n        </ion-buttons>\n\n        <boxx-native-version-info></boxx-native-version-info>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding-horizontal\">\n\n    <ion-list [formGroup]=\"eventForm\">\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.eventName\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-input formControlName=\"title\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.title\">\n            <div class=\"error-feedback\"\n                *ngIf=\"eventForm.get('title').hasError(validation.type) && eventForm.get('title').invalid && (eventForm.get('title').dirty || eventForm.get('title').touched)\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item custom-item ion-no-padding\">\n            <ion-label>\n                <p translate (click)=\"openSearchPlace()\">\n                    <ion-text color=\"primary\" translate>FORM.LABEL.place</ion-text>\n                    <ion-icon name=\"search-outline\" color=\"primary\" style=\"margin-left: 8px;\"></ion-icon>\n                </p>\n                <p [hidden]=\"eventForm.get('place').value\" (click)=\"openSearchPlace()\">\n                    <ion-text color=\"medium\" translate>EVENTS.tapToSearchPlace</ion-text>\n                </p>\n                <p [hidden]=\"!eventForm.get('place').value\" (click)=\"openSearchPlace()\" class=\"ion-text-wrap\">\n                    <ion-text color=\"medium\">{{eventForm.get('place').value}}</ion-text>\n                </p>\n            </ion-label>\n        </ion-item>\n        <!--<div *ngFor=\"let validation of validationMessages.place\">\n      <div class=\"error-feedback\"\n        *ngIf=\"eventForm.get('place').hasError(validation.type) && eventForm.get('place').invalid\">\n        <p>{{validation.message | translate: validation.params}}</p>\n      </div>\n    </div>-->\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                GENERAL.from\n                <ion-text color=\"danger\">*</ion-text>\n            </ion-label>\n            <ion-datetime #myStartPicker cancelText=\"{{'GENERAL.ACTION.cancel' | translate}}\" doneText=\"Ok\"\n                formControlName=\"datetime_from\" displayFormat=\"DD MMMM YYYY - HH:mm\"\n                value=\"{{eventForm.get('datetime_from').value}}\" (ionChange)=\"dateChange(myStartPicker.value)\"\n                placeholder=\"{{'GENERAL.ACTION.selectDate' | translate}}\" monthNames=\"{{monthNames}}\"></ion-datetime>\n        </ion-item>\n        <div *ngFor=\"let validation of validationMessages.datetime_from\">\n            <div class=\"error-feedback\"\n                *ngIf=\"eventForm.get('datetime_from').hasError(validation.type) && eventForm.get('datetime_from').invalid && (eventForm.get('datetime_from').dirty || eventForm.get('datetime_from').touched)\">\n                <p>{{validation.message | translate: validation.params}}</p>\n            </div>\n        </div>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                GENERAL.to\n                <!--<ion-text color=\"danger\">*</ion-text>-->\n            </ion-label>\n            <ion-datetime #myEndPicker cancelText=\"{{'GENERAL.ACTION.cancel' | translate}}\" doneText=\"Ok\"\n                formControlName=\"datetime_to\" (ionChange)=\"dateChange(myEndPicker.value, false)\"\n                displayFormat=\"DD MMMM YYYY - HH:mm\" value=\"{{eventForm.get('datetime_to').value}}\"\n                placeholder=\"{{'GENERAL.ACTION.selectDate' | translate}}\" monthNames=\"{{monthNames}}\"></ion-datetime>\n        </ion-item>\n        <!--<div *ngFor=\"let validation of validationMessages.datetime_to\">\n      <div class=\"error-feedback\"\n        *ngIf=\"eventForm.get('datetime_to').hasError(validation.type) && eventForm.get('datetime_to').invalid\">\n        <p>{{validation.message | translate: validation.params}}</p>\n      </div>\n    </div>-->\n\n        <ion-item class=\"boxx-input-item custom-item ion-no-padding\">\n            <ion-label class=\"attendee-chip\" color=\"primary\">\n                <p (click)=\"openContactPicker()\" class=\"vertical-center\">\n                    <ion-text color=\"primary\" translate>FORM.LABEL.attendees</ion-text>\n                    <ion-icon name=\"add-circle\" color=\"primary\"></ion-icon>\n                </p>\n                <p [hidden]=\"evAttendees.length > 0\" (click)=\"openContactPicker()\">\n                    <ion-text color=\"medium\" translate>EVENTS.tapToSelectContact</ion-text>\n                </p>\n                <ion-chip *ngFor=\"let contact of evAttendees\">\n                    <ion-avatar>\n                        <div>{{contact.name[0]}}{{contact.lastName[0]}}</div>\n                    </ion-avatar>\n                    <ion-label (click)=\"showAttendeeDetails(contact)\">{{contact.name}} {{contact.lastName}}</ion-label>\n                    <ion-icon name=\"close-circle-outline\" (click)=\"removeAttendee(contact)\"></ion-icon>\n                </ion-chip>\n            </ion-label>\n        </ion-item>\n\n        <ion-item class=\"boxx-input-item\">\n            <ion-label position=\"stacked\" color=\"primary\" translate>\n                FORM.LABEL.description\n                <!--<ion-text color=\"danger\">*</ion-text>-->\n            </ion-label>\n            <ion-textarea formControlName=\"description\"></ion-textarea>\n        </ion-item>\n    </ion-list>\n\n    <div class=\"ion-margin-vertical\">\n        <ion-button color=\"primary\" expand=\"block\" [disabled]=\"eventForm.invalid\" (click)=\"upsertEvent()\">\n            {{(action == 'CREATE' ? 'GENERAL.ACTION.create': 'GENERAL.ACTION.update') | translate}} {{'GENERAL.event' |\n            translate}}\n        </ion-button>\n    </div>\n\n</ion-content>",
            styles: ["@charset \"UTF-8\";:host ion-toolbar.md ion-title{padding:0}:host .custom-item ion-label{padding-left:16px}:host .custom-item ion-label .attendee-chip p:first-child{display:flex;justify-content:space-between}:host .custom-item ion-label .attendee-chip p:first-child ion-icon{width:20px;height:20px}:host .custom-item ion-chip ion-label{padding-left:0}ion-label.attendee-chip p:first-child{display:flex;justify-content:space-between}ion-label.attendee-chip p:first-child ion-icon{width:20px!important;height:20px!important}"]
        }),
        __param(0, Optional()), __param(0, Inject(LOCAL_NOTIFICATIONS_SERVICE))
    ], EventUpsertModalComponent);
    return EventUpsertModalComponent;
}());
export { EventUpsertModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnQtdXBzZXJ0Lm1vZGFsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLW1vZHVsZXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ldmVudC11cHNlcnQtbW9kYWwvZXZlbnQtdXBzZXJ0Lm1vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQXFCLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN0RixPQUFPLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNwRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDbkQsT0FBTyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDNUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxlQUFlLEVBQUUsZUFBZSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDdEcsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkQsT0FBTyxLQUFLLE9BQU8sTUFBTSxRQUFRLENBQUM7QUFDbEMsT0FBTyxFQUFjLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMzQyxPQUFPLEVBQUUsU0FBUyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSwyQkFBMkIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBRWhHLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQzNGLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBR3JGLDhCQUE4QjtBQUM5QixJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUM7QUFPdkI7SUFFSSxtQ0FDNkQsY0FBMEMsRUFDM0YsS0FBa0IsRUFDbkIsV0FBOEIsRUFDN0IsU0FBMEIsRUFDMUIsU0FBMEIsRUFDMUIsU0FBMkIsRUFDM0IsZUFBZ0MsRUFDaEMsYUFBaUM7UUFSN0MsaUJBY0M7UUFiNEQsbUJBQWMsR0FBZCxjQUFjLENBQTRCO1FBQzNGLFVBQUssR0FBTCxLQUFLLENBQWE7UUFDbkIsZ0JBQVcsR0FBWCxXQUFXLENBQW1CO1FBQzdCLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBQzFCLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBQzFCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBQzNCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyxrQkFBYSxHQUFiLGFBQWEsQ0FBb0I7UUFhN0MsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFLcEMsMENBQTBDO1FBQzFDLHdEQUF3RDtRQUV4RCxlQUFVLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRXhDLGdCQUFXLEdBQXdCLEVBQUUsQ0FBQztRQU10Qyx1QkFBa0IsR0FBRztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO1lBQ3JFLEtBQUssRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDLG1CQUFtQixDQUFDLEtBQUs7WUFDckUsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFLENBQUMsbUJBQW1CLENBQUMsYUFBYTtZQUNyRixXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXO1NBQ3BGLENBQUM7UUEvQkUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2FBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxVQUFDLENBQU0sSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixDQUFDLENBQUM7SUFDdEQsQ0FBQztJQThCRCw0Q0FBUSxHQUFSO1FBQUEsaUJBa0RDO1FBaERHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxTQUFTLENBQUM7WUFDM0IsS0FBSyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQy9DLEtBQUssRUFBRSxJQUFJLFdBQVcsRUFBRTtZQUN4QixhQUFhLEVBQUUsSUFBSSxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7WUFDL0YsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RGLFNBQVMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLEVBQUUsQ0FBQztZQUNoRCxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDO1lBQ2hDLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUM7WUFDN0IsU0FBUyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQztTQUNqQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUV6RCxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssUUFBUSxFQUFFO1lBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFO2lCQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDaEMsU0FBUyxDQUFDLFVBQUEsRUFBRTtnQkFDVCxLQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztnQkFFbEIsS0FBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO3FCQUNwQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztxQkFDaEMsU0FBUyxDQUFDLFVBQUEsQ0FBQztvQkFFUixLQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUM5QixVQUFBLENBQUMsSUFBSSxPQUFBLElBQUksWUFBWSxDQUFDO3dCQUNsQixFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLEtBQUs7cUJBQ3RGLENBQUMsRUFGRyxDQUVILENBQ0wsQ0FBQztvQkFFRixLQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQzt3QkFDdEIsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLO3dCQUNkLEtBQUssRUFBRSxDQUFDLENBQUMsS0FBSzt3QkFDZCxhQUFhLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO3dCQUN2RCxXQUFXLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO3dCQUNuRCxTQUFTLEVBQUUsS0FBSSxDQUFDLFdBQVc7d0JBQzNCLFdBQVcsRUFBRSxDQUFDLENBQUMsV0FBVztxQkFDN0IsQ0FBQyxDQUFDO2dCQUNQLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxDQUFDLENBQUM7U0FDVjthQUNJO1lBQ0QsMkNBQTJDO1lBQzNDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQy9CO1FBRUQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUVPLGtEQUFjLEdBQXRCO1FBQUEsaUJBcUJDO1FBcEJHLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7UUFFcEMsSUFBSSxDQUFDLFFBQVE7YUFDUixJQUFJLENBQ0QsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFDMUIsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQ3BDO2FBQ0EsU0FBUyxDQUFDLFVBQU8sRUFBZ0I7Z0JBQWhCLGtCQUFnQixFQUFmLGVBQU8sRUFBRSxhQUFLOzs7Ozs7O2lDQUN6QixPQUFPLEVBQVAsd0JBQU87NEJBQ1AsS0FBQSxJQUFJLENBQUE7NEJBQVUscUJBQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsRUFBQTs7NEJBQTdDLEdBQUssTUFBTSxHQUFHLFNBQStCLENBQUM7NEJBQzlDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7Ozs0QkFHdEIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO2dDQUNiLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDO29DQUM1QixLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztnQ0FDdkIsQ0FBQyxDQUFDLENBQUM7NkJBQ047Ozs7OztTQUVSLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTyxnREFBWSxHQUFwQjtRQUFBLGlCQWFDO1FBWkcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNO2FBQ1osSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUyxDQUFDLFVBQU8sQ0FBQzs7Ozs7NkJBQ1gsQ0FBQyxFQUFELHdCQUFDO3dCQUNhLHFCQUFNLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDO2dDQUM1QyxPQUFPLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7Z0NBQy9ELFFBQVEsRUFBRSxJQUFJO2dDQUNkLEtBQUssRUFBRSxRQUFROzZCQUNsQixDQUFDLEVBQUE7O3dCQUpJLEtBQUssR0FBRyxTQUlaO3dCQUNGLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQzs7Ozs7YUFFdkIsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVPLGtEQUFjLEdBQXRCO1FBQUEsaUJBa0NDO1FBakNHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUTthQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxVQUFPLE9BQU87Ozs7Ozs2QkFDakIsT0FBTyxFQUFQLHdCQUFPO3dCQUNPLHFCQUFNLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDO2dDQUM1QyxPQUFPLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7Z0NBQ3RFLFFBQVEsRUFBRSxJQUFJO2dDQUNkLEtBQUssRUFBRSxTQUFTOzZCQUNuQixDQUFDLEVBQUE7O3dCQUpJLEtBQUssR0FBRyxTQUlaO3dCQUVGLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsT0FBTyxFQUFFLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFqRCxDQUFpRCxDQUFDLENBQUM7d0JBRTlFLElBQUksT0FBTyxDQUFDLEtBQUssS0FBSyxRQUFRLElBQUksT0FBTyxDQUFDLEtBQUssS0FBSyxRQUFRLEVBQUU7NEJBQ3BELGVBQWUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBRXBFLFFBQVEsR0FBbUI7Z0NBQzdCLEVBQUUsRUFBRSxPQUFPLENBQUMsS0FBSyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUUsT0FBTyxDQUFDLElBQW1CLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTztnQ0FDL0UsS0FBSyxFQUFFLGdCQUFnQjtnQ0FDdkIsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUs7Z0NBQ3ZDLElBQUksRUFBRSxlQUFlLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLFdBQVcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sRUFBRTs2QkFDNUYsQ0FBQzs0QkFFRixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUM7Z0NBQ3BCLElBQUksT0FBTyxDQUFDLEtBQUssS0FBSyxRQUFRLEVBQUU7b0NBQzVCLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lDQUM3QztxQ0FDSTtvQ0FDRCxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztpQ0FDaEQ7NkJBQ0o7eUJBQ0o7Ozs7O2FBRVIsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELCtDQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCwrQ0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDO2FBQzFCLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQUMsQ0FBZSxJQUFLLE9BQUEsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBdEIsQ0FBc0IsQ0FBQyxDQUFDLENBQUM7UUFFeEcsSUFBSSxDQUFDLE1BQU0sS0FBSyxRQUFRLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEgsQ0FBQztJQUVLLHFEQUFpQixHQUF2Qjs7Ozs7NEJBQ2tCLHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDOzRCQUN0QyxTQUFTLEVBQUUsMkJBQTJCOzRCQUN0QyxjQUFjLEVBQUU7Z0NBQ1osZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsRUFBRSxFQUFKLENBQUksQ0FBQzs2QkFDcEQ7eUJBQ0osQ0FBQyxFQUFBOzt3QkFMSSxLQUFLLEdBQUcsU0FLWjt3QkFFRixxQkFBTSxLQUFLLENBQUMsT0FBTyxFQUFFLEVBQUE7O3dCQUFyQixTQUFxQixDQUFDO3dCQUVMLHFCQUFNLEtBQUssQ0FBQyxhQUFhLEVBQUUsRUFBQTs7d0JBQXBDLElBQUksR0FBSyxDQUFBLFNBQTJCLENBQUEsS0FBaEM7d0JBRVosSUFBSSxJQUFJLEVBQUU7NEJBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFBRSxTQUFTLFdBQU0sSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDOzRCQUNwRCxJQUFJLENBQUMsV0FBVyxZQUFPLElBQUksQ0FBQyxDQUFDO3lCQUNoQzs7Ozs7S0FDSjtJQUVLLGtEQUFjLEdBQXBCLFVBQXFCLE9BQXFCOzs7O2dCQUN0QyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsRUFBRSxFQUFFLElBQUksRUFBSyxPQUFPLENBQUMsSUFBSSxTQUFJLE9BQU8sQ0FBQyxRQUFVLEVBQUUsQ0FBQztxQkFDOUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQ2hDLFNBQVMsQ0FBQyxVQUFPLEdBQUc7Ozs7b0NBQ2hCLHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO29DQUN6QixNQUFNLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU87b0NBQ2hELE9BQU8sRUFBRSxHQUFHO29DQUNaLE9BQU8sRUFBRSxDQUFDOzRDQUNOLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTs0Q0FDekMsT0FBTyxFQUFFO2dEQUNMLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsRUFBRSxLQUFLLE9BQU8sQ0FBQyxFQUFFLEVBQW5CLENBQW1CLENBQUMsQ0FBQztnREFDckUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUM7b0RBQ3RCLFNBQVMsRUFBRSxLQUFJLENBQUMsV0FBVztpREFDOUIsQ0FBQyxDQUFDOzRDQUNQLENBQUM7eUNBQ0osRUFBRTs0Q0FDQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU07NENBQzdDLElBQUksRUFBRSxRQUFROzRDQUNkLFFBQVEsRUFBRSxTQUFTO3lDQUN0QixDQUFDO2lDQUNMLENBQUMsRUFBQTs7Z0NBaEJGLENBQUMsU0FnQkMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDOzs7O3FCQUNqQixDQUFDLENBQUM7Ozs7S0FDVjtJQUVLLHVEQUFtQixHQUF6QixVQUEwQixPQUFxQjs7Ozs0QkFDMUMscUJBQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7NEJBQ3pCLE1BQU0sRUFBSyxPQUFPLENBQUMsSUFBSSxTQUFJLE9BQU8sQ0FBQyxRQUFVOzRCQUM3QyxPQUFPLEVBQUUsbUJBQWlCLE9BQU8sQ0FBQyxLQUFLLGVBQVUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssY0FBUyxPQUFPLENBQUMsS0FBTzs0QkFDM0csT0FBTyxFQUFFLENBQUM7b0NBQ04sSUFBSSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO29DQUN6QyxJQUFJLEVBQUUsUUFBUTtvQ0FDZCxRQUFRLEVBQUUsU0FBUztpQ0FFdEIsQ0FBQzt5QkFDTCxDQUFDLEVBQUE7O3dCQVRGLENBQUMsU0FTQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7Ozs7O0tBQ2pCO0lBRUssbURBQWUsR0FBckI7Ozs7OzRCQUNrQixxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQzs0QkFDdEMsU0FBUyxFQUFFLHlCQUF5Qjt5QkFFdkMsQ0FBQyxFQUFBOzt3QkFISSxLQUFLLEdBQUcsU0FHWjt3QkFFRixxQkFBTSxLQUFLLENBQUMsT0FBTyxFQUFFLEVBQUE7O3dCQUFyQixTQUFxQixDQUFDO3dCQUVMLHFCQUFNLEtBQUssQ0FBQyxhQUFhLEVBQUUsRUFBQTs7d0JBQXBDLElBQUksR0FBSyxDQUFBLFNBQTJCLENBQUEsS0FBaEM7d0JBRVosSUFBSSxJQUFJLEVBQUU7NEJBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7eUJBQzFEOzs7OztLQUNKO0lBRUQseUNBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELDhDQUFVLEdBQVYsVUFBVyxLQUFLLEVBQUUsT0FBYztRQUFkLHdCQUFBLEVBQUEsY0FBYztRQUM1QixJQUFNLGdCQUFnQixHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwRSxJQUFNLGNBQWMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDaEUsSUFBSSxPQUFPLEVBQUU7WUFDVCxJQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDakMsSUFBSSxVQUFVLEdBQUcsY0FBYyxFQUFFO2dCQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUFFLFdBQVcsRUFBRSxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDbEc7U0FDSjthQUFNO1lBQ0gsSUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQy9CLElBQUksUUFBUSxHQUFHLGdCQUFnQixFQUFFO2dCQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ3ZHO1NBQ0o7SUFDTCxDQUFDOztnREE5UUksUUFBUSxZQUFJLE1BQU0sU0FBQywyQkFBMkI7Z0JBQ2hDLFdBQVc7Z0JBQ04saUJBQWlCO2dCQUNsQixlQUFlO2dCQUNmLGVBQWU7Z0JBQ2YsZ0JBQWdCO2dCQUNWLGVBQWU7Z0JBQ2pCLGtCQUFrQjs7SUFPcEM7UUFBUixLQUFLLEVBQUU7NkRBQWdDO0lBQy9CO1FBQVIsS0FBSyxFQUFFOzZEQUE2QjtJQUM1QjtRQUFSLEtBQUssRUFBRTtnRUFBZ0M7SUFuQi9CLHlCQUF5QjtRQUxyQyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLGsyTkFBd0M7O1NBRTNDLENBQUM7UUFJTyxXQUFBLFFBQVEsRUFBRSxDQUFBLEVBQUUsV0FBQSxNQUFNLENBQUMsMkJBQTJCLENBQUMsQ0FBQTtPQUgzQyx5QkFBeUIsQ0FrUnJDO0lBQUQsZ0NBQUM7Q0FBQSxBQWxSRCxJQWtSQztTQWxSWSx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEluamVjdCwgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0LCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIEZvcm1Hcm91cCwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IENvbnRhY3RNb2RlbCB9IGZyb20gJ0Bib3h4L2NvbnRhY3RzLWNvcmUnO1xuaW1wb3J0IHsgRXZlbnRNb2RlbCwgRXZlbnRzU3RvcmUsIElFdmVudFJlbWluZGVyIH0gZnJvbSAnQGJveHgvZXZlbnRzLWNvcmUnO1xuaW1wb3J0IHsgQWxlcnRDb250cm9sbGVyLCBMb2FkaW5nQ29udHJvbGxlciwgTW9kYWxDb250cm9sbGVyLCBUb2FzdENvbnRyb2xsZXIgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQgKiBhcyBtb21lbnRfIGZyb20gJ21vbWVudCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YWtlVW50aWwsIHdpdGhMYXRlc3RGcm9tIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQXBwU2V0dGluZ3NTZXJ2aWNlLCBMT0NBTF9OT1RJRklDQVRJT05TX1NFUlZJQ0UgfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvZ2xvYmFsLXBhcmFtcyc7XG5pbXBvcnQgeyBJTG9jYWxOb3RpZmljYXRpb25zU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC1tb2R1bGVzLm1vZHVsZSc7XG5pbXBvcnQgeyBDb250YWN0UGlja2VyTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi9jb250YWN0LXBpY2tlci1tb2RhbC9jb250YWN0LXBpY2tlci5tb2RhbCc7XG5pbXBvcnQgeyBQbGFjZVBpY2tlck1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vcGxhY2UtcGlja2VyLW1vZGFsL3BsYWNlLXBpY2tlci5tb2RhbCc7XG5cblxuLy8gVG8gcHJldmVudCBjb21waWxpbmcgZXJyb3JzXG5jb25zdCBtb21lbnQgPSBtb21lbnRfO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtZXZlbnQtdXBzZXJ0JyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vZXZlbnQtdXBzZXJ0Lm1vZGFsLmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL2V2ZW50LXVwc2VydC5tb2RhbC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIEV2ZW50VXBzZXJ0TW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgQE9wdGlvbmFsKCkgQEluamVjdChMT0NBTF9OT1RJRklDQVRJT05TX1NFUlZJQ0UpIHByaXZhdGUgbG9jYWxOb3RpZlNydmM6IElMb2NhbE5vdGlmaWNhdGlvbnNTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIHN0b3JlOiBFdmVudHNTdG9yZSxcbiAgICAgICAgcHVibGljIGxvYWRpbmdDdHJsOiBMb2FkaW5nQ29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSBtb2RhbEN0cmw6IE1vZGFsQ29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSBhbGVydEN0cmw6IEFsZXJ0Q29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgdG9hc3RDb250cm9sbGVyOiBUb2FzdENvbnRyb2xsZXIsXG4gICAgICAgIHByaXZhdGUgbW9kdWxlU2VydmljZTogQXBwU2V0dGluZ3NTZXJ2aWNlXG4gICAgKSB7XG5cbiAgICAgICAgdGhpcy50cmFuc2xhdGUuZ2V0KFsnR0VORVJBTCcsICdGT1JNJywgJ0VWRU5UUyddKVxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuZGVzdHJveWVkJCkpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKCh0OiBhbnkpID0+IHRoaXMudHJhbnNsYXRpb25zID0gdCk7XG4gICAgfVxuICAgIEBJbnB1dCgpIHBlcmlvZDogeyBmcm9tOiBhbnksIHRvOiBhbnkgfTtcbiAgICBASW5wdXQoKSBhY3Rpb246ICdDUkVBVEUnIHwgJ1VQREFURSc7XG4gICAgQElucHV0KCkgYXR0ZW5kZWVzOiBBcnJheTxDb250YWN0TW9kZWw+O1xuXG4gICAgZXZlbnQkOiBPYnNlcnZhYmxlPEV2ZW50TW9kZWw+O1xuICAgIGxvYWRpbmckOiBPYnNlcnZhYmxlPGJvb2xlYW4+O1xuICAgIGRlc3Ryb3llZCQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xuICAgIGV2ZW50SWQ6IG51bWJlcjtcblxuICAgIGxvYWRlcjogYW55O1xuXG4gICAgLy8gc3RhcnRUaW1lID0gbW9tZW50KCkudG9JU09TdHJpbmcodHJ1ZSk7XG4gICAgLy8gZW5kVGltZSA9IG1vbWVudCgpLmFkZCgxLCAnaG91cnMnKS50b0lTT1N0cmluZyh0cnVlKTtcblxuICAgIG1vbnRoTmFtZXMgPSBtb21lbnQubW9udGhzKCkudG9TdHJpbmcoKTtcblxuICAgIGV2QXR0ZW5kZWVzOiBBcnJheTxDb250YWN0TW9kZWw+ID0gW107XG5cbiAgICBldmVudEZvcm06IEZvcm1Hcm91cDtcblxuICAgIHRyYW5zbGF0aW9uczogYW55O1xuXG4gICAgdmFsaWRhdGlvbk1lc3NhZ2VzID0ge1xuICAgICAgICB0aXRsZTogdGhpcy5tb2R1bGVTZXJ2aWNlLmdldEFwcENvbnN0YW50cygpLlZBTElEQVRJT05fTUVTU0FHRVMudGl0bGUsXG4gICAgICAgIHBsYWNlOiB0aGlzLm1vZHVsZVNlcnZpY2UuZ2V0QXBwQ29uc3RhbnRzKCkuVkFMSURBVElPTl9NRVNTQUdFUy5wbGFjZSxcbiAgICAgICAgZGF0ZXRpbWVfZnJvbTogdGhpcy5tb2R1bGVTZXJ2aWNlLmdldEFwcENvbnN0YW50cygpLlZBTElEQVRJT05fTUVTU0FHRVMuZGF0ZXRpbWVfZnJvbSxcbiAgICAgICAgZGF0ZXRpbWVfdG86IHRoaXMubW9kdWxlU2VydmljZS5nZXRBcHBDb25zdGFudHMoKS5WQUxJREFUSU9OX01FU1NBR0VTLmRhdGV0aW1lX3RvLFxuICAgIH07XG5cbiAgICBuZ09uSW5pdCgpIHtcblxuICAgICAgICB0aGlzLmV2ZW50Rm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xuICAgICAgICAgICAgdGl0bGU6IG5ldyBGb3JtQ29udHJvbCgnJywgVmFsaWRhdG9ycy5yZXF1aXJlZCksXG4gICAgICAgICAgICBwbGFjZTogbmV3IEZvcm1Db250cm9sKCksXG4gICAgICAgICAgICBkYXRldGltZV9mcm9tOiBuZXcgRm9ybUNvbnRyb2wobW9tZW50KHRoaXMucGVyaW9kLmZyb20pLnRvSVNPU3RyaW5nKHRydWUpLCBWYWxpZGF0b3JzLnJlcXVpcmVkKSxcbiAgICAgICAgICAgIGRhdGV0aW1lX3RvOiBuZXcgRm9ybUNvbnRyb2wobW9tZW50KHRoaXMucGVyaW9kLnRvKS5hZGQoMSwgJ2hvdXJzJykudG9JU09TdHJpbmcodHJ1ZSkpLFxuICAgICAgICAgICAgYXR0ZW5kZWVzOiBuZXcgRm9ybUNvbnRyb2wodGhpcy5hdHRlbmRlZXMgfHwgW10pLFxuICAgICAgICAgICAgZGVzY3JpcHRpb246IG5ldyBGb3JtQ29udHJvbCgnJyksXG4gICAgICAgICAgICBsYXRpdHVkZTogbmV3IEZvcm1Db250cm9sKCcnKSxcbiAgICAgICAgICAgIGxvbmdpdHVkZTogbmV3IEZvcm1Db250cm9sKCcnKVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmV2QXR0ZW5kZWVzID0gdGhpcy5ldmVudEZvcm0uZ2V0KCdhdHRlbmRlZXMnKS52YWx1ZTtcblxuICAgICAgICBpZiAodGhpcy5hY3Rpb24gPT09ICdVUERBVEUnKSB7XG4gICAgICAgICAgICB0aGlzLnN0b3JlLnNlbGVjdGVkRXZlbnQkKClcbiAgICAgICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSlcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGlkID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ldmVudElkID0gaWQ7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdG9yZS5FdmVudEJ5SWQkKGlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuZGVzdHJveWVkJCkpXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKG0gPT4ge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ldkF0dGVuZGVlcyA9IG0uYXR0ZW5kZWVzLm1hcChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYyA9PiBuZXcgQ29udGFjdE1vZGVsKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBjLmNvbnRhY3RJZCwgbmFtZTogYy5uYW1lLCBsYXN0TmFtZTogYy5sYXN0TmFtZSwgZW1haWw6IGMuZW1haWwsIHBob25lOiBjLnBob25lXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZXZlbnRGb3JtLnBhdGNoVmFsdWUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogbS50aXRsZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2U6IG0ucGxhY2UsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGV0aW1lX2Zyb206IG1vbWVudChtLmRhdGV0aW1lRnJvbSkudG9JU09TdHJpbmcodHJ1ZSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGV0aW1lX3RvOiBtb21lbnQobS5kYXRldGltZVRvKS50b0lTT1N0cmluZyh0cnVlKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0ZW5kZWVzOiB0aGlzLmV2QXR0ZW5kZWVzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbS5kZXNjcmlwdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAvLyBvbmx5IHRvIHJlc2V0IHRoZSBzdGF0ZS5zdWNjZXNzIHByb3BlcnR5XG4gICAgICAgICAgICB0aGlzLnN0b3JlLkV2ZW50QnlJZCQobnVsbCk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLl9oYW5kbGVMb2FkaW5nKCk7XG4gICAgICAgIHRoaXMuX2hhbmRsZVN1Y2Nlc3MoKTtcbiAgICAgICAgdGhpcy5faGFuZGxlRXJyb3IoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIF9oYW5kbGVMb2FkaW5nKCkge1xuICAgICAgICB0aGlzLmxvYWRpbmckID0gdGhpcy5zdG9yZS5Mb2FkaW5nJDtcblxuICAgICAgICB0aGlzLmxvYWRpbmckXG4gICAgICAgICAgICAucGlwZShcbiAgICAgICAgICAgICAgICB0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSxcbiAgICAgICAgICAgICAgICB3aXRoTGF0ZXN0RnJvbSh0aGlzLnN0b3JlLkVycm9yJCksXG4gICAgICAgICAgICApXG4gICAgICAgICAgICAuc3Vic2NyaWJlKGFzeW5jIChbbG9hZGluZywgZXJyb3JdKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGxvYWRpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXIgPSBhd2FpdCB0aGlzLmxvYWRpbmdDdHJsLmNyZWF0ZSgpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlci5wcmVzZW50KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5sb2FkZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ0N0cmwuZGlzbWlzcygpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfaGFuZGxlRXJyb3IoKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuRXJyb3IkXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoYXN5bmMgKGUpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoZSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCB0b2FzdCA9IGF3YWl0IHRoaXMudG9hc3RDb250cm9sbGVyLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiB0aGlzLnRyYW5zbGF0aW9ucy5FVkVOVFMuRVJST1JTW2UuYWZ0ZXIudG9Mb3dlckNhc2UoKV0sXG4gICAgICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogNTAwMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAnZGFuZ2VyJ1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgdG9hc3QucHJlc2VudCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgX2hhbmRsZVN1Y2Nlc3MoKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuU3VjY2VzcyRcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLmRlc3Ryb3llZCQpKVxuICAgICAgICAgICAgLnN1YnNjcmliZShhc3luYyAoc3VjY2VzcykgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChzdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHRvYXN0ID0gYXdhaXQgdGhpcy50b2FzdENvbnRyb2xsZXIuY3JlYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IHRoaXMudHJhbnNsYXRpb25zLkVWRU5UUy5TVUNDRVNTW3N1Y2Nlc3MuYWZ0ZXIudG9Mb3dlckNhc2UoKV0sXG4gICAgICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogMzAwMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAnc3VjY2VzcydcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgdG9hc3QucHJlc2VudCgpLnRoZW4oKCkgPT4gdGhpcy5tb2RhbEN0cmwuZGlzbWlzcyh7IGV2ZW50SWQ6IHRoaXMuZXZlbnRJZCB9KSk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHN1Y2Nlc3MuYWZ0ZXIgPT09ICdDUkVBVEUnIHx8IHN1Y2Nlc3MuYWZ0ZXIgPT09ICdVUERBVEUnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBtb21lbnRTdGFydFRpbWUgPSBtb21lbnQodGhpcy5ldmVudEZvcm0uZ2V0KCdkYXRldGltZV9mcm9tJykudmFsdWUpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZW1pbmRlcjogSUV2ZW50UmVtaW5kZXIgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IHN1Y2Nlc3MuYWZ0ZXIgPT09ICdDUkVBVEUnID8gKHN1Y2Nlc3MuZGF0YSBhcyBFdmVudE1vZGVsKS5pZCA6IHRoaXMuZXZlbnRJZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0V2ZW50byBwcsOzeGltbycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogdGhpcy5ldmVudEZvcm0uZ2V0KCd0aXRsZScpLnZhbHVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGU6IG1vbWVudFN0YXJ0VGltZS5zdWJ0cmFjdCgxMCwgJ21pbnV0ZXMnKS5zZXQoeyBzZWNvbmQ6IDAsIG1pbGxpc2Vjb25kOiAwIH0pLnRvRGF0ZSgpXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5sb2NhbE5vdGlmU3J2Yyl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHN1Y2Nlc3MuYWZ0ZXIgPT09ICdDUkVBVEUnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9jYWxOb3RpZlNydmMuc2V0UmVtaW5kZXIocmVtaW5kZXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2NhbE5vdGlmU3J2Yy51cGRhdGVSZW1pbmRlcihyZW1pbmRlcik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMuZGVzdHJveWVkJC5uZXh0KHRydWUpO1xuICAgICAgICB0aGlzLmRlc3Ryb3llZCQuY29tcGxldGUoKTtcbiAgICB9XG5cbiAgICB1cHNlcnRFdmVudCgpIHtcbiAgICAgICAgdGhpcy5ldmVudEZvcm0uZ2V0KCdhdHRlbmRlZXMnKVxuICAgICAgICAgICAgLnBhdGNoVmFsdWUodGhpcy5ldmVudEZvcm0uZ2V0KCdhdHRlbmRlZXMnKS52YWx1ZS5tYXAoKGM6IENvbnRhY3RNb2RlbCkgPT4gKHsgY29udGFjdF9pZDogYy5pZCB9KSkpO1xuXG4gICAgICAgIHRoaXMuYWN0aW9uID09PSAnQ1JFQVRFJyA/XG4gICAgICAgICAgICB0aGlzLnN0b3JlLmNyZWF0ZUV2ZW50KHRoaXMuZXZlbnRGb3JtLnZhbHVlKSA6IHRoaXMuc3RvcmUudXBkYXRlRXZlbnQodGhpcy5ldmVudElkLCB0aGlzLmV2ZW50Rm9ybS52YWx1ZSk7XG4gICAgfVxuXG4gICAgYXN5bmMgb3BlbkNvbnRhY3RQaWNrZXIoKSB7XG4gICAgICAgIGNvbnN0IG1vZGFsID0gYXdhaXQgdGhpcy5tb2RhbEN0cmwuY3JlYXRlKHtcbiAgICAgICAgICAgIGNvbXBvbmVudDogQ29udGFjdFBpY2tlck1vZGFsQ29tcG9uZW50LFxuICAgICAgICAgICAgY29tcG9uZW50UHJvcHM6IHtcbiAgICAgICAgICAgICAgICBjdXJyZW50QXR0ZW5kZWVzOiB0aGlzLmV2QXR0ZW5kZWVzLm1hcChjID0+IGMuaWQpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGF3YWl0IG1vZGFsLnByZXNlbnQoKTtcblxuICAgICAgICBjb25zdCB7IGRhdGEgfSA9IGF3YWl0IG1vZGFsLm9uV2lsbERpc21pc3MoKTtcblxuICAgICAgICBpZiAoZGF0YSkge1xuICAgICAgICAgICAgdGhpcy5ldmVudEZvcm0ucGF0Y2hWYWx1ZSh7IGF0dGVuZGVlczogWy4uLmRhdGFdIH0pO1xuICAgICAgICAgICAgdGhpcy5ldkF0dGVuZGVlcyA9IFsuLi5kYXRhXTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFzeW5jIHJlbW92ZUF0dGVuZGVlKGNvbnRhY3Q6IENvbnRhY3RNb2RlbCkge1xuICAgICAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoJ0VWRU5UUy5jb25maXJtUmVtb3ZlQXR0ZW5kZWUnLCB7IG5hbWU6IGAke2NvbnRhY3QubmFtZX0gJHtjb250YWN0Lmxhc3ROYW1lfWAgfSlcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLmRlc3Ryb3llZCQpKVxuICAgICAgICAgICAgLnN1YnNjcmliZShhc3luYyAobXNnKSA9PiB7XG4gICAgICAgICAgICAgICAgKGF3YWl0IHRoaXMuYWxlcnRDdHJsLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGhlYWRlcjogdGhpcy50cmFuc2xhdGlvbnMuR0VORVJBTC5BQ1RJT04uY29uZmlybSxcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogbXNnLFxuICAgICAgICAgICAgICAgICAgICBidXR0b25zOiBbe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogdGhpcy50cmFuc2xhdGlvbnMuR0VORVJBTC5BQ1RJT04ub2ssXG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVyOiAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ldkF0dGVuZGVlcyA9IHRoaXMuZXZBdHRlbmRlZXMuZmlsdGVyKGMgPT4gYy5pZCAhPT0gY29udGFjdC5pZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ldmVudEZvcm0ucGF0Y2hWYWx1ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dGVuZGVlczogdGhpcy5ldkF0dGVuZGVlc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiB0aGlzLnRyYW5zbGF0aW9ucy5HRU5FUkFMLkFDVElPTi5jYW5jZWwsXG4gICAgICAgICAgICAgICAgICAgICAgICByb2xlOiAnY2FuY2VsJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNzc0NsYXNzOiAncHJpbWFyeScsXG4gICAgICAgICAgICAgICAgICAgIH1dXG4gICAgICAgICAgICAgICAgfSkpLnByZXNlbnQoKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIGFzeW5jIHNob3dBdHRlbmRlZURldGFpbHMoY29udGFjdDogQ29udGFjdE1vZGVsKSB7XG4gICAgICAgIChhd2FpdCB0aGlzLmFsZXJ0Q3RybC5jcmVhdGUoe1xuICAgICAgICAgICAgaGVhZGVyOiBgJHtjb250YWN0Lm5hbWV9ICR7Y29udGFjdC5sYXN0TmFtZX1gLFxuICAgICAgICAgICAgbWVzc2FnZTogYDxiPkVtYWlsPC9iPjogJHtjb250YWN0LmVtYWlsfTxicj48Yj4ke3RoaXMudHJhbnNsYXRpb25zLkZPUk0uTEFCRUwucGhvbmV9PC9iPjogJHtjb250YWN0LnBob25lfWAsXG4gICAgICAgICAgICBidXR0b25zOiBbe1xuICAgICAgICAgICAgICAgIHRleHQ6IHRoaXMudHJhbnNsYXRpb25zLkdFTkVSQUwuQUNUSU9OLm9rLFxuICAgICAgICAgICAgICAgIHJvbGU6ICdjYW5jZWwnLFxuICAgICAgICAgICAgICAgIGNzc0NsYXNzOiAncHJpbWFyeScsXG4gICAgICAgICAgICAgICAgLy8gaGFuZGxlcjogKCkgPT4geyB9XG4gICAgICAgICAgICB9XVxuICAgICAgICB9KSkucHJlc2VudCgpO1xuICAgIH1cblxuICAgIGFzeW5jIG9wZW5TZWFyY2hQbGFjZSgpIHtcbiAgICAgICAgY29uc3QgbW9kYWwgPSBhd2FpdCB0aGlzLm1vZGFsQ3RybC5jcmVhdGUoe1xuICAgICAgICAgICAgY29tcG9uZW50OiBQbGFjZVBpY2tlck1vZGFsQ29tcG9uZW50LFxuICAgICAgICAgICAgLy8gY29tcG9uZW50UHJvcHM6IHt9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGF3YWl0IG1vZGFsLnByZXNlbnQoKTtcblxuICAgICAgICBjb25zdCB7IGRhdGEgfSA9IGF3YWl0IG1vZGFsLm9uV2lsbERpc21pc3MoKTtcblxuICAgICAgICBpZiAoZGF0YSkge1xuICAgICAgICAgICAgdGhpcy5ldmVudEZvcm0ucGF0Y2hWYWx1ZSh7IHBsYWNlOiBkYXRhLmRlc2NyaXB0aW9uIH0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgY2xvc2UoKSB7XG4gICAgICAgIHRoaXMubW9kYWxDdHJsLmRpc21pc3MoLyp7c29tZURhdGE6IC4uLn0gKi8pO1xuICAgIH1cblxuICAgIGRhdGVDaGFuZ2UodmFsdWUsIGlzU3RhcnQgPSB0cnVlKSB7XG4gICAgICAgIGNvbnN0IGN1cnJlbnRTdGFydERhdGUgPSBtb21lbnQodGhpcy5ldmVudEZvcm0udmFsdWUuZGF0ZXRpbWVfZnJvbSk7XG4gICAgICAgIGNvbnN0IGN1cnJlbnRFbmREYXRlID0gbW9tZW50KHRoaXMuZXZlbnRGb3JtLnZhbHVlLmRhdGV0aW1lX3RvKTtcbiAgICAgICAgaWYgKGlzU3RhcnQpIHtcbiAgICAgICAgICAgIGNvbnN0IHN0YXJ0VmFsdWUgPSBtb21lbnQodmFsdWUpO1xuICAgICAgICAgICAgaWYgKHN0YXJ0VmFsdWUgPiBjdXJyZW50RW5kRGF0ZSkge1xuICAgICAgICAgICAgICAgIHRoaXMuZXZlbnRGb3JtLnBhdGNoVmFsdWUoeyBkYXRldGltZV90bzogY3VycmVudFN0YXJ0RGF0ZS5hZGQoMSwgJ2hvdXJzJykudG9JU09TdHJpbmcodHJ1ZSkgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb25zdCBlbmRWYWx1ZSA9IG1vbWVudCh2YWx1ZSk7XG4gICAgICAgICAgICBpZiAoZW5kVmFsdWUgPCBjdXJyZW50U3RhcnREYXRlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5ldmVudEZvcm0ucGF0Y2hWYWx1ZSh7IGRhdGV0aW1lX2Zyb206IGN1cnJlbnRFbmREYXRlLnN1YnRyYWN0KDEsICdob3VycycpLnRvSVNPU3RyaW5nKHRydWUpIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuIl19